package com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003;

import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.DeviceVariable;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command003Response;

/**
 * The response command packet for Command #3, specific to Rosemount's WirelessHART Gateway 1420.
 * 
 * As per the Rosemount documentation, this device ignores the loop current (it returns NAN), and uses PV, SV, TV, and
 * QV to obtain its needed information.
 */
public class C003Dx264eResponse extends Command003Response {

    /**
     * Constructor. This initializes the expected internal state.
     */
    public C003Dx264eResponse() {
        this.setNumberOfHealthyDevices(new DeviceVariable());
        this.setNumberOfUnreachableDevices(new DeviceVariable());
        this.setElectronicsTemperature(new DeviceVariable());
        this.setAmbientTemperature(new DeviceVariable());
    }

    @Override
    public String toString() {
        return "C003Dx264eResponse ["
                + "commandRegistry=" + this.getCommandType()
                + ", frameType=" + this.getFrameType()
                + ", #HealthyDevices=" + super.getPrimaryVariable()
                + ", #UnreachableDevices=" + super.getSecondaryVariable()
                + ", electronicsTemperature=" + super.getTertiaryVariable()
                + ", ambientTemperature=" + super.getQuaternaryVariable()
                + "]";
    }

    /**
     * Get the total number of active, healthy devices (PV).
     * 
     * @return The total number of devices.
     */
    DeviceVariable getNumberOfHealthyDevices() {
        return super.getPrimaryVariable();
    }

    /**
     * Set the total number of active, healthy devices (PV).
     * 
     * @param numberOfHealthyDevices
     *            The total number of devices.
     */
    void setNumberOfHealthyDevices(DeviceVariable numberOfHealthyDevices) {
        super.setPrimaryVariable(numberOfHealthyDevices);
    }

    /**
     * Get the total number of unreachable, or idle devices.
     * 
     * @return The total number of devices.
     */
    DeviceVariable getNumberOfUnreachableDevices() {
        return super.getSecondaryVariable();
    }

    /**
     * Set the total number of unreachable, or idle devices.
     * 
     * @param numberOfUnreachableDevices
     *            The total number of devices.
     */
    void setNumberOfUnreachableDevices(DeviceVariable numberOfUnreachableDevices) {
        super.setSecondaryVariable(numberOfUnreachableDevices);
    }

    /**
     * Get the temperature of the electronics device.
     * 
     * @return The temperature.
     */
    DeviceVariable getElectronicsTemperature() {
        return super.getTertiaryVariable();
    }

    /**
     * Set the temperature of the electronics device.
     * 
     * @param electronicsTemperature
     *            The temperature.
     */
    void setElectronicsTemperature(DeviceVariable electronicsTemperature) {
        super.setTertiaryVariable(electronicsTemperature);
    }

    /**
     * Get the ambient temperature.
     * 
     * @return The temperature.
     */
    DeviceVariable getAmbientTemperature() {
        return super.getQuaternaryVariable();
    }

    /**
     * Get the ambient temperature.
     * 
     * @param ambientTemperature
     *            The temperature.
     */
    void setAmbientTemperature(DeviceVariable ambientTemperature) {
        super.setQuaternaryVariable(ambientTemperature);
    }
}
