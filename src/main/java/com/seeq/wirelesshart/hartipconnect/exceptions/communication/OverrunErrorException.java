package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

/**
 * Thrown when a buffer overrun condition is encountered by the target device. This generally happens when at least one
 * byte of data in the receive buffer of the UART was overwritten before it was read (i.e. the slave did not process the
 * incoming byte fast enough).
 */
public class OverrunErrorException extends HartCommunicationException {

    private static final long serialVersionUID = -1360784980220921605L;

    /**
     * Constructs a {@link OverrunErrorException} instance.
     */
    public OverrunErrorException() {
        super();
    }

    /**
     * Constructs an instance of {@link OverrunErrorException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public OverrunErrorException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link OverrunErrorException} instance with a detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public OverrunErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
