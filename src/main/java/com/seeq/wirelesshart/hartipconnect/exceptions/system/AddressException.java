package com.seeq.wirelesshart.hartipconnect.exceptions.system;

/**
 * This exception is thrown if the Address Type is not recognized.
 */
public class AddressException extends HartSystemException {

    private static final long serialVersionUID = -5404062934875204407L;

    /**
     * Creates a new instance of {@link AddressException} without detail message.
     */
    public AddressException() {}

    /**
     * Constructs an instance of {@link AddressException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public AddressException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of {@link AddressException} with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public AddressException(String message, Throwable cause) {
        super(message, cause);
    }
}
