package com.seeq.wirelesshart.hartipconnect.exceptions.system;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;

/**
 * This exception is thrown when the stored value inside the response byte does not represent a known
 * {@link ResponseCodeType}.
 */
public class ResponseException extends HartSystemException {

    private static final long serialVersionUID = -1793462810439298444L;

    /**
     * Creates a new instance of {@link ResponseException} without detail message.
     */
    public ResponseException() {}

    /**
     * Constructs an instance of {@link ResponseException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public ResponseException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of the {@link ResponseException} class with the specified detail and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public ResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
