package com.seeq.wirelesshart.hartipconnect.exceptions;

/**
 * The Root Exception of all exceptions thrown by the HartIpConnect Library.
 */
public class HartException extends Exception {

    private static final long serialVersionUID = -1712343047813054571L;

    /**
     * Creates a new instance of <code>HartException</code> without detail message.
     */
    public HartException() {}

    /**
     * Constructs an instance of <code>HartException</code> with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public HartException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of {@link HartException} with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause for this exception.
     */
    public HartException(String message, Throwable cause) {
        super(message, cause);
    }
}
