package com.seeq.wirelesshart.hartipconnect.exceptions.response;

/**
 * Thrown when a single error type is encountered in response to a Command query. HART allows certain error codes to
 * have either a single meaning or multiple meanings, and is manufacturer and device dependent. This exception is thrown
 * when the former is detected.
 */
public class ErrorSingleResponseException extends HartResponseException {

    private static final long serialVersionUID = -1236476166919815751L;

    /**
     * Creates a new instance of {@link ErrorSingleResponseException} without detail message.
     */
    public ErrorSingleResponseException() {
        super();
    }

    /**
     * Constructs an instance of {@link ErrorSingleResponseException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public ErrorSingleResponseException(String message) {
        super(message);
    }

    /**
     * Constructs an instance with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public ErrorSingleResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
