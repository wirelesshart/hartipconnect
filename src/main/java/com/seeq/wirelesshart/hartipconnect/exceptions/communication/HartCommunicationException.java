package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;

/**
 * This is a top level exception that is thrown when a HART communication error is detected by a target device.
 */
public class HartCommunicationException extends HartException {

    private static final long serialVersionUID = -6746618311089001485L;

    /**
     * Constructs an instance of the {@link HartCommunicationException}.
     */
    public HartCommunicationException() {
        super();
    }

    /**
     * Constructs an instance of {@link HartCommunicationException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public HartCommunicationException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of the {@link HartCommunicationException} with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public HartCommunicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
