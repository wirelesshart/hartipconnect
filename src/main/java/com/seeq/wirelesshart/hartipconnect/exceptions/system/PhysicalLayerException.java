package com.seeq.wirelesshart.hartipconnect.exceptions.system;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;

/**
 * This exception is thrown when the stored value inside the Delimeter byte does not represent a known
 * {@link PhysicalLayerType}.
 */
public class PhysicalLayerException extends HartSystemException {

    private static final long serialVersionUID = 268923008871021070L;

    /**
     * Creates a new instance of {@link PhysicalLayerException} without detail message.
     */
    public PhysicalLayerException() {}

    /**
     * Constructs an instance of {@link PhysicalLayerException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public PhysicalLayerException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link PhysicalLayerException} instance with the given detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public PhysicalLayerException(String message, Throwable cause) {
        super(message, cause);
    }
}
