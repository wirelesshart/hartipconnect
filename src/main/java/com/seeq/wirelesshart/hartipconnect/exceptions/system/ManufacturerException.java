package com.seeq.wirelesshart.hartipconnect.exceptions.system;

/**
 * This exception is thrown if the Manufacturer Type is not recognized.
 */
public class ManufacturerException extends HartSystemException {

    private static final long serialVersionUID = -7532631550279205335L;

    /**
     * Creates a new instance of {@link ManufacturerException} without detail message.
     */
    public ManufacturerException() {}

    /**
     * Constructs an instance of {@link ManufacturerException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public ManufacturerException(String message) {
        super(message);
    }

    /**
     * Constructor for {@link ManufacturerException} with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public ManufacturerException(String message, Throwable cause) {
        super(message, cause);
    }
}
