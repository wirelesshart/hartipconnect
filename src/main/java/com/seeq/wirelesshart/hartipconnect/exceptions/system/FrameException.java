package com.seeq.wirelesshart.hartipconnect.exceptions.system;

/**
 * An exception thrown when the Frame type is not recognized.
 */
public class FrameException extends HartSystemException {

    private static final long serialVersionUID = 6505813714686925261L;

    /**
     * Creates a new instance of {@link FrameException} without detail message.
     */
    public FrameException() {}

    /**
     * Constructs an instance of {@link FrameException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public FrameException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of {@link FrameException} with the detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public FrameException(String message, Throwable cause) {
        super(message, cause);
    }
}
