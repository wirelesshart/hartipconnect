package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

/**
 * This exception is thrown when a Vertical Parity error is detected as reported by a slave device. Specifically, this
 * happens when one or more of the bytes received by the device was not odd.
 * <p>
 * Typically, to perform error detection, HART utilizes a single parity check product coding scheme. This allows HART to
 * use parity checking in two dimensions: across the bits in a single byte, and across the bit positions in the
 * transmitted message. In effect, each transmitted byte uses 9 bits during transmission which includes 8 bits plus a
 * "Vertical Parity". In addition, across each corresponding byte position in the message, bits have their own parity
 * check, termed the "Longitudinal Parity" check.
 */
public class VerticalParityErrorException extends HartCommunicationException {

    private static final long serialVersionUID = -7581261557944849539L;

    /**
     * Constructs a {@link VerticalParityErrorException} instance.
     */
    public VerticalParityErrorException() {
        super();
    }

    /**
     * Constructs an instance of {@link VerticalParityErrorException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public VerticalParityErrorException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link VerticalParityErrorException} instance with a detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public VerticalParityErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
