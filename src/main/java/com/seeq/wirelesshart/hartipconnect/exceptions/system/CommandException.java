package com.seeq.wirelesshart.hartipconnect.exceptions.system;

/**
 * This exception is thrown if the Command Type is not recognized.
 */
public class CommandException extends HartSystemException {

    private static final long serialVersionUID = -7050648532257261646L;

    /**
     * Creates a new instance of {@link CommandException} without detail message.
     */
    public CommandException() {}

    /**
     * Constructs an instance of {@link CommandException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public CommandException(String message) {
        super(message);
    }

    /**
     * Constructor for {@link CommandException} with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
