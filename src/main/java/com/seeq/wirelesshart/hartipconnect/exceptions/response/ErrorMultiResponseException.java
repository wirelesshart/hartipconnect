package com.seeq.wirelesshart.hartipconnect.exceptions.response;

/**
 * Thrown when a multi error type is encountered in response to a Command query. HART allows certain error codes to have
 * either a single meaning or multiple meanings, and is manufacturer and device dependent. This exception is thrown when
 * the latter is detected.
 */
public class ErrorMultiResponseException extends HartResponseException {

    private static final long serialVersionUID = 7791526683174452787L;

    /**
     * Constructs an instance of {@link ErrorMultiResponseException}.
     */
    public ErrorMultiResponseException() {
        super();
    }

    /**
     * Constructs an instance of {@link ErrorMultiResponseException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public ErrorMultiResponseException(String message) {
        super(message);
    }

    /**
     * Constructs an instance with the specified detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public ErrorMultiResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
