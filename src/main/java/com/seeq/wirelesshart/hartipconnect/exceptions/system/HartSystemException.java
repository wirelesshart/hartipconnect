package com.seeq.wirelesshart.hartipconnect.exceptions.system;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;

/**
 * This exception is thrown whenever a Processing problem is detected. A processing problem is something that is
 * described in the HART specification but which there is no specific error defined in the specification.
 */
public class HartSystemException extends HartException {

    private static final long serialVersionUID = -5860959974969367708L;

    /**
     * Creates a new instance of {@link HartSystemException} without detail message.
     */
    public HartSystemException() {}

    /**
     * Constructs an instance of {@link HartSystemException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public HartSystemException(String message) {
        super(message);
    }

    /**
     * Constructs an instance of the exception with detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public HartSystemException(String message, Throwable cause) {
        super(message, cause);
    }
}
