package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

/**
 * This exception is thrown when a Longitudinal Parity error is detected as reported by a slave device. This typically
 * happens when the calculated Longitudinal Parity does not match the Check Byte at the end of the message.
 * <p>
 * Typically, to perform error detection, HART utilizes a single parity check product coding scheme. This allows HART to
 * use parity checking in two dimensions: across the bits in a single byte, and across the bit positions in the
 * transmitted message. In effect, each transmitted byte uses 9 bits during transmission which includes 8 bits plus a
 * "Vertical Parity". In addition, across each corresponding byte position in the message, bits have their own parity
 * check, termed the "Longitudinal Parity" check.
 */
public class LongitudinalParityErrorException extends HartCommunicationException {

    private static final long serialVersionUID = -4156828049828946302L;

    /**
     * Constructs a {@link LongitudinalParityErrorException} instance.
     */
    public LongitudinalParityErrorException() {
        super();
    }

    /**
     * Constructs an instance of {@link LongitudinalParityErrorException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public LongitudinalParityErrorException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link LongitudinalParityErrorException} instance with a detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public LongitudinalParityErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
