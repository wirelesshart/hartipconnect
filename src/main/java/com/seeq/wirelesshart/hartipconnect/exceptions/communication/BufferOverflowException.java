package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

/**
 * Thrown when a buffer overflow is encountered. This is reported by the target device when the message was too long for
 * the receive buffer of the device itself.
 */
public class BufferOverflowException extends HartCommunicationException {

    private static final long serialVersionUID = 9133950592694996803L;

    /**
     * Constructs a {@link BufferOverflowException} instance.
     */
    public BufferOverflowException() {
        super();
    }

    /**
     * Constructs an instance of {@link BufferOverflowException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public BufferOverflowException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link BufferOverflowException} instance with a detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public BufferOverflowException(String message, Throwable cause) {
        super(message, cause);
    }
}
