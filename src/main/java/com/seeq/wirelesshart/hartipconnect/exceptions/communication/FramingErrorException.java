package com.seeq.wirelesshart.hartipconnect.exceptions.communication;

/**
 * Thrown when a framing exception is encountered. This occurs when the Stop Bit of one or more bytes received by the
 * device was not detected by the UART (i.e. a mark or 1 was not detected when a Stop Bit should have occurred).
 */
public class FramingErrorException extends HartCommunicationException {

    private static final long serialVersionUID = -7768545319319251072L;

    /**
     * Constructs a {@link FramingErrorException} instance.
     */
    public FramingErrorException() {
        super();
    }

    /**
     * Constructs an instance of {@link FramingErrorException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public FramingErrorException(String message) {
        super(message);
    }

    /**
     * Constructs a {@link FramingErrorException} instance with a detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public FramingErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
