package com.seeq.wirelesshart.hartipconnect.exceptions.response;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;

/**
 * This is a top level exception that is thrown when a HART response error is encountered. A response error is one that
 * may be reported by the Gateway in the event that a command results in an error.
 */
public class HartResponseException extends HartException {

    private static final long serialVersionUID = -2149691645897304123L;

    /**
     * Creates a new instance of {@link HartResponseException} without detail message.
     */
    public HartResponseException() {
        super();
    }

    /**
     * Constructs an instance of {@link HartResponseException} with the specified detail message.
     *
     * @param message
     *            The detail message.
     */
    public HartResponseException(String message) {
        super(message);
    }

    /**
     * Constructs an instance with the specified detail and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public HartResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
