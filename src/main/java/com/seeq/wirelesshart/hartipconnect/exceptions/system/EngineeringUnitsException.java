package com.seeq.wirelesshart.hartipconnect.exceptions.system;

/**
 * This exception is thrown when an error is encountered involving Engineering Units, for example, when the Engineering
 * Units code was not recognized by the system.
 */
public class EngineeringUnitsException extends HartSystemException {

    private static final long serialVersionUID = 2423604609506983953L;

    /**
     * Creates a new instance of the {@link EngineeringUnitsException} exception.
     */
    public EngineeringUnitsException() {}

    /**
     * Creates a new instance of the {@link EngineeringUnitsException} exception, with a detail message.
     * 
     * @param message
     *            The detail message.
     */
    public EngineeringUnitsException(String message) {
        super(message);
    }

    /**
     * Creates a new instance of the {@link EngineeringUnitsException} exception, with detail message and cause.
     * 
     * @param message
     *            The detail message.
     * @param cause
     *            The cause.
     */
    public EngineeringUnitsException(String message, Throwable cause) {
        super(message, cause);
    }
}
