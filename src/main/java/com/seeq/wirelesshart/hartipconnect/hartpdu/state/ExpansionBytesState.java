package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;

/**
 * This class implements the state that processes the expansion state bits. Specifically, it extracts the bytes, if
 * available, depending on what was discovered in the {@link DelimeterState}.
 */
class ExpansionBytesState implements State {

    ExpansionBytesState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        if (context.getNumberOfExpansionBytes() == 0) {
            context.setActiveState(new CommandState());
            context.getActiveState().decodeByte(newByte, context);
        } else {
            context.getWorkspaceBytes().add(newByte);

            if (context.getWorkspaceBytes().size() == context.getNumberOfExpansionBytes()) {
                // extract the expansion bytes.
                byte[] expansionBytes = new byte[context.getWorkspaceBytes().size()];
                for (int index = 0; index < context.getWorkspaceBytes().size(); index++) {
                    expansionBytes[index] = context.getWorkspaceBytes().get(index);
                }

                context.getOutputHartPdu().setExpansionBytes(expansionBytes);

                context.getWorkspaceBytes().clear();
                context.setActiveState(new CommandState());
            }
        }
    }

    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) throws HartException {
        // encode the expansion byte, if any.
        if (hartPdu.getExpansionBytes() != null) {
            for (byte expansionByte : hartPdu.getExpansionBytes()) {
                context.getOutputBytes().add(expansionByte);
            }
        }

        context.setActiveState(new CommandState());
    }

}
