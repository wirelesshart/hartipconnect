package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

/**
 * Implements the Address state. From the {@link DelimeterState}, we know the address type, so this class will go
 * through and fill in the address information into the {@link DeviceAddress} instance -- specifically
 * {@link LongFrameAddress} and {@link ShortFrameAddress} instances.
 */
class AddressState implements State {
    public static final byte MASK_6_BITS = (byte) 0x3f;
    public static final byte MASK_8_BITS = (byte) 0xff;
    public static final short DEVICE_UPPER_6_BITS_MASK = MASK_6_BITS << 8;
    public static final short DEVICE_LOWER_8_BITS_MASK = MASK_8_BITS;

    AddressState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) {

        context.getWorkspaceBytes().add(newByte);

        if (context.getOutputHartPdu().getDeviceAddress().getAddressType() == AddressType.IS_POLLING_0_BYTE) {
            // Address type is the short one byte polling protocolVersion.

            boolean isPrimaryMaster = ((newByte >> 7 & 0x01) == 0x01) ? true : false;
            boolean isDeviceInBurstMode = ((newByte >> 6) & 0x01) == 0x01 ? true : false;
            byte pollingAddress = (byte) (newByte & MASK_6_BITS);

            context.getOutputHartPdu().getDeviceAddress().setPrimaryMaster(isPrimaryMaster);
            context.getOutputHartPdu().getDeviceAddress().setDeviceInBurstMode(isDeviceInBurstMode);
            ((ShortFrameAddress) context.getOutputHartPdu().getDeviceAddress()).setPollingAddress(pollingAddress);

            context.getWorkspaceBytes().clear();
            context.setActiveState(new ExpansionBytesState());
        } else {
            // AddressType is unique 5 byte.

            switch (context.getWorkspaceBytes().size()) {
            case 2:

                // extract the primary master and device burst information.
                boolean isPrimaryMaster = ((context.getWorkspaceBytes().get(0).byteValue() >> 7 & 0x01) == 0x01) ? true
                        : false;
                boolean isDeviceInBurstMode = ((context.getWorkspaceBytes().get(0).byteValue() >> 6) & 0x01) == 0x01 ? true
                        : false;

                // assemble the Expanded Device Type Code.
                byte manufacturerIdLSB = (byte) (context.getWorkspaceBytes().get(0).byteValue() & MASK_6_BITS);
                byte deviceTypeCode = (byte) (context.getWorkspaceBytes().get(1).byteValue() & MASK_8_BITS);
                short expandedDeviceTypeCode = (short) ((manufacturerIdLSB << 8) | deviceTypeCode);

                // Set all of the above in our output HART Pdu.
                context.getOutputHartPdu().getDeviceAddress().setPrimaryMaster(isPrimaryMaster);
                context.getOutputHartPdu().getDeviceAddress().setDeviceInBurstMode(isDeviceInBurstMode);
                ((LongFrameAddress) context.getOutputHartPdu().getDeviceAddress())
                        .setExpandedDeviceTypeCode(expandedDeviceTypeCode);

                break;
            case 5:
                // We now have the remaining 3 bytes. These are the unique device identifier.
                byte[] uniqueDeviceIdentifier = new byte[] {
                        context.getWorkspaceBytes().get(2).byteValue(),
                        context.getWorkspaceBytes().get(3).byteValue(),
                        context.getWorkspaceBytes().get(4).byteValue() };

                ((LongFrameAddress) context.getOutputHartPdu().getDeviceAddress())
                        .setUniqueDeviceIdentifier(uniqueDeviceIdentifier);

                // Change state.
                context.getWorkspaceBytes().clear();
                context.setActiveState(new ExpansionBytesState());
                break;
            default:
                // do nothing while we continue to build up our bytes.
            }
        }
    }

    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) {

        DeviceAddress deviceAddress = hartPdu.getDeviceAddress();

        byte firstByte = 0x00;

        // Set the master address bit.
        if (deviceAddress.isPrimaryMaster()) {
            firstByte |= (byte) (0x01 << 7);
        }

        // Set the field device bit.
        if (deviceAddress.isDeviceInBurstMode()) {
            firstByte |= (byte) (0x01 << 6);
        }

        // set the Manufacturer ID or 6 bit polling address. This
        firstByte |= MASK_6_BITS
                & (deviceAddress.getAddressType() == AddressType.IS_POLLING_0_BYTE
                        ? ((ShortFrameAddress) deviceAddress).getPollingAddress()
                        : (((LongFrameAddress) deviceAddress).getExpandedDeviceTypeCode() & DEVICE_UPPER_6_BITS_MASK) >> 8);

        context.getOutputBytes().add(firstByte);

        if (deviceAddress.getAddressType() == AddressType.IS_UNIQUE_5_BYTE) {
            // set the Device Type
            context.getOutputBytes().add(
                    (byte) (((LongFrameAddress) deviceAddress).getExpandedDeviceTypeCode() & DEVICE_LOWER_8_BITS_MASK));

            // set the three byte unique device identifier.
            byte[] longAddress = ((LongFrameAddress) deviceAddress).getUniqueDeviceIdentifier();
            for (Byte addressByte : longAddress) {
                context.getOutputBytes().add(addressByte);
            }
        }

        context.setActiveState(new ExpansionBytesState());
    }

}
