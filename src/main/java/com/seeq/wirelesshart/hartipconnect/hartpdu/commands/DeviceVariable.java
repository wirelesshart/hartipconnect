package com.seeq.wirelesshart.hartipconnect.hartpdu.commands;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.EngineeringUnitsType;

/**
 * A class to represent device variables (eg. PV, SV, TV, QV, as defined by the HART Communications Foundation).
 */
public class DeviceVariable {
    /**
     * Engineering units, as defined by the HART Communications Foundation.
     */
    EngineeringUnitsType units;

    /**
     * Value for the variable.
     */
    float value;

    /**
     * Constructor using units and value.
     * 
     * @param units
     *            Engineering Units, as defined by the HART Communications Foundation.
     * @param value
     *            The Value.
     */
    public DeviceVariable(float value, EngineeringUnitsType units) {
        this.units = units;
        this.value = value;
    }

    /**
     * Constructor.
     * 
     * This initializes the value to NaN, and units to "unknown" ({@link EngineeringUnitsType.UNITS_252})
     */
    public DeviceVariable() {
        this(Float.NaN, EngineeringUnitsType.UNITS_252);
    }

    /**
     * Given an array of bytes, create a new {@link DeviceVariable} instance. The sequence should contain five bytes,
     * containing one byte for the units, four bytes for a float denoting the process value.
     * 
     * @param bytesToProcess
     *            A list of bytes to process. This must be identically 5 bytes long and should have the units byte,
     *            followed by four bytes which assemble into the float value.
     * @return A new instance of a device variable.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    public static DeviceVariable fromBytes(List<Byte> bytesToProcess) throws EngineeringUnitsException {
        if (bytesToProcess.size() != 5) {
            throw new IllegalArgumentException("Expected 5 bytes. Received " + bytesToProcess.size());
        }

        EngineeringUnitsType units = EngineeringUnitsType.fromEngineeringUnitsByte(bytesToProcess.get(0));
        float value = HartUtilities.floatFromBytes(HartUtilities.byteListToByteArray(bytesToProcess.subList(1, 5)));

        return new DeviceVariable(value, units);
    }

    /**
     * Convert the internal state of this class instance into a byte array. The sequence should contain five bytes,
     * containing one byte for the units, four bytes for a float denoting the process value.
     * 
     * @return The byte array.
     */
    public List<Byte> toBytes() {
        List<Byte> outArray = new ArrayList<>();
        outArray.add(this.units.toEngineeringUnitsByte());
        outArray.addAll(HartUtilities.byteArrayToByteList(HartUtilities.floatToByteArray(this.value)));
        return outArray;
    }

    @Override
    public String toString() {
        return "DeviceVariable [" + this.value + " " + this.units.getShortUnits() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.units == null) ? 0 : this.units.hashCode());
        result = prime * result + Float.floatToIntBits(this.value);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        DeviceVariable other = (DeviceVariable) obj;
        if (this.units != other.units) {
            return false;
        }
        // Special case. Let's treat an uninitialized device variable as being "equivalent".
        if (Float.isNaN(this.value)) {
            if (Float.isNaN(other.value)) {
                return true;
            } else {
                return false;
            }
        }
        if (this.value != other.value) {
            return false;
        }
        return true;
    }

    /**
     * Get the Engineering Units, as defined by the HART Communications Foundation.
     * 
     * @return The {@link EngineeringUnitsType} instance.
     */
    public EngineeringUnitsType getUnits() {
        return this.units;
    }

    /**
     * Set the Engineering Units, as defined by the HART Communications Foundation.
     * 
     * @param units
     *            The {@link EngineeringUnitsType} instance.
     */
    public void setUnits(EngineeringUnitsType units) {
        this.units = units;
    }

    /**
     * Get the process value.
     * 
     * @return The process value.
     */
    public float getValue() {
        return this.value;
    }

    /**
     * Set the process value.
     * 
     * @param value
     *            The process value.
     */
    public void setValue(float value) {
        this.value = value;
    }

}
