package com.seeq.wirelesshart.hartipconnect.hartpdu.commands;

import java.util.Map;
import java.util.TreeMap;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.CommandException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command074Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command084Request;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command084Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericNoArgCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command000Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command003Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command020Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * Supported command types from the Protocol. The name is in the format of "COMMAND_***" where '***' denotes a HART
 * Command Number from 0 to 254.
 * 
 * A list of the recognized commands for which we have a corresponding Request and Response class. Third party devices
 * can further register their request and response classes here (see {@link #registerRequestClass(short, Class)} and
 * {@link #registerResponseClass(short, Class)}) so that the encoding/decoding engine knows to call them during the
 * encoding/decoding process, respectively.
 */
public enum CommandRegistry {

    COMMAND_000("Read Unique Identifier", GenericNoArgCommandData.class, Command000Response.class),
    COMMAND_001("Not Implemented"),
    COMMAND_002("Not Implemented"),
    COMMAND_003("Read Dynamic Variables and Loop Current", GenericNoArgCommandData.class, Command003Response.class),
    COMMAND_004("Not Implemented"),
    COMMAND_005("Not Implemented"),
    COMMAND_006("Not Implemented"),
    COMMAND_007("Not Implemented"),
    COMMAND_008("Not Implemented"),
    COMMAND_009("Read Device Variables with Status"),
    COMMAND_010("Not Implemented"),
    COMMAND_011("Read Unique Identifier Associated with Tag"),
    COMMAND_012("Not Implemented"),
    COMMAND_013("Not Implemented"),
    COMMAND_014("Not Implemented"),
    COMMAND_015("Not Implemented"),
    COMMAND_016("Not Implemented"),
    COMMAND_017("Not Implemented"),
    COMMAND_018("Not Implemented"),
    COMMAND_019("Not Implemented"),
    COMMAND_020("Read Long Tag", GenericNoArgCommandData.class, Command020Response.class),
    COMMAND_021("Read Unique Identifier Associated with Long Tag"),
    COMMAND_022("Not Implemented"),
    COMMAND_023("Not Implemented"),
    COMMAND_024("Not Implemented"),
    COMMAND_025("Not Implemented"),
    COMMAND_026("Not Implemented"),
    COMMAND_027("Not Implemented"),
    COMMAND_028("Not Implemented"),
    COMMAND_029("Not Implemented"),
    COMMAND_030("Not Implemented"),
    COMMAND_031("Not Implemented"),
    COMMAND_032("Not Implemented"),
    COMMAND_033("Not Implemented"),
    COMMAND_034("Not Implemented"),
    COMMAND_035("Not Implemented"),
    COMMAND_036("Not Implemented"),
    COMMAND_037("Not Implemented"),
    COMMAND_038("Not Implemented"),
    COMMAND_039("Not Implemented"),
    COMMAND_040("Not Implemented"),
    COMMAND_041("Not Implemented"),
    COMMAND_042("Not Implemented"),
    COMMAND_043("Not Implemented"),
    COMMAND_044("Not Implemented"),
    COMMAND_045("Not Implemented"),
    COMMAND_046("Not Implemented"),
    COMMAND_047("Not Implemented"),
    COMMAND_048("Read Additional Device Status"),
    COMMAND_049("Not Implemented"),
    COMMAND_050("Not Implemented"),
    COMMAND_051("Not Implemented"),
    COMMAND_052("Not Implemented"),
    COMMAND_053("Not Implemented"),
    COMMAND_054("Not Implemented"),
    COMMAND_055("Not Implemented"),
    COMMAND_056("Not Implemented"),
    COMMAND_057("Not Implemented"),
    COMMAND_058("Not Implemented"),
    COMMAND_059("Not Implemented"),
    COMMAND_060("Not Implemented"),
    COMMAND_061("Not Implemented"),
    COMMAND_062("Not Implemented"),
    COMMAND_063("Not Implemented"),
    COMMAND_064("Not Implemented"),
    COMMAND_065("Not Implemented"),
    COMMAND_066("Not Implemented"),
    COMMAND_067("Not Implemented"),
    COMMAND_068("Not Implemented"),
    COMMAND_069("Not Implemented"),
    COMMAND_070("Not Implemented"),
    COMMAND_071("Not Implemented"),
    COMMAND_072("Not Implemented"),
    COMMAND_073("Find Device"),
    COMMAND_074("Read I/O Subsystem Capabilities", GenericNoArgCommandData.class, Command074Response.class),
    COMMAND_075("Poll Sub Device"),
    COMMAND_076("Not Implemented"),
    COMMAND_077("Not Implemented"),
    COMMAND_078("Not Implemented"),
    COMMAND_079("Not Implemented"),
    COMMAND_080("Not Implemented"),
    COMMAND_081("Not Implemented"),
    COMMAND_082("Not Implemented"),
    COMMAND_083("Not Implemented"),
    COMMAND_084("Read Sub-Device Identity Summary", Command084Request.class, Command084Response.class),
    COMMAND_085("Not Implemented"),
    COMMAND_086("Not Implemented"),
    COMMAND_087("Not Implemented"),
    COMMAND_088("Not Implemented"),
    COMMAND_089("Not Implemented"),
    COMMAND_090("Not Implemented"),
    COMMAND_091("Not Implemented"),
    COMMAND_092("Not Implemented"),
    COMMAND_093("Not Implemented"),
    COMMAND_094("Not Implemented"),
    COMMAND_095("Not Implemented"),
    COMMAND_096("Not Implemented"),
    COMMAND_097("Not Implemented"),
    COMMAND_098("Not Implemented"),
    COMMAND_099("Not Implemented"),
    COMMAND_100("Not Implemented"),
    COMMAND_101("Not Implemented"),
    COMMAND_102("Not Implemented"),
    COMMAND_103("Not Implemented"),
    COMMAND_104("Not Implemented"),
    COMMAND_105("Not Implemented"),
    COMMAND_106("Not Implemented"),
    COMMAND_107("Not Implemented"),
    COMMAND_108("Not Implemented"),
    COMMAND_109("Not Implemented"),
    COMMAND_110("Not Implemented"),
    COMMAND_111("Not Implemented"),
    COMMAND_112("Not Implemented"),
    COMMAND_113("Not Implemented"),
    COMMAND_114("Not Implemented"),
    COMMAND_115("Not Implemented"),
    COMMAND_116("Not Implemented"),
    COMMAND_117("Not Implemented"),
    COMMAND_118("Not Implemented"),
    COMMAND_119("Not Implemented"),
    COMMAND_120("Not Implemented"),
    COMMAND_121("Not Implemented"),
    COMMAND_122("Not Implemented"),
    COMMAND_123("Not Implemented"),
    COMMAND_124("Not Implemented"),
    COMMAND_125("Not Implemented"),
    COMMAND_126("Not Implemented"),
    COMMAND_127("Not Implemented"),
    COMMAND_128("Not Implemented"),
    COMMAND_129("Not Implemented"),
    COMMAND_130("Not Implemented"),
    COMMAND_131("Not Implemented"),
    COMMAND_132("Not Implemented"),
    COMMAND_133("Not Implemented"),
    COMMAND_134("Not Implemented"),
    COMMAND_135("Not Implemented"),
    COMMAND_136("Not Implemented"),
    COMMAND_137("Not Implemented"),
    COMMAND_138("Not Implemented"),
    COMMAND_139("Not Implemented"),
    COMMAND_140("Not Implemented"),
    COMMAND_141("Not Implemented"),
    COMMAND_142("Not Implemented"),
    COMMAND_143("Not Implemented"),
    COMMAND_144("Not Implemented"),
    COMMAND_145("Not Implemented"),
    COMMAND_146("Not Implemented"),
    COMMAND_147("Not Implemented"),
    COMMAND_148("Not Implemented"),
    COMMAND_149("Not Implemented"),
    COMMAND_150("Not Implemented"),
    COMMAND_151("Not Implemented"),
    COMMAND_152("Not Implemented"),
    COMMAND_153("Not Implemented"),
    COMMAND_154("Not Implemented"),
    COMMAND_155("Not Implemented"),
    COMMAND_156("Not Implemented"),
    COMMAND_157("Not Implemented"),
    COMMAND_158("Not Implemented"),
    COMMAND_159("Not Implemented"),
    COMMAND_160("Not Implemented"),
    COMMAND_161("Not Implemented"),
    COMMAND_162("Not Implemented"),
    COMMAND_163("Not Implemented"),
    COMMAND_164("Not Implemented"),
    COMMAND_165("Not Implemented"),
    COMMAND_166("Not Implemented"),
    COMMAND_167("Not Implemented"),
    COMMAND_168("Not Implemented"),
    COMMAND_169("Not Implemented"),
    COMMAND_170("Not Implemented"),
    COMMAND_171("Not Implemented"),
    COMMAND_172("Not Implemented"),
    COMMAND_173("Not Implemented"),
    COMMAND_174("Not Implemented"),
    COMMAND_175("Not Implemented"),
    COMMAND_176("Not Implemented"),
    COMMAND_177("Not Implemented"),
    COMMAND_178("Not Implemented"),
    COMMAND_179("Not Implemented"),
    COMMAND_180("Not Implemented"),
    COMMAND_181("Not Implemented"),
    COMMAND_182("Not Implemented"),
    COMMAND_183("Not Implemented"),
    COMMAND_184("Not Implemented"),
    COMMAND_185("Not Implemented"),
    COMMAND_186("Not Implemented"),
    COMMAND_187("Not Implemented"),
    COMMAND_188("Not Implemented"),
    COMMAND_189("Not Implemented"),
    COMMAND_190("Not Implemented"),
    COMMAND_191("Not Implemented"),
    COMMAND_192("Not Implemented"),
    COMMAND_193("Not Implemented"),
    COMMAND_194("Not Implemented"),
    COMMAND_195("Not Implemented"),
    COMMAND_196("Not Implemented"),
    COMMAND_197("Not Implemented"),
    COMMAND_198("Not Implemented"),
    COMMAND_199("Not Implemented"),
    COMMAND_200("Not Implemented"),
    COMMAND_201("Not Implemented"),
    COMMAND_202("Read Device Network Statistics"),
    COMMAND_203("Not Implemented"),
    COMMAND_204("Not Implemented"),
    COMMAND_205("Not Implemented"),
    COMMAND_206("Not Implemented"),
    COMMAND_207("Not Implemented"),
    COMMAND_208("Not Implemented"),
    COMMAND_209("Not Implemented"),
    COMMAND_210("Not Implemented"),
    COMMAND_211("Not Implemented"),
    COMMAND_212("Not Implemented"),
    COMMAND_213("Not Implemented"),
    COMMAND_214("Not Implemented"),
    COMMAND_215("Not Implemented"),
    COMMAND_216("Not Implemented"),
    COMMAND_217("Not Implemented"),
    COMMAND_218("Not Implemented"),
    COMMAND_219("Not Implemented"),
    COMMAND_220("Not Implemented"),
    COMMAND_221("Not Implemented"),
    COMMAND_222("Not Implemented"),
    COMMAND_223("Not Implemented"),
    COMMAND_224("Not Implemented"),
    COMMAND_225("Not Implemented"),
    COMMAND_226("Not Implemented"),
    COMMAND_227("Not Implemented"),
    COMMAND_228("Not Implemented"),
    COMMAND_229("Not Implemented"),
    COMMAND_230("Not Implemented"),
    COMMAND_231("Not Implemented"),
    COMMAND_232("Not Implemented"),
    COMMAND_233("Not Implemented"),
    COMMAND_234("Not Implemented"),
    COMMAND_235("Not Implemented"),
    COMMAND_236("Not Implemented"),
    COMMAND_237("Not Implemented"),
    COMMAND_238("Not Implemented"),
    COMMAND_239("Not Implemented"),
    COMMAND_240("Not Implemented"),
    COMMAND_241("Not Implemented"),
    COMMAND_242("Not Implemented"),
    COMMAND_243("Not Implemented"),
    COMMAND_244("Not Implemented"),
    COMMAND_245("Not Implemented"),
    COMMAND_246("Not Implemented"),
    COMMAND_247("Not Implemented"),
    COMMAND_248("Not Implemented"),
    COMMAND_249("Not Implemented"),
    COMMAND_250("Not Implemented"),
    COMMAND_251("Not Implemented"),
    COMMAND_252("Not Implemented"),
    COMMAND_253("Not Implemented"),
    COMMAND_254("Not Implemented");

    private final String humanString;

    /**
     * Constructor.
     * 
     * @param humanString
     *            A human readable string that describes the purpose of this command.
     * @param defaultRequestClass
     *            The class to use for requests to the device when no other factory can take claim of the device number.
     * @param defaultResponseClass
     *            The class to use for response from the device when no other factory can take claim for the device
     *            number.
     */
    private CommandRegistry(String humanString, Class<? extends CommandData> defaultRequestClass,
            Class<? extends CommandData> defaultResponseClass) {
        this.humanString = humanString;
        this.defaultRequestClass = defaultRequestClass;
        this.defaultResponseClass = defaultResponseClass;
        this.deviceRequestClasses = new TreeMap<>();
        this.deviceResponseClasses = new TreeMap<>();
    }

    /**
     * Constructor without classes. If no default classes are defined, then a generic class shall be assumed.
     * 
     * @param humanString
     *            A human readable string that describes the purpose of this command.
     */
    private CommandRegistry(String humanString) {
        this(humanString, GenericCommandData.class, GenericCommandData.class);
    }

    /**
     * Determine command type from the bit values.
     *
     * @param commandByte
     *            The command byte.
     * @return The {@link CommandRegistry} instance found inside the command byte.
     * @throws CommandException
     *             If the address type is not recognized in the incoming delimeter byte.
     */
    public static CommandRegistry fromCommandByte(byte commandByte) throws CommandException {
        for (CommandRegistry type : CommandRegistry.values()) {
            byte testByte = type.toCommandByte();

            if (testByte == commandByte) {
                return type;
            }
        }
        throw new CommandException("An unrecognized command type was found in commandByte byte: " + commandByte);
    }

    /**
     * Return a command byte.
     * 
     * @return A command byte.
     */
    public byte toCommandByte() {
        return this.getCommandID();
    }

    /**
     * Return the command ID.
     *
     * @return The command ID.
     */
    public byte getCommandID() {
        String numericPart = this.name().substring(this.name().indexOf('_') + 1);
        return Integer.valueOf(numericPart).byteValue();
    }

    /**
     * A human readable string that can be used for debugging purposes.
     *
     * @return The human readable string.
     */
    public String getHumanString() {
        return this.humanString;
    }

    /**
     * Create a new instance of this command's {@link CommandData} strategy.
     * 
     * @param frameType
     *            The frame type.
     * @param deviceAddress
     *            The device address.
     *
     * @return A new {@link CommandData} instance.
     */
    public CommandData createNewInstanceOfStrategy(FrameType frameType, DeviceAddress deviceAddress) {
        CommandDataFactory factory = new CommandDataFactory();
        return factory.create(this, frameType, deviceAddress);
    }

    /**
     * Optional registered request classes for specific devices.
     */
    private final Map<Short, Class<? extends CommandData>> deviceRequestClasses;

    /**
     * Optional registered response classes for specific devices.
     */
    private final Map<Short, Class<? extends CommandData>> deviceResponseClasses;

    /**
     * Default class to use when none is found in the device Factories list.
     */
    private final Class<? extends CommandData> defaultRequestClass;

    /**
     * Default class to use when none is found in the device Factories list.
     */
    private final Class<? extends CommandData> defaultResponseClass;

    /**
     * Find the best implementing class for the given expanded device code and frame type.
     * 
     * @param address
     *            Device address.
     * @param frameType
     *            The frame type.
     * @return The class that implements the class for the given frame type and expanded device code. If none are
     *         registered and there is no default one defined, return null.
     */
    public Class<? extends CommandData> getCommandDataClassForDevice(DeviceAddress address, FrameType frameType) {
        if (this == COMMAND_000 && address.getAddressType() == AddressType.IS_POLLING_0_BYTE) {
            if (frameType == FrameType.STX) {
                return this.defaultRequestClass;
            } else {
                return this.defaultResponseClass;
            }
        }

        final Class<? extends CommandData> implementingClass;
        if (frameType == FrameType.STX) {
            short expandedDeviceTypeCode = ((LongFrameAddress) address).getExpandedDeviceTypeCode();
            Class<? extends CommandData> testClass = this.deviceRequestClasses.get(expandedDeviceTypeCode);
            if (testClass == null) {
                implementingClass = this.defaultRequestClass;
            } else {
                implementingClass = testClass;
            }
        } else {
            short expandedDeviceTypeCode = ((LongFrameAddress) address).getExpandedDeviceTypeCode();
            Class<? extends CommandData> testClass = this.deviceResponseClasses.get(expandedDeviceTypeCode);
            if (testClass == null) {
                implementingClass = this.defaultResponseClass;
            } else {
                implementingClass = testClass;
            }
        }

        return implementingClass;
    }

    /**
     * Register a {@link CommandData} class that should be used for the specified expanded device type code during all
     * Requests.
     * 
     * @param expandedDeviceTypeCode
     *            The device's unique Expanded Device Type Code as registered with the HART Communications Foundation.
     * @param requestClass
     *            The Request class.
     */
    public void registerRequestClass(short expandedDeviceTypeCode, Class<? extends CommandData> requestClass) {
        this.deviceRequestClasses.put(expandedDeviceTypeCode, requestClass);
    }

    /**
     * Deregister any request classes for the given Expanded Device Type Code.
     * 
     * @param expandedDeviceTypeCode
     *            The device's unique Expanded Device Type Code as registered with the HART Communications Foundation.
     */
    public void deregisterRequestClass(short expandedDeviceTypeCode) {
        this.deviceRequestClasses.remove(expandedDeviceTypeCode);
    }

    /**
     * Register a {@link CommandData} class that should be used for the specified expanded device type code during all
     * Responses.
     * 
     * @param expandedDeviceTypeCode
     *            The device's unique Expanded Device Type Code as registered with the HART Communications Foundation.
     * @param responseClass
     *            The Response class.
     */
    public void registerResponseClass(short expandedDeviceTypeCode, Class<? extends CommandData> responseClass) {
        this.deviceResponseClasses.put(expandedDeviceTypeCode, responseClass);
    }

    /**
     * Deregister any response classes for the given Expanded Device Type Code.
     * 
     * @param expandedDeviceTypeCode
     *            The device's unique Expanded Device Type Code as registered with the HART Communications Foundation.
     */
    public void deregisterResponseClass(short expandedDeviceTypeCode) {
        this.deviceResponseClasses.remove(expandedDeviceTypeCode);
    }
}
