package com.seeq.wirelesshart.hartipconnect.hartpdu.addresses;

import java.util.Arrays;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

/**
 * This class represents a long frame address. A long frame address uses 5 bytes in the HART protocol. It contains
 * information about whether the calling device is the primary or secondary master, whether the device is in burst mode,
 * the manufacturer's unique ID, the manufacturer's device's unique id, and the unique address.
 */
public class LongFrameAddress implements DeviceAddress {

    private final AddressType addressType = AddressType.IS_UNIQUE_5_BYTE;
    private boolean isPrimaryMaster = true;
    private boolean isDeviceInBurstMode = false;
    private short expandedDeviceTypeCode = 0x0000;
    private byte[] uniqueDeviceIdentifier;

    /**
     * Get the Expanded Device Type Code as defined by the HART Communications Foundation.
     * 
     * @return The Expanded Device Type Code.
     */
    public short getExpandedDeviceTypeCode() {
        return this.expandedDeviceTypeCode;
    }

    /**
     * Set the Expanded Device Type Code as defined by the HART Communication Foundation.
     * 
     * @param expandedDeviceTypeCode
     *            The Expanded Device Type Code.
     */
    public void setExpandedDeviceTypeCode(short expandedDeviceTypeCode) {
        this.expandedDeviceTypeCode = expandedDeviceTypeCode;
    }

    /**
     * Get the three byte unique device identifier, as specified in Spec 81 of the HART Protocol Specification.
     * 
     * @return Get the device's address.
     */
    public byte[] getUniqueDeviceIdentifier() {
        return this.uniqueDeviceIdentifier;
    }

    /**
     * Set the device's three byte unique device identifier, as specified in Spec 81 of the HART Protocol Specification.
     * 
     * @param uniqueDeviceIdentifier
     *            The device's three byte address.
     * @throws IllegalArgumentException
     *             If the input unique device identifier is null or does not have the expected length of 3 bytes.
     */
    public void setUniqueDeviceIdentifier(byte[] uniqueDeviceIdentifier) throws IllegalArgumentException {
        if (uniqueDeviceIdentifier == null || uniqueDeviceIdentifier.length != 3) {
            throw new IllegalArgumentException("The Unique Device Identifier must be 3 bytes long. Received "
                    + Arrays.toString(uniqueDeviceIdentifier));
        }
        this.uniqueDeviceIdentifier = uniqueDeviceIdentifier;
    }

    @Override
    public AddressType getAddressType() {
        return this.addressType;
    }

    @Override
    public boolean isPrimaryMaster() {
        return this.isPrimaryMaster;
    }

    @Override
    public void setPrimaryMaster(boolean isPrimaryMaster) {
        this.isPrimaryMaster = isPrimaryMaster;
    }

    @Override
    public boolean isDeviceInBurstMode() {
        return this.isDeviceInBurstMode;
    }

    @Override
    public void setDeviceInBurstMode(boolean isDeviceInBurstMode) {
        this.isDeviceInBurstMode = isDeviceInBurstMode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.addressType.hashCode();
        result = prime * result + this.expandedDeviceTypeCode;
        result = prime * result + (this.isDeviceInBurstMode ? 1231 : 1237);
        result = prime * result + (this.isPrimaryMaster ? 1231 : 1237);
        result = prime * result + Arrays.hashCode(this.uniqueDeviceIdentifier);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        }

        LongFrameAddress other = (LongFrameAddress) obj;

        if (this.isDeviceInBurstMode != other.isDeviceInBurstMode) {
            return false;
        } else if (this.isPrimaryMaster != other.isPrimaryMaster) {
            return false;
        } else if (this.expandedDeviceTypeCode != other.expandedDeviceTypeCode) {
            return false;
        } else if (!Arrays.equals(this.uniqueDeviceIdentifier, other.uniqueDeviceIdentifier)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return "LongFrameAddress ["
                + "addressType=" + this.addressType
                + ", isPrimaryMaster=" + this.isPrimaryMaster
                + ", isDeviceInBurstMode=" + this.isDeviceInBurstMode
                + ", expandedDeviceTypeCode=" + this.expandedDeviceTypeCode
                + ", uniqueDeviceIdentifier=" + Arrays.toString(this.uniqueDeviceIdentifier) + "]";
    }

}
