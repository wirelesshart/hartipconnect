package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;

/**
 * This class is used to maintain the state of the processing engine.
 */
public class StateContextForEncoder extends StateContext {
    /**
     * Preallocation of memory for the List<Byte> members, for performance reasons.
     */
    private static final int INITIAL_MEMORY_ALLOCATION = 1024;

    /**
     * An output List<Byte> to hold the encoded packet.
     */
    private final List<Byte> outputBytes;

    /**
     * The packet to be encoded into a byte array.
     */
    private HartPdu inputHartPdu;

    /**
     * Constructor.
     */
    public StateContextForEncoder() {
        this.outputBytes = new ArrayList<>(INITIAL_MEMORY_ALLOCATION);
    }

    /**
     * Return the hart pdu packet which is being encoded into bytes.
     *
     * @return The hart-pdu packet.
     */
    public HartPdu getInputHartPdu() {
        return this.inputHartPdu;
    }

    /**
     * Set the hart pdu packet. This is used by the States during the encoding or decoding process, as needed. It is up
     * to the States to manage the state of this instance.
     *
     * @param hartPdu
     *            The hart pdu.
     */
    public void setInputHartPdu(HartPdu hartPdu) {
        this.inputHartPdu = hartPdu;
    }

    /**
     * Obtain the total set of bytes received/encoded in the packet. At the end of encoding, this should contain the
     * byte representation of the input {@link HartPdu} instance. Conversely, at the end of the decoding process, this
     * should contain the input byte representation of the output {@link HartPdu} instance.
     * 
     * @return The total set of bytes in the packet.
     */
    public List<Byte> getOutputBytes() {
        return this.outputBytes;
    }
}
