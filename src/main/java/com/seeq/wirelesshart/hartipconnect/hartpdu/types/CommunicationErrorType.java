package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.seeq.wirelesshart.hartipconnect.exceptions.communication.BufferOverflowException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.FramingErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.HartCommunicationException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.OverrunErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.VerticalParityErrorException;

/**
 * A representation of all the known errors that can be reported by a remote device.
 */
public enum CommunicationErrorType {

    /**
     * A general communication error was detected. This bit must always be defined if any type of communication error is
     * detected.
     */
    COMMUNICATION_ERROR(0x80, "General Communication Error", HartCommunicationException.class),

    /**
     * An odd parity does not match the corresponding transmitted byte in packet received by device.
     */
    VERTICAL_PARITY_ERROR(0x40, "Vertical Parity Error", VerticalParityErrorException.class),

    /**
     * At least one byte in the receive buffer in the device's UART was overwritten before it was read.
     */
    OVERRUN_ERROR(0x20, "Overrun Error", OverrunErrorException.class),

    /**
     * The stop bit of one or more bytes received by the device was not detected in the frame.
     */
    FRAMING_ERROR(0x10, "Framing Error", FramingErrorException.class),

    /**
     * The longitudinal parity calculated byte does not match the check byte at the end of the message.
     */
    LONGITUDINAL_PARITY_ERROR(0x08, "Longitudinal Parity Error", LongitudinalParityErrorException.class),

    /**
     * The message was too long for the receive buffer in the device.
     */
    BUFFER_OVERFLOW(0x02, "Buffer Overflow", BufferOverflowException.class);

    private final int errorValue;
    private final String humanReadableString;
    private final Class<? extends HartCommunicationException> exceptionClass;

    /**
     * Private constructor.
     * 
     * @param value
     *            The value as defined in the HART protocol.
     * @param humanString
     *            A human readable string that describes the error.
     * @param exceptionClass
     *            The exception class that will be thrown when this error is encountered.
     */
    private CommunicationErrorType(int value, String humanString,
            Class<? extends HartCommunicationException> exceptionClass) {
        this.errorValue = value;
        this.humanReadableString = humanString;
        this.exceptionClass = exceptionClass;
    }

    /**
     * The numerical error code as defined in the HART Protocol.
     *
     * @return The error value as defined in the HART Protocol.
     */
    public int getErrorValue() {
        return this.errorValue;
    }

    /**
     * A human readable string for the error.
     *
     * @return The human readable string representation of the error.
     */
    public String getHumanReadableString() {
        return this.humanReadableString;
    }

    /**
     * The class type representing this error.
     *
     * @return The class type.
     */
    public Class<? extends HartCommunicationException> getExceptionClass() {
        return this.exceptionClass;
    }

    /**
     * Create a new instance of the exception.
     * 
     * @param message
     *            An optional message. If null, the {@link CommunicationErrorType#getHumanReadableString()} string shall
     *            be used.
     * @param cause
     *            The cause for the exception. If null, then none shall be used.
     *
     * @return A new Exception instance.
     */
    public HartCommunicationException createExceptionInstance(String message, Throwable cause) {
        try {
            Constructor<? extends HartCommunicationException> constructor = this.exceptionClass
                    .getDeclaredConstructor(String.class, Throwable.class);
            constructor.setAccessible(true);

            Object[] argumentList = new Object[] {
                    (message == null || message.isEmpty()) ? this.humanReadableString : message, cause };

            HartCommunicationException newInstance = constructor.newInstance(argumentList);

            return newInstance;
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(CommunicationErrorType.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalStateException("Unable to instantiate exception. This should not happen");
        }
    }

    /**
     * Find all the error conditions inside of the supplied communication error byte.
     *
     * @param communicationErrorByte
     *            The byte containing the communication error.
     *
     * @return List of discovered communication errors.
     * 
     */
    public static CommunicationErrorType[] fromCommunicationErrorByte(byte communicationErrorByte) {
        List<CommunicationErrorType> errorSet = new ArrayList<>(CommunicationErrorType.values().length);

        if (isErrorInErrorByte(communicationErrorByte)) {
            for (CommunicationErrorType type : CommunicationErrorType.values()) {
                if ((communicationErrorByte & (type.getErrorValue() & 0xff)) == type.getErrorValue()) {
                    errorSet.add(type);
                }
            }
        }

        return errorSet.toArray(new CommunicationErrorType[errorSet.size()]);
    }

    /**
     * Convert the communication errors into a communication error byte.
     * 
     * @param communicationErrors
     *            A list of communication errors. Note that the {@link CommunicationErrorType#COMMUNICATION_ERROR} error
     *            type must be in the list, otherwise this will throw an {@link IllegalArgumentException}.
     * @return A communication error byte.
     */
    public static byte toCommunicationErrorByte(CommunicationErrorType[] communicationErrors) {
        byte communicationErrorByte = 0x00;

        for (CommunicationErrorType communicationErrorType : communicationErrors) {
            communicationErrorByte |= communicationErrorType.getErrorValue();
        }

        if (!isErrorInErrorByte(communicationErrorByte)) {
            throw new IllegalArgumentException("This is not a valid communication error byte: "
                    + communicationErrorByte);
        }

        return communicationErrorByte;
    }

    /**
     * Determine if the supplied status byte contains an error condition.
     *
     * @param testByte
     *            The status byte being tested.
     * @return True if the General Communication Error bit is detected.
     */
    public static boolean isErrorInErrorByte(byte testByte) {
        return (testByte & (COMMUNICATION_ERROR.getErrorValue() & 0xff)) == COMMUNICATION_ERROR.getErrorValue();
    }

}
