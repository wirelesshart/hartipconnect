package com.seeq.wirelesshart.hartipconnect.hartpdu.commands;

import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericNoArgCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * For a specific command number (for example Command 3) and the device's Expanded Device Type Code, establish the most
 * appropriate Response/Request class to use. It does this by referencing the {@link CommandRegistry} and obtaining the
 * best set of strategy classes to use for a specific command number and device.
 */
public class CommandDataFactory {

    /**
     * Create a new {@link CommandData} instance.
     * <p>
     * For a specific command number (for example Command 3) and the device's Expanded Device Type Code, establish the
     * most appropriate Response/Request class to use. It does this by referencing the {@link CommandRegistry} and
     * obtaining the best set of strategy classes to use for a specific command number and device.
     * 
     * @param commandRegistry
     *            The {@link CommandRegistry} instance.
     * @param frameType
     *            The {@link FrameType} instance.
     * @param address
     *            The {@link DeviceAddress} instance. This contains the device's Expanded Device Type Code.
     * @return A new instance of a {@link CommandData} Request/Response class.
     */
    public CommandData create(CommandRegistry commandRegistry, FrameType frameType, DeviceAddress address) {
        // The HART protocol presently defines Short Frame Addresses exclusively for Command 0 and no others.
        if (address.getAddressType() == AddressType.IS_POLLING_0_BYTE && commandRegistry != CommandRegistry.COMMAND_000) {
            throw new IllegalArgumentException("Only command 0 supports the short frame polling addresses");
        }

        // Instantiate the best command, as registered in our command registry.
        try {
            Class<? extends CommandData> implementingClass = commandRegistry.getCommandDataClassForDevice(address,
                    frameType);
            if (implementingClass == GenericNoArgCommandData.class) {
                return new GenericNoArgCommandData(commandRegistry, frameType);
            } else if (implementingClass == GenericCommandData.class
                    || implementingClass == null) {
                return new GenericCommandData(commandRegistry, frameType);
            } else {
                return implementingClass.newInstance();
            }
        } catch (IllegalStateException | InstantiationException | IllegalAccessException e) {
            // We can't identify a specific command data instance. Create a generic one.
            return new GenericCommandData(commandRegistry, frameType);
        }
    }
}
