package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * A generic command packet for Command 0 -- Read Unique Identifier. This is the Data Link Layer Management Command. In
 * general, it returns the Expanded Device Type Code, Revision Levels, and Device Identification Number.
 * 
 * The full encoding is as follows:
 * <ul>
 * <li>1B response code</li>
 * <li>2B expanded device type code</li>
 * <li>1B number of request preambles</li>
 * <li>1B universal revision level</li>
 * <li>1B transmitter revision level</li>
 * <li>1B software revision level</li>
 * <li>1B hardware revision level</li>
 * <li>1B device flags</li>
 * <li>3B unique device identification code</li>
 * <li>1B minimum number of response preambles</li>
 * <li>1B maximum number of device variables</li>
 * <li>2B configuration change number</li>
 * <li>1B extended field device status</li>
 * <li>2B manufacturer identification code</li>
 * <li>2B private label distributor</li>
 * <li>1B device profile</li>
 * </ul>
 * for a total of 22 bytes.
 * 
 * This class implements the Response strategy class for this command.
 */
public class Command000Response implements CommandData {
    private static final int NUM_BYTES_IN_PACKET = 22;

    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_000;
    private final FrameType frameType = FrameType.ACK;

    private short expandedDeviceTypeCode;

    private byte responseCode;

    private byte numRequestPreambles;

    private byte universalRevisionLevel;

    private byte transmitterRevisionLevel;

    private byte softwareRevision;

    private byte hardwareRevisionLevel;

    private byte flags;

    private byte[] uniqueDeviceIdentificationCode = new byte[] { 0, 0, 0 };

    private byte minNumResponsePreambles;

    private byte maxNumOfDeviceVariables;

    private short configurationChangeCounter;

    private byte extendedFieldDeviceStatus;

    private short manufacturerIdentificationCode;

    private short privateLabelDistributor;

    private byte deviceProfile;

    public Command000Response() {}

    /**
     * Return the entire internal byte representation for this {@link CommandData}. The encoding is described
     * {@link Command000Response here}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();
        encodedBytes.add(this.responseCode);
        encodedBytes
                .addAll(HartUtilities.byteArrayToByteList(HartUtilities.shortToByteArray(this.expandedDeviceTypeCode)));
        encodedBytes.add(this.numRequestPreambles);
        encodedBytes.add(this.universalRevisionLevel);
        encodedBytes.add(this.transmitterRevisionLevel);
        encodedBytes.add(this.softwareRevision);
        encodedBytes.add(this.hardwareRevisionLevel);
        encodedBytes.add(this.flags);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(this.uniqueDeviceIdentificationCode));
        encodedBytes.add(this.minNumResponsePreambles);
        encodedBytes.add(this.maxNumOfDeviceVariables);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.configurationChangeCounter)));
        encodedBytes.add(this.extendedFieldDeviceStatus);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.manufacturerIdentificationCode)));
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.privateLabelDistributor)));
        encodedBytes.add(this.deviceProfile);
        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}. The encoding is described
     * {@link Command000Response here}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) {
        if (bytesToDecode.size() != NUM_BYTES_IN_PACKET) {
            throw new IllegalArgumentException("Expected " + NUM_BYTES_IN_PACKET + " bytes. Received "
                    + bytesToDecode.size());
        }

        this.responseCode = bytesToDecode.get(0);
        this.expandedDeviceTypeCode = HartUtilities.shortFromBytes(HartUtilities.byteListToByteArray(bytesToDecode
                .subList(1, 3)));
        this.numRequestPreambles = bytesToDecode.get(3);
        this.universalRevisionLevel = bytesToDecode.get(4);
        this.transmitterRevisionLevel = bytesToDecode.get(5);
        this.softwareRevision = bytesToDecode.get(6);
        this.hardwareRevisionLevel = bytesToDecode.get(7);
        this.flags = bytesToDecode.get(8);
        this.uniqueDeviceIdentificationCode = HartUtilities.byteListToByteArray(bytesToDecode.subList(9, 12));
        this.minNumResponsePreambles = bytesToDecode.get(12);
        this.maxNumOfDeviceVariables = bytesToDecode.get(13);
        this.configurationChangeCounter = HartUtilities.shortFromBytes(HartUtilities.byteListToByteArray(bytesToDecode
                .subList(14, 16)));
        this.extendedFieldDeviceStatus = bytesToDecode.get(16);
        this.manufacturerIdentificationCode = HartUtilities.shortFromBytes(HartUtilities
                .byteListToByteArray(bytesToDecode.subList(17, 19)));
        this.privateLabelDistributor = HartUtilities.shortFromBytes(HartUtilities.byteListToByteArray(bytesToDecode
                .subList(19, 21)));
        this.deviceProfile = bytesToDecode.get(21);
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return NUM_BYTES_IN_PACKET;
    }

    /**
     * Get the Expanded Device Type Code as defined by the HART Communication Foundation (HCF).
     * 
     * @return The unique expanded device type code for the queried device.
     */
    public short getExpandedDeviceTypeCode() {
        return this.expandedDeviceTypeCode;
    }

    /**
     * Set the Expanded Device Type Code as defined by the HART Communication Foundation (HCF).
     * 
     * @param expandedDeviceTypeCode
     *            The unique expanded device type code for the queried device.
     */
    public void setExpandedDeviceTypeCode(short expandedDeviceTypeCode) {
        this.expandedDeviceTypeCode = expandedDeviceTypeCode;
    }

    /**
     * Get the response code.
     * 
     * @return The response code.
     */
    public byte getResponseCode() {
        return this.responseCode;
    }

    /**
     * Set the response code.
     * 
     * @param responseCode
     *            The response code.
     */
    public void setResponseCode(byte responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * Get the number of request preambles.
     * 
     * @return The number of request preambles.
     */
    public byte getNumRequestPreambles() {
        return this.numRequestPreambles;
    }

    /**
     * Set the number of request preambles.
     * 
     * @param numRequestPreambles
     *            The number of request preambles.
     */
    public void setNumRequestPreambles(byte numRequestPreambles) {
        this.numRequestPreambles = numRequestPreambles;
    }

    /**
     * Get the device's universal revision level.
     * 
     * @return The universal revision level.
     */
    public byte getUniversalRevisionLevel() {
        return this.universalRevisionLevel;
    }

    /**
     * Set the universal revision level.
     * 
     * @param universalRevisionLevel
     *            The device's universal revision level.
     */
    public void setUniversalRevisionLevel(byte universalRevisionLevel) {
        this.universalRevisionLevel = universalRevisionLevel;
    }

    /**
     * Get the Transmitter's Revision level.
     * 
     * @return The revision level.
     */
    public byte getTransmitterRevisionLevel() {
        return this.transmitterRevisionLevel;
    }

    /**
     * Set the Transmitter's Revision Level.
     * 
     * @param transmitterRevisionLevel
     *            The revision level.
     */
    public void setTransmitterRevisionLevel(byte transmitterRevisionLevel) {
        this.transmitterRevisionLevel = transmitterRevisionLevel;
    }

    /**
     * Get the device's software revision.
     * 
     * @return The device's software revision.
     */
    public byte getSoftwareRevision() {
        return this.softwareRevision;
    }

    /**
     * Set the device's software revision.
     * 
     * @param softwareRevision
     *            The software revision.
     */
    public void setSoftwareRevision(byte softwareRevision) {
        this.softwareRevision = softwareRevision;
    }

    /**
     * Get the device's hardware revision level.
     * 
     * @return The device's hardware revision level.
     */
    public byte getHardwareRevisionLevel() {
        return this.hardwareRevisionLevel;
    }

    /**
     * Set the hardware's revision level.
     * 
     * @param hardwareRevisionLevel
     *            The hardware's revision level.
     */
    public void setHardwareRevisionLevel(byte hardwareRevisionLevel) {
        this.hardwareRevisionLevel = hardwareRevisionLevel;
    }

    /**
     * Get device flags.
     * 
     * @return The device's flags.
     */
    public byte getFlags() {
        return this.flags;
    }

    /**
     * Set the device's flags.
     * 
     * @param flags
     *            The device's flags.
     */
    public void setFlags(byte flags) {
        this.flags = flags;
    }

    /**
     * Get the unique device's identification code. This, combined with the 6 lower bits of the manufacturer's unique
     * identifier as defined by the HART Communication Foundation combine to build the Expanded Device Type Code.
     * 
     * @return The unique device identification code.
     */
    public byte[] getUniqueDeviceIdentificationCode() {
        return this.uniqueDeviceIdentificationCode;
    }

    /**
     * Set the unique device's identification code. This, combined with the 6 lower bits of the manufacturer's unique
     * identifier as defined by the HART Communication Foundation combine to build the Expanded Device Type Code.
     * 
     * @param deviceIdentificationCode
     *            The unique device identification code.
     */
    public void setUniqueDeviceIdentificationCode(byte[] deviceIdentificationCode) {
        this.uniqueDeviceIdentificationCode = deviceIdentificationCode;
    }

    /**
     * Get the minimum number of response preambles.
     * 
     * @return The minimum number of response preambles.
     */
    public byte getMinNumResponsePreambles() {
        return this.minNumResponsePreambles;
    }

    /**
     * Set the minimum number of response preambles.
     * 
     * @param minNumResponsePreambles
     *            The minimum number of response preambles.
     */
    public void setMinNumResponsePreambles(byte minNumResponsePreambles) {
        this.minNumResponsePreambles = minNumResponsePreambles;
    }

    /**
     * Get the maximum number of device variables.
     * 
     * @return The maximum number of device variables.
     */
    public byte getMaxNumOfDeviceVariables() {
        return this.maxNumOfDeviceVariables;
    }

    /**
     * Get the maximum number of device variables.
     * 
     * @param maxNumOfDeviceVariables
     *            The maximum number of device variables.
     */
    public void setMaxNumOfDeviceVariables(byte maxNumOfDeviceVariables) {
        this.maxNumOfDeviceVariables = maxNumOfDeviceVariables;
    }

    /**
     * Get the number of times that the device configuration has changed.
     * 
     * @return The number of times that the configuration changed.
     */
    public short getConfigurationChangeCounter() {
        return this.configurationChangeCounter;
    }

    /**
     * Set the number of times that the device configuration has changed.
     * 
     * @param configurationChangeCounter
     *            The number of times that the configuration of the device changed.
     */
    public void setConfigurationChangeCounter(short configurationChangeCounter) {
        this.configurationChangeCounter = configurationChangeCounter;
    }

    /**
     * Get the extended field device status.
     * 
     * @return The extended field device status.
     */
    public byte getExtendedFieldDeviceStatus() {
        return this.extendedFieldDeviceStatus;
    }

    /**
     * Set the extended field device status.
     * 
     * @param extendedFieldDeviceStatus
     *            The extended field device status.
     */
    public void setExtendedFieldDeviceStatus(byte extendedFieldDeviceStatus) {
        this.extendedFieldDeviceStatus = extendedFieldDeviceStatus;
    }

    /**
     * Get the manufacturer's identification code as defined by the HART Communications Foundation (HCF).
     * 
     * @return The manufacturer's identification code.
     */
    public short getManufacturerIdentificationCode() {
        return this.manufacturerIdentificationCode;
    }

    /**
     * Set the manufacturer's identification code as defined by the HART Communications Foundation (HCF).
     * 
     * @param manufacturerIdentificationCode
     *            The manufacturer's identification code.
     */
    public void setManufacturerIdentificationCode(short manufacturerIdentificationCode) {
        this.manufacturerIdentificationCode = manufacturerIdentificationCode;
    }

    /**
     * Get the unique distributor's identification code as defined by the HART Communication Foundation (HCF).
     * 
     * @return The unique distributor's identification code.
     */
    public short getPrivateLabelDistributor() {
        return this.privateLabelDistributor;
    }

    /**
     * Set the unique distributor's identification code as defined by the HART Communication Foundation (HCF).
     * 
     * @param privateLabelDistributor
     *            The unique distributor's identification code.
     */
    public void setPrivateLabelDistributor(short privateLabelDistributor) {
        this.privateLabelDistributor = privateLabelDistributor;
    }

    /**
     * Get the unique profile for this device as defined by the HART Communication Foundation (HCF).
     * 
     * @return The unique profile ID.
     */
    public byte getDeviceProfile() {
        return this.deviceProfile;
    }

    /**
     * Set the unique profile for this device as defined by the HART Communication Foundation (HCF).
     * 
     * @param deviceProfile
     *            The unique profile ID.
     */
    public void setDeviceProfile(byte deviceProfile) {
        this.deviceProfile = deviceProfile;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.commandRegistry == null) ? 0 : this.commandRegistry.hashCode());
        result = prime * result + this.configurationChangeCounter;
        result = prime * result + Arrays.hashCode(this.uniqueDeviceIdentificationCode);
        result = prime * result + this.deviceProfile;
        result = prime * result + this.expandedDeviceTypeCode;
        result = prime * result + this.extendedFieldDeviceStatus;
        result = prime * result + this.flags;
        result = prime * result + ((this.frameType == null) ? 0 : this.frameType.hashCode());
        result = prime * result + this.hardwareRevisionLevel;
        result = prime * result + this.manufacturerIdentificationCode;
        result = prime * result + this.maxNumOfDeviceVariables;
        result = prime * result + this.minNumResponsePreambles;
        result = prime * result + this.numRequestPreambles;
        result = prime * result + this.privateLabelDistributor;
        result = prime * result + this.responseCode;
        result = prime * result + this.softwareRevision;
        result = prime * result + this.transmitterRevisionLevel;
        result = prime * result + this.universalRevisionLevel;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command000Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command000Response other = (Command000Response) obj;
        return this.equals(other);
    }

    @Override
    public String toString() {
        return "Command000Response ["
                + "commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", expandedDeviceTypeCode=" + this.expandedDeviceTypeCode
                + ", responseCode=" + this.responseCode
                + ", numRequestPreambles=" + this.numRequestPreambles
                + ", universalRevisionLevel=" + this.universalRevisionLevel
                + ", transmitterRevisionLevel=" + this.transmitterRevisionLevel
                + ", softwareRevision=" + this.softwareRevision
                + ", hardwareRevisionLevel=" + this.hardwareRevisionLevel
                + ", flags=" + this.flags
                + ", uniqueDeviceIdentificationCode=" + Arrays.toString(this.uniqueDeviceIdentificationCode)
                + ", minNumResponsePreambles=" + this.minNumResponsePreambles
                + ", maxNumOfDeviceVariables=" + this.maxNumOfDeviceVariables
                + ", configurationChangeCounter=" + this.configurationChangeCounter
                + ", extendedFieldDeviceStatus=" + this.extendedFieldDeviceStatus
                + ", manufacturerIdentificationCode=" + this.manufacturerIdentificationCode
                + ", privateLabelDistributor=" + this.privateLabelDistributor
                + ", deviceProfile=" + this.deviceProfile
                + "]";
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command000Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Command000Response other = (Command000Response) obj;
        if (this.commandRegistry != other.commandRegistry) {
            return false;
        }
        if (this.configurationChangeCounter != other.configurationChangeCounter) {
            return false;
        }
        if (!Arrays.equals(this.uniqueDeviceIdentificationCode, other.uniqueDeviceIdentificationCode)) {
            return false;
        }
        if (this.deviceProfile != other.deviceProfile) {
            return false;
        }
        if (this.expandedDeviceTypeCode != other.expandedDeviceTypeCode) {
            return false;
        }
        if (this.extendedFieldDeviceStatus != other.extendedFieldDeviceStatus) {
            return false;
        }
        if (this.flags != other.flags) {
            return false;
        }
        if (this.frameType != other.frameType) {
            return false;
        }
        if (this.hardwareRevisionLevel != other.hardwareRevisionLevel) {
            return false;
        }
        if (this.manufacturerIdentificationCode != other.manufacturerIdentificationCode) {
            return false;
        }
        if (this.maxNumOfDeviceVariables != other.maxNumOfDeviceVariables) {
            return false;
        }
        if (this.minNumResponsePreambles != other.minNumResponsePreambles) {
            return false;
        }
        if (this.numRequestPreambles != other.numRequestPreambles) {
            return false;
        }
        if (this.privateLabelDistributor != other.privateLabelDistributor) {
            return false;
        }
        if (this.responseCode != other.responseCode) {
            return false;
        }
        if (this.softwareRevision != other.softwareRevision) {
            return false;
        }
        if (this.transmitterRevisionLevel != other.transmitterRevisionLevel) {
            return false;
        }
        if (this.universalRevisionLevel != other.universalRevisionLevel) {
            return false;
        }
        return true;
    }

}
