package com.seeq.wirelesshart.hartipconnect.hartpdu;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.state.StateContextForDecoder;
import com.seeq.wirelesshart.hartipconnect.hartpdu.state.StateContextForEncoder;

/**
 * This class provides convenience methods to encode/decode a {@link HartPdu} into/from a series of bytes.
 */
public class HartPduEncoder {

    /**
     * Do not instantiate this class.
     */
    private HartPduEncoder() {};

    /**
     * Encode the incoming HartPdu into a series of bytes.
     *
     * @param packetToEncode
     *            The {@link HartPdu} instance that we want to encode.
     * @return A list of bytes.
     * @throws HartException
     *             If any number of errors are encountered during the encoding process.
     */
    public static byte[] encode(HartPdu packetToEncode) throws HartException {
        packetToEncode.validate();

        StateContextForEncoder context = new StateContextForEncoder();

        while (context.getActiveState() != null) {
            context.getActiveState().encodePacket(packetToEncode, context);
        }

        return HartUtilities.byteListToByteArray(context.getOutputBytes());
    }

    /**
     * Given a byte array, decode these bytes into a {@link HartPdu} packet.
     * 
     * @param bytesToDecode
     *            The byte array to decode.
     * @return The output {@link HartPdu} packet, decoded from the incoming byte array.
     * @throws HartException
     *             If any number of errors are encountered during the decoding process.
     */
    public static HartPdu decode(byte[] bytesToDecode) throws HartException {
        StateContextForDecoder context = new StateContextForDecoder();

        for (byte newByte : bytesToDecode) {
            context.getRawIncomingBytes().add(newByte);
            context.getActiveState().decodeByte(newByte, context);
        }

        /**
         * The state machine above should have set the active state to null. If not, then something went wrong and the
         * bytes were processed incorrectly -- specifically, less bytes were received than expected.
         */
        if (context.getActiveState() != null) {
            throw new LongitudinalParityErrorException("Number of bytes less than expected. Last state executed: "
                    + context.getActiveState().getClass());
        }

        context.getOutputHartPdu().validate();

        return context.getOutputHartPdu();
    }
}
