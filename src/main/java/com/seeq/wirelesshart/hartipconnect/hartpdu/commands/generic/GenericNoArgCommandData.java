package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * This class is used as a generic {@link CommandData} packet, which models command data packets having zero arguments.
 * Specifically, this class is used as a default command, such that if no specific version of a command is defined, this
 * will act as a generic command, taking in zero bytes.
 */
public class GenericNoArgCommandData extends GenericCommandData {

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public GenericNoArgCommandData() {}

    /**
     * Constructor, with {@link FrameType} and {@link CommandRegistry} initialized.
     * 
     * @param commandRegistry
     *            The Command Type.
     * @param frameType
     *            The Frame Type.
     */
    public GenericNoArgCommandData(CommandRegistry commandRegistry, FrameType frameType) {
        super(commandRegistry, frameType);
    }

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation, or null if there is nothing to represent.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();
        // there are no arguments.
        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation. In this case, this should be an empty list or null.
     * @throws IllegalArgumentException
     *             If anything other than an empty list or null are specified.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) {
        if (bytesToDecode != null && !bytesToDecode.isEmpty()) {
            throw new IllegalArgumentException("No arguments expected, received " + bytesToDecode);
        }
        super.fromBytes(bytesToDecode);
    }

    /**
     * Set the {@link CommandRegistry} instance.
     * 
     * <b>Note:</b> This is only supported for the {@link GenericNoArgCommandData} class and must be called during
     * initialization.
     * 
     * @param commandRegistry
     *            The {@link CommandRegistry} instance.
     */
    @Override
    public void setCommandType(CommandRegistry commandRegistry) {
        this.commandRegistry = commandRegistry;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GenericNoArgCommandData [commandRegistry=").append(this.commandRegistry);
        builder.append(", bytes=");

        if (this.bytes == null) {
            builder.append("null");
        } else {
            for (byte outByte : this.bytes) {
                builder.append(String.format("0x%x ", outByte));
            }
        }

        builder.append("]");
        return builder.toString();
    }
}
