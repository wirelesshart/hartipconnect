package com.seeq.wirelesshart.hartipconnect.hartpdu;

import java.util.Arrays;

import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.MessageBody;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.CommunicationErrorType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;

/**
 * This class implements the HART Protocol Data Unit (PDU). The PDU encapsulates the following bytes:
 * 
 * <ul>
 * <li><b>Delimeter Byte</b> 1 Byte, contains multiple types of information, such as the Frame Type, the Physical Layer
 * Type, Number of Expansion Bytes, and the Address Type</li>
 * <li><b>Address Byte</b> 1 or 5 Bytes, is either a unique 1 byte polling address or a 5 byte unique device address.
 * This also contains information about the manufacturer and type of device.</li>
 * <li><b>Expansion Bytes</b> 0-3 Bytes. The meaning of the expansion bytes is controlled by the Hart Communication
 * Foundation and is generally command specific.</li>
 * <li><b>Command Byte</b> 1 Byte, specifies the command being executed. It is set by the client, copied back by the
 * server.</li>
 * <li><b>Byte Count</b> 1 Byte, specifies the number of bytes to expect in the data packet</li>
 * <li><b>Data Bytes</b> 0-255 Bytes, specific to the command type request or response, communication errors, command
 * execution errors, or device status information. The size of this data packet is determined by the Byte Count byte
 * above.</li>
 * <li><b>Check Byte</b> A full XOR'd check byte of all other encapsulatetd data.</li>
 * </ul>
 */
public class HartPdu implements MessageBody {

    /**
     * The Frame Type. The Frame Type determines whether this PDU is being sent as a request, a response, or part of a
     * burst response.
     */
    private FrameType frameType = FrameType.STX;

    /**
     * The physical layer, which can be either synchronous or asynchronous.
     */
    private PhysicalLayerType physicalLayerType = PhysicalLayerType.IS_SYNCHRONOUS;

    /**
     * The address of the device. This can be either a 1 byte polling address or a 5 byte unique device address. Please
     * refer to {@link DeviceAddress} for more information.
     */
    private DeviceAddress deviceAddress;

    /**
     * Expansion bytes. These bytes are determined by the context and use of the specific PDU.
     */
    private byte[] expansionBytes;

    /**
     * The command being sent. Please refer to {@link CommandRegistry} for a listing of all the supported commands.
     */
    private CommandRegistry commandRegistry;

    /**
     * Data unique to the {@link HartPdu#commandRegistry}. This could be the arguments sent to the Server, or response data.
     */
    private CommandData commandData;

    /**
     * Response codes, generated by the execution of the command on the server. Please refer to {@link ResponseCodeType}
     * for a full listing of all supported response codes. This is mutually exclusive to {@link #communicationErrors},
     * meaning that only one can be defined. The other must be null.
     */
    private ResponseCodeType responseCodeType;

    /**
     * Any communication errors encountered. Please refer to {@link CommunicationErrorType} for more information. If
     * null, then there are no communication errors. This should be mutually exclusive to {@link #responseCodeType},
     * hence only one can be defined as non-null at any moment in time. The other must be null.
     */
    private CommunicationErrorType[] communicationErrors = null;

    /**
     * All status bits returned by the device. Please refer to {@link DeviceStatusType} for a listing of known device
     * status information.
     */
    private DeviceStatusType[] deviceStatus = null;

    /**
     * Get the frame type. This returns the {@link FrameType} instance for this HART PDU.
     * 
     * @return The instance of the {@link FrameType}.
     */
    public FrameType getFrameType() {
        return this.frameType;
    }

    /**
     * Set the {@link FrameType} instance.
     * 
     * @param frameType
     *            The {@link FrameType} instance.
     */
    public void setFrameType(FrameType frameType) {
        this.frameType = frameType;
    }

    /**
     * Get the expansion bytes array.
     * 
     * @return The expansion bytes array.
     */
    public byte[] getExpansionBytes() {
        return this.expansionBytes;
    }

    /**
     * Set the expansion bytes array.
     * 
     * @param expansionByte
     *            The expansion byte array.
     */

    public void setExpansionBytes(byte[] expansionByte) {
        this.expansionBytes = expansionByte;
    }

    /**
     * Get the command data packet.
     * 
     * @return The command data packet.
     */
    public CommandData getCommandData() {
        return this.commandData;
    }

    /**
     * Set the command data packet.
     * 
     * @param commandData
     *            The command data packet.
     */
    public void setCommandData(CommandData commandData) {
        this.commandData = commandData;
        if (this.commandData != null) {
            this.commandRegistry = this.commandData.getCommandType();
        }
    }

    /**
     * Get the physical layer type.
     * 
     * @return The physical layer type.
     */
    public PhysicalLayerType getPhysicalLayerType() {
        return this.physicalLayerType;
    }

    /**
     * Set the physical layer type.
     * 
     * @param physicalLayerType
     */
    public void setPhysicalLayerType(PhysicalLayerType physicalLayerType) {
        this.physicalLayerType = physicalLayerType;
    }

    /**
     * Get the list of communication errors.
     * 
     * @return The list of communication errors.
     */
    public CommunicationErrorType[] getCommunicationErrors() {
        return this.communicationErrors;
    }

    /**
     * Set the communication errors. This list should contain the {@link CommunicationErrorType#COMMUNICATION_ERROR} as
     * a minimum.
     * <p>
     * Note that the communication errors are mutually exclusive to {@link #setResponseCodeType(ResponseCodeType)},
     * hence when communication errors are defined, the response code type must be null, and vice versa.
     * 
     * @param communicationErrors
     *            The list of communication errors.
     */
    public void setCommunicationErrors(CommunicationErrorType[] communicationErrors) {
        this.communicationErrors = communicationErrors;
    }

    /**
     * Get the list of device status codes.
     * 
     * @return A list of device status codes.
     */
    public DeviceStatusType[] getDeviceStatus() {
        return this.deviceStatus;
    }

    /**
     * Sets the device status codes.
     * 
     * @param deviceStatus
     *            The device status codes.
     */
    public void setDeviceStatus(DeviceStatusType[] deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    /**
     * Get the device address.
     * 
     * @return The device address.
     */
    public DeviceAddress getDeviceAddress() {
        return this.deviceAddress;
    }

    /**
     * Set the device address.
     * 
     * @param deviceAddress
     *            The device address.
     */
    public void setDeviceAddress(DeviceAddress deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    /**
     * Get the response code for the command execution.
     * 
     * @return The response code.
     */
    public ResponseCodeType getResponseCodeType() {
        return this.responseCodeType;
    }

    /**
     * Set the response code.
     * <p>
     * Note that the response code type must be mutually exclusive to
     * {@link #setCommunicationErrors(CommunicationErrorType[])}. Hence, if the response code type is defined, then the
     * communication errors must be null, and vice versa.
     * 
     * @param responseCodeType
     *            The response code.
     */
    public void setResponseCodeType(ResponseCodeType responseCodeType) {
        this.responseCodeType = responseCodeType;
    }

    /**
     * Get the command type.
     * 
     * @return The command type.
     */
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    /**
     * Set the command type.
     * 
     * @param commandRegistry
     *            The command type.
     */
    public void setCommandType(CommandRegistry commandRegistry) {
        this.commandRegistry = commandRegistry;
    }

    /**
     * Validate that the assembled HartPdu is conformant with the HART standard.
     * 
     * @throws IllegalStateException
     *             If any of the necessary requirements for the HART-PDU are violated.
     */
    public void validate() throws IllegalStateException {
        if (this.getPhysicalLayerType() == null) {
            throw new IllegalStateException("PhysicalLayerType not defined");
        } else if (this.getDeviceAddress() == null) {
            throw new IllegalStateException("Device Address is not defined");
        } else if (this.getCommandType() == null) {
            throw new IllegalStateException("Command Type not defined");
        } else if (this.getFrameType() == null) {
            throw new IllegalStateException("Frame type is not defined");
        }
        if (this.getFrameType() == FrameType.STX) {
            if (this.getCommunicationErrors() != null || this.getResponseCodeType() != null
                    || this.getDeviceStatus() != null) {
                throw new IllegalStateException("Extra data defined in Request packet");
            }
        } else {
            if (this.getCommunicationErrors() != null) {
                if (this.getDeviceStatus() == null) {
                    throw new IllegalStateException(
                            "The device status is missing though communication errors are present.");
                }
            } else if (this.getResponseCodeType() == null || this.getDeviceStatus() == null) {
                throw new IllegalStateException(
                        "No communication errors encountered, "
                                + "yet either the response code or the device status are missing");
            }
        }
        if (this.getCommandType() != CommandRegistry.COMMAND_000) {
            if (this.getDeviceAddress().getClass() == ShortFrameAddress.class) {
                throw new IllegalStateException("Short Frame Addresses are only supported by Command 0");
            }
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.commandData == null) ? 0 : this.commandData.hashCode());
        result = prime * result + ((this.commandRegistry == null) ? 0 : this.commandRegistry.hashCode());
        result = prime * result + Arrays.hashCode(this.communicationErrors);
        result = prime * result + ((this.deviceAddress == null) ? 0 : this.deviceAddress.hashCode());
        result = prime * result + Arrays.hashCode(this.deviceStatus);
        result = prime * result + Arrays.hashCode(this.expansionBytes);
        result = prime * result + ((this.frameType == null) ? 0 : this.frameType.hashCode());
        result = prime * result + ((this.physicalLayerType == null) ? 0 : this.physicalLayerType.hashCode());
        result = prime * result + ((this.responseCodeType == null) ? 0 : this.responseCodeType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        }

        HartPdu other = (HartPdu) obj;

        if (this.commandRegistry == null) {
            if (other.commandRegistry != null) {
                return false;
            }
        } else if (this.commandRegistry != other.commandRegistry) {
            return false;
        }

        if (this.commandData == null) {
            if (other.commandData != null) {
                return false;
            }
        } else if (other.commandData == null) {
            return false;
        } else if (!this.commandData.equals(other.commandData)) {
            return false;
        }

        if (!Arrays.equals(this.communicationErrors, other.communicationErrors)) {
            return false;
        }

        if (this.deviceAddress == null) {
            if (other.deviceAddress != null) {
                return false;
            }
        } else if (!this.deviceAddress.equals(other.deviceAddress)) {
            if (this.deviceAddress.getAddressType() == AddressType.IS_POLLING_0_BYTE) {
                return ((ShortFrameAddress) this.deviceAddress).equals((other.deviceAddress));
            } else {
                return ((LongFrameAddress) this.deviceAddress).equals((other.deviceAddress));
            }
        }

        if (!Arrays.equals(this.deviceStatus, other.deviceStatus)) {
            return false;
        } else if (!Arrays.equals(this.expansionBytes, other.expansionBytes)) {
            return false;
        } else if (this.frameType != other.frameType) {
            return false;
        } else if (this.physicalLayerType != other.physicalLayerType) {
            return false;
        }

        if (this.responseCodeType == null) {
            if (other.responseCodeType != null) {
                return false;
            }
        } else if (this.responseCodeType != other.responseCodeType) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "HartPdu [frameType=" + this.frameType
                + ", physicalLayerType=" + this.physicalLayerType
                + ", deviceAddress=" + this.deviceAddress
                + ", expansionBytes=" + Arrays.toString(this.expansionBytes)
                + ", commandRegistry=" + this.commandRegistry
                + ", commandData=" + this.commandData
                + ", communicationErrors=" + Arrays.toString(this.communicationErrors)
                + ", responseCodeType=" + this.responseCodeType
                + ", deviceStatus=" + Arrays.toString(this.deviceStatus) + "]";
    }

}
