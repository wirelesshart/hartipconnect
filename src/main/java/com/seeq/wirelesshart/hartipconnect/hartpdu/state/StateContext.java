package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;

/**
 * This class is used to maintain the state of the processing engine.
 */
class StateContext {
    /**
     * The presently active state.
     */

    private State activeState;
    /**
     * The number of expansion bytes that were discovered in the framing bytes.
     */

    private int numberOfExpansionBytes;
    /**
     * The number of bytes that are expected to be in the {@link CommandData} packet.
     */

    private int dataByteCount;

    /**
     * Constructor.
     */
    public StateContext() {
        this.activeState = new DelimeterState();
    }

    /**
     * Get the active state.
     *
     * @return The presently running state.
     */
    public State getActiveState() {
        return this.activeState;
    }

    /**
     * Set the active state.
     *
     * @param activeState
     *            The active state.
     */
    public void setActiveState(State activeState) {
        this.activeState = activeState;
    }

    /**
     * Return the total byte count of the Data packet. This includes data, status bits, and (eventually) the extended
     * command registers.
     *
     * @return The total byte count.
     */
    public int getDataByteCount() {
        return this.dataByteCount;
    }

    /**
     * Set the total byte count of the Data packet. This should include both data and status bits as well as extended
     * command registers.
     *
     * @param byteCount
     *            The total byte count.
     */
    public void setDataByteCount(int byteCount) {
        this.dataByteCount = byteCount;
    }

    /**
     * The number of bytes in the expansion byte. This is a place holder that <b>may</b> be used by the states during
     * the encoding/decoding process.
     * 
     * @return The number of bytes in the expansion byte.
     */
    public int getNumberOfExpansionBytes() {
        return this.numberOfExpansionBytes;
    }

    /**
     * Set the number of bytes in the expansion byte. This is a place holder that <b>may</b> be used by the states
     * during the encoding/decoding process.
     * 
     * @param numberOfExpansionBytes
     *            The number of bytes in the expansion byte.
     */
    public void setNumberOfExpansionBytes(int numberOfExpansionBytes) {
        this.numberOfExpansionBytes = numberOfExpansionBytes;
    }
}
