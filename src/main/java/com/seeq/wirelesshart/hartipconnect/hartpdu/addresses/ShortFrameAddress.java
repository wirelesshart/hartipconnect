package com.seeq.wirelesshart.hartipconnect.hartpdu.addresses;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

/**
 * This class is used with polling type addresses. A short polling address uses 1 byte in the HART protocol, and
 * contains information about who is making the request (Primary or Secondary Master), whether the device is in Polling
 * mode, and the polling address itself.
 */
public class ShortFrameAddress implements DeviceAddress {

    private final AddressType addressType = AddressType.IS_POLLING_0_BYTE;
    private boolean isPrimaryMaster = true;
    private boolean isDeviceInBurstMode = false;
    private byte pollingAddress;

    /**
     * Get the polling address.
     * 
     * @return The polling address.
     */
    public byte getPollingAddress() {
        return this.pollingAddress;
    }

    /**
     * Set the polling address.
     * 
     * @param pollingAddress
     *            The polling address.
     */
    public void setPollingAddress(byte pollingAddress) {
        this.pollingAddress = pollingAddress;
    }

    @Override
    public AddressType getAddressType() {
        return this.addressType;
    }

    @Override
    public boolean isPrimaryMaster() {
        return this.isPrimaryMaster;
    }

    @Override
    public void setPrimaryMaster(boolean isPrimaryMaster) {
        this.isPrimaryMaster = isPrimaryMaster;
    }

    @Override
    public boolean isDeviceInBurstMode() {
        return this.isDeviceInBurstMode;
    }

    @Override
    public void setDeviceInBurstMode(boolean isDeviceInBurstMode) {
        this.isDeviceInBurstMode = isDeviceInBurstMode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.addressType.hashCode();
        result = prime * result + (this.isDeviceInBurstMode ? 1231 : 1237);
        result = prime * result + (this.isPrimaryMaster ? 1231 : 1237);
        result = prime * result + this.pollingAddress;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        }

        ShortFrameAddress other = (ShortFrameAddress) obj;

        if (this.isDeviceInBurstMode != other.isDeviceInBurstMode) {
            return false;
        } else if (this.isPrimaryMaster != other.isPrimaryMaster) {
            return false;
        } else if (this.pollingAddress != other.pollingAddress) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ShortFrameAddress ["
                + "addressType=" + this.addressType
                + ", isPrimaryMaster=" + this.isPrimaryMaster
                + ", isDeviceInBurstMode=" + this.isDeviceInBurstMode
                + ", pollingAddress=" + this.pollingAddress
                + "]";
    }

}
