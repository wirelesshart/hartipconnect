package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;

/**
 * This class is used to maintain the state of the processing engine.
 */
public class StateContextForDecoder extends StateContext {
    /**
     * Preallocation of memory for the List<Byte> members, for performance reasons.
     */
    private static final int INITIAL_MEMORY_ALLOCATION = 1024;

    /**
     * The total set of raw bytes, as they are received.
     */
    private final List<Byte> rawIncomingBytes;

    /**
     * A workspace area. This is expected to grow and shrink as needed by the states.
     */
    private final List<Byte> workspaceBytes;

    /**
     * The message packet that is being built.
     */
    private HartPdu outputHartPdu;

    /**
     * Constructor.
     */
    public StateContextForDecoder() {
        this.rawIncomingBytes = new ArrayList<>(INITIAL_MEMORY_ALLOCATION);
        this.workspaceBytes = new ArrayList<>(INITIAL_MEMORY_ALLOCATION);
    }

    /**
     * Return the hart pdu packet.
     *
     * @return The hart-pdu packet.
     */
    public HartPdu getOutputHartPdu() {
        return this.outputHartPdu;
    }

    /**
     * Set the hart pdu packet. This is used by the States during the encoding or decoding process, as needed. It is up
     * to the States to manage the state of this instance.
     *
     * @param hartPdu
     *            The hart pdu.
     */
    public void setOutputHartPdu(HartPdu hartPdu) {
        this.outputHartPdu = hartPdu;
    }

    /**
     * Get the workspace bytes. The workspace is managed by the states and is used to hold incoming bytes until an
     * appropriate number are received. At that time, the states will process those bytes and clear the list. It is up
     * to each of the states to determine how or if they want to use this particular workspace. The only requirement is
     * that the states clear the list after they are finished using it.
     *
     * @return A list of the processed bytes.
     */
    public List<Byte> getWorkspaceBytes() {
        return this.workspaceBytes;
    }

    /**
     * Obtain the total set of bytes received/encoded in the packet. At the end of encoding, this should contain the
     * byte representation of the input {@link HartPdu} instance. Conversely, at the end of the decoding process, this
     * should contain the input byte representation of the output {@link HartPdu} instance.
     * 
     * @return The total set of bytes in the packet.
     */
    public List<Byte> getRawIncomingBytes() {
        return this.rawIncomingBytes;
    }
}
