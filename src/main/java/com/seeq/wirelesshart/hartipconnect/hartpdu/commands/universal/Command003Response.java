package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.DeviceVariable;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * The command packet for Command #3.
 * 
 * This command is specifically used to read in dynamic variables and loop current from a remote device. It reads the
 * loop current and up to four predefined dynamic variables. The loop current always matches the current that can be
 * measured by a milli-ammeter in series with the field device. This includes alarm conditions and set values.
 * 
 * For a given device type, the number of response data bytes must be fixed. In other words, a device type may not
 * return primaryVariable, secondaryVariable, and tertiaryVariable in one operating mode and later (in a different
 * operating mode) return only primaryVariable and secondaryVariable.
 * 
 * <table>
 * <tr>
 * <th>Dynamic Variables</th>
 * <th>Number of Response Bytes</th>
 * </tr>
 * <tr>
 * <td>primaryVariable</td>
 * <td>9</td>
 * </tr>
 * <tr>
 * <td>primaryVariable,secondaryVariable</td>
 * <td>14</td>
 * </tr>
 * <tr>
 * <td>primaryVariable,secondaryVariable,tertiaryVariable</td>
 * <td>19</td>
 * </tr>
 * <tr>
 * <td>primaryVariable,secondaryVariable,tertiaryVariable,quaternaryVariable</td>
 * <td>24</td>
 * </tr>
 * </table>
 */
public class Command003Response implements CommandData {

    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_003;
    private final FrameType frameType = FrameType.ACK;

    /**
     * The primary dynamic variable.
     */
    private DeviceVariable primaryVariable;
    /**
     * The secondary dynamic variable.
     */
    private DeviceVariable secondaryVariable;
    /**
     * The tertiary dynamic variable.
     */
    private DeviceVariable tertiaryVariable;
    /**
     * The quaternary dynamic variable.
     */
    private DeviceVariable quaternaryVariable;
    /**
     * The primary dynamic variable's loop current.
     */
    private Float pVLoopCurrent = 0.0f;

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters. Only
     * the primary variable is initialized to a default value.
     */
    public Command003Response() {
        this.setPrimaryVariable(new DeviceVariable());
    }

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();

        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities.floatToByteArray(this.pVLoopCurrent)));
        encodedBytes.addAll(this.primaryVariable.toBytes());
        if (this.secondaryVariable != null) {
            encodedBytes.addAll(this.secondaryVariable.toBytes());
        }
        if (this.tertiaryVariable != null) {
            encodedBytes.addAll(this.tertiaryVariable.toBytes());
        }
        if (this.quaternaryVariable != null) {
            encodedBytes.addAll(this.quaternaryVariable.toBytes());
        }

        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) throws EngineeringUnitsException {
        if (bytesToDecode.size() != 9
                && bytesToDecode.size() != 14
                && bytesToDecode.size() != 19
                && bytesToDecode.size() != 24) {
            throw new IndexOutOfBoundsException("Expected a length of 9, 14, 19, or 24, but received "
                    + bytesToDecode.size());
        }

        // Process the least number of expected bytes (the first 9).
        this.pVLoopCurrent = HartUtilities
                .floatFromBytes(HartUtilities.byteListToByteArray(bytesToDecode.subList(0, 4)));
        this.primaryVariable = DeviceVariable.fromBytes(bytesToDecode.subList(4, 9));

        // Process the next optional set of bytes (next 5 beyond 9).
        if (bytesToDecode.size() > 9) {
            this.secondaryVariable = DeviceVariable.fromBytes(bytesToDecode.subList(9, 14));
        }

        // Process the next optional set of bytes (next 5 beyond 14).
        if (bytesToDecode.size() > 14) {
            this.tertiaryVariable = DeviceVariable.fromBytes(bytesToDecode.subList(14, 19));
        }

        // Process the next optional set of bytes (next 5 beyond 19).
        if (bytesToDecode.size() > 19) {
            this.quaternaryVariable = DeviceVariable.fromBytes(bytesToDecode.subList(19, 24));
        }
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (!Command003Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Command003Response other = (Command003Response) obj;
        if (this.primaryVariable == null) {
            if (other.primaryVariable != null) {
                return false;
            }
        } else if (!this.primaryVariable.equals(other.primaryVariable)) {
            return false;
        }
        if (this.quaternaryVariable == null) {
            if (other.quaternaryVariable != null) {
                return false;
            }
        } else if (!this.quaternaryVariable.equals(other.quaternaryVariable)) {
            return false;
        }
        if (this.secondaryVariable == null) {
            if (other.secondaryVariable != null) {
                return false;
            }
        } else if (!this.secondaryVariable.equals(other.secondaryVariable)) {
            return false;
        }
        if (this.tertiaryVariable == null) {
            if (other.tertiaryVariable != null) {
                return false;
            }
        } else if (!this.tertiaryVariable.equals(other.tertiaryVariable)) {
            return false;
        }
        if (this.pVLoopCurrent == null) {
            if (other.pVLoopCurrent != null) {
                return false;
            }
        } else if (!this.pVLoopCurrent.equals(other.pVLoopCurrent)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.commandRegistry.hashCode();
        result = prime * result + this.frameType.hashCode();
        result = prime * result + ((this.pVLoopCurrent == null) ? 0 : this.pVLoopCurrent.hashCode());
        result = prime * result + ((this.primaryVariable == null) ? 0 : this.primaryVariable.hashCode());
        result = prime * result + ((this.quaternaryVariable == null) ? 0 : this.quaternaryVariable.hashCode());
        result = prime * result + ((this.secondaryVariable == null) ? 0 : this.secondaryVariable.hashCode());
        result = prime * result + ((this.tertiaryVariable == null) ? 0 : this.tertiaryVariable.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Command003Response ["
                + "commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", PV=" + this.primaryVariable
                + ", SV=" + this.secondaryVariable
                + ", TV=" + this.tertiaryVariable
                + ", QV=" + this.quaternaryVariable
                + ", pvLoopCurrent=" + this.pVLoopCurrent
                + "]";
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return this.toBytes().size();
    }

    /**
     * Get the Primary Variable.
     * 
     * @return The primary variable. This should never be null, as defined by the HART Communication Foundation and
     *         documented {@link Command003Response here}.
     */
    public DeviceVariable getPrimaryVariable() {
        return this.primaryVariable;
    }

    /**
     * Set the Primary Variable.
     * 
     * @param primaryVariable
     *            The primary variable. This should not be null, as defined by the HART Communication Foundation and
     *            documented {@link Command003Response here}.
     */
    public void setPrimaryVariable(DeviceVariable primaryVariable) {
        this.primaryVariable = primaryVariable;
    }

    /**
     * Get the Secondary Variable.
     * 
     * @return The Secondary Variable, or null, if none is supplied, as defined by the HART Communication Foundation and
     *         documented {@link Command003Response here}.
     */
    public DeviceVariable getSecondaryVariable() {
        return this.secondaryVariable;
    }

    /**
     * Set the Secondary Variable.
     * 
     * @param secondaryVariable
     *            The Secondary Variable, or null, if none is supplied, as defined by the HART Communication Foundation
     *            and documented {@link Command003Response here}.
     */
    public void setSecondaryVariable(DeviceVariable secondaryVariable) {
        this.secondaryVariable = secondaryVariable;
    }

    /**
     * Get the Tertiary Variable.
     * 
     * @return The Tertiary Variable or null, if none is supplied, as defined by the HART Communication Foundation and
     *         documented {@link Command003Response here}.
     */
    public DeviceVariable getTertiaryVariable() {
        return this.tertiaryVariable;
    }

    /**
     * Set the Tertiary Variable.
     * 
     * @param tertiaryVariable
     *            The Tertiary Variable, or null, if none is supplied, as defined by the HART Communication Foundation
     *            and documented {@link Command003Response here}.
     */
    public void setTertiaryVariable(DeviceVariable tertiaryVariable) {
        this.tertiaryVariable = tertiaryVariable;
    }

    /**
     * Get the Quarternary Variable.
     * 
     * @return The Quarternary Variable, or null, if none is supplied, as defined by the HART Communication Foundation
     *         and documented {@link Command003Response here}.
     */
    public DeviceVariable getQuaternaryVariable() {
        return this.quaternaryVariable;
    }

    /**
     * Set the Quarternary Variable.
     * 
     * @param quaternaryVariable
     *            The Quarternary Variable, or null, if none is supplied, as defined by the HART Communication
     *            Foundation and documented {@link Command003Response here}.
     */
    public void setQuaternaryVariable(DeviceVariable quaternaryVariable) {
        this.quaternaryVariable = quaternaryVariable;
    }

    /**
     * Get the Loop Current.
     * 
     * @return The Loop Current, in mA.
     */
    public Float getpVLoopCurrent() {
        return this.pVLoopCurrent;
    }

    /**
     * Set the Loop Current.
     * 
     * @param pVLoopCurrent
     *            The Loop Current, in mA.
     */
    public void setpVLoopCurrent(Float pVLoopCurrent) {
        this.pVLoopCurrent = pVLoopCurrent;
    }
}
