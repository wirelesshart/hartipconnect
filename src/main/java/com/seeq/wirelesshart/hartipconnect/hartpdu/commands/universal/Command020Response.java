package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * The Response packet for Command #020 - Read Long Tag.
 *
 */
public class Command020Response implements CommandData {
    private static final Logger log = Logger.getLogger(Command020Response.class.getName());
    protected static final int TOTAL_BYTE_COUNT = 32;
    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_020;
    private final FrameType frameType = FrameType.ACK;

    private String longTag = "uninitialized";

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public Command020Response() {}

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();

        String paddedTag = String.format("%32s", this.longTag);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(paddedTag.getBytes()));

        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) throws EngineeringUnitsException {
        if (bytesToDecode.size() != TOTAL_BYTE_COUNT) {
            throw new IllegalArgumentException("Expected a length of " + TOTAL_BYTE_COUNT + ", but received "
                    + bytesToDecode.size());
        }

        byte[] byteArray = HartUtilities.byteListToByteArray(bytesToDecode);
        try {
            this.longTag = new String(byteArray, "ISO-8859-1").trim();
        } catch (UnsupportedEncodingException e) {
            log.log(Level.WARNING, "Unsupported encoding exception", e);
            this.longTag = "uninitialized";
        }
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return TOTAL_BYTE_COUNT;
    }

    /**
     * Get the Long Tag.
     * 
     * @return The long tag.
     */
    public String getLongTag() {
        return this.longTag;
    }

    /**
     * Set the long tag.
     * 
     * @param longTag
     *            The long tag.
     */
    public void setLongTag(String longTag) {
        this.longTag = longTag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.longTag.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command020Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command020Response other = (Command020Response) obj;
        return this.equals(other);
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command020Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Command020Response other = (Command020Response) obj;
        if (this.longTag == null) {
            if (other.longTag == null) {
                return true;
            } else if (other.longTag != null) {
                return false;
            }
        }
        return this.longTag.equals(other.longTag);
    }

    @Override
    public String toString() {
        return "Command020Response ["
                + "commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", long tag=" + this.longTag
                + "]";
    }
}
