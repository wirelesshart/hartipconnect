package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import java.util.ArrayList;
import java.util.List;

/**
 * This enumerates the known device status codes as reported by the target device.
 */
public enum DeviceStatusType {

    /**
     * The device detected a serious error or failure that compromises device operation.
     */
    DEVICE_MALFUNCTION((byte) 0x80, "General Device Malfunction"),

    /**
     * An operation was performed that changed the device's configuration.
     */
    CONFIGURATION_CHANGED((byte) 0x40, "Configuration Changed"),

    /**
     * A power failure or Device Reset has occurred.
     */
    COLD_START((byte) 0x20, "Cold Start"),

    /**
     * More status information is available via Command 48. Read "Additional Status Information".
     */
    MORE_STATUS_AVAILABLE((byte) 0x10, "More Status Available"),

    /**
     * The Loop Current is being held at a fixed value and is not responding to process variations.
     */
    LOOP_CURRENT_FIXED((byte) 0x08, "Loop Current Fixed"),

    /**
     * The Loop Current has reached its upper (or lower) endpoint limit and cannot increase (or decrease) further.
     */
    LOOP_CURRENT_SATURATED((byte) 0x04, "Loop Current Saturated"),

    /**
     * A device variable not mapped to the primary Process Variable, PV, is beyond its operating limits.
     */
    NON_PRIMARY_VARIABLE_OOL((byte) 0x02, "Non-Primary Variable Out of Limits"),

    /**
     * The primary process variable, PV, is beyond its operating limits.
     */
    PRIMARY_VARIABLE_OOL((byte) 0x01, "Primary Variable Out Of Limits"),

    /**
     * The device is operating correctly.
     */
    ALL_OKAY((byte) 0x00, "All is well!");

    private final byte value;
    private final String humanString;

    private DeviceStatusType(byte value, String humanString) {
        this.value = value;
        this.humanString = humanString;
    }

    /**
     * Get the internal value as represented in the HART protocol.
     *
     * @return The bit value from the HART protocol.
     */
    public byte getValue() {
        return this.value;
    }

    /**
     * A human readable string that describes the condition of the device.
     *
     * @return The String that describes this condition.
     */
    public String getHumanString() {
        return this.humanString;
    }

    /**
     * Given the device status byte, obtain a list of status codes.
     *
     * @param deviceStatusByte
     *            The device status byte to query.
     * @return The list of status types extracted from the input byte.
     */
    public static DeviceStatusType[] fromDeviceStatusByte(byte deviceStatusByte) {
        List<DeviceStatusType> outList = new ArrayList<>(DeviceStatusType.values().length);

        if (deviceStatusByte == 0x00) {
            outList.add(DeviceStatusType.ALL_OKAY);
        } else {
            for (DeviceStatusType status : DeviceStatusType.values()) {
                if (status == DeviceStatusType.ALL_OKAY) {
                    continue;
                }
                if ((deviceStatusByte & status.getValue()) == status.getValue()) {
                    outList.add(status);
                }
            }
        }
        return outList.toArray(new DeviceStatusType[outList.size()]);
    }

    /**
     * Convert a list of status to a device status byte.
     *
     * @param statusList
     *            A list of status types.
     * @return A byte.
     */
    public static byte toDeviceStatusByte(DeviceStatusType[] statusList) {
        if (statusList == null) {
            return 0x00;
        }

        byte deviceStatusByte = 0x00;
        for (DeviceStatusType status : statusList) {
            deviceStatusByte |= status.getValue();
        }
        return deviceStatusByte;
    }
}
