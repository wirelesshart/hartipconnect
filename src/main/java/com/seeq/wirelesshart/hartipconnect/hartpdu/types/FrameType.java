package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.FrameException;

/**
 * Types of supported frame types.
 */
public enum FrameType {

    /**
     * Request frame type.
     */
    STX((byte) 0x02),

    /**
     * A response frame type.
     */
    ACK((byte) 0x06),

    /**
     * A response frame type, generated in burst mode.
     */
    BACK((byte) 0x01);

    private final byte decodeByte;

    private FrameType(byte decodeByte) {
        this.decodeByte = (byte) (decodeByte & 0xff);
    }

    /**
     * Returns the byte representation in the protocol.
     *
     * @return The byte representation.
     */
    public byte getDecodeByte() {
        return this.decodeByte;
    }

    /**
     * Extract the {@link FrameType} from the incoming byte.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return A corresponding {@link FrameType}.
     * @throws FrameException
     *             If the testByte does not correspond to a recognized {@link FrameType}.
     */
    public static FrameType fromDelimeterByte(byte delimeterByte) throws FrameException {
        for (FrameType type : FrameType.values()) {
            if ((delimeterByte & 0x07) == type.getDecodeByte()) {
                return type;
            }
        }
        throw new FrameException("Unrecognized frame value " + delimeterByte);
    }

    /**
     * Append the needed bits into the delimeter byte.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return A new delimeter byte with the appropriately modified bits.
     */
    public byte appendToDelimeterByte(byte delimeterByte) {
        byte clearedByte = (byte) (delimeterByte & ~(0x7));
        return (byte) (clearedByte | this.decodeByte);
    }

}
