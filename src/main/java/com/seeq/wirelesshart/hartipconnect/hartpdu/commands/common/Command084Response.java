package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * The Response packet for Command #84 -- Read Sub-Device Identity Summary
 * 
 * This command response provides the following parameters:
 * 
 * <table border="1" style="width:100%">
 * <tr>
 * <th>Type</th>
 * <th>Description</th>
 * </tr>
 * 
 * <tr>
 * <td>Unsigned-16</td>
 * <td>Sub-device index</td>
 * </tr>
 * 
 * <tr>
 * <td>Unsigned-8</td>
 * <td>I/O Card</td>
 * </tr>
 * <tr>
 * <td>Unsigned-8</td>
 * <td>Channel</td>
 * </tr>
 * <tr>
 * <td>Unsigned-16</td>
 * <td>Manufacturer Id</td>
 * </tr>
 * <tr>
 * <td>Unsigned-16</td>
 * <td>Expanded Device Type Code</td>
 * </tr>
 * <tr>
 * <td>Unsigned-24</td>
 * <td>Device Id</td>
 * </tr>
 * <tr>
 * <td>Unsigned-8</td>
 * <td>Universal Command Revision</td>
 * </tr>
 * <tr>
 * <td>Array of 32, Unsigned-8</td>
 * <td>Long Tag (ISO Latin-1)
 * <tr>
 * <td>Unsigned-8</td>
 * <td>Device Revision</td>
 * </tr>
 * <tr>
 * <td>Unsigned-8</td>
 * <td>Device Profile</td>
 * </tr>
 * <tr>
 * <td>Unsigned-16</td>
 * <td>Private Label Distributor</td>
 * </tr>
 * </table>
 */
public class Command084Response implements CommandData {
    Logger log = Logger.getLogger(Command084Response.class.getName());
    protected static final int TOTAL_BYTE_COUNT = 44;
    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_084;
    private final FrameType frameType = FrameType.ACK;

    private short subDeviceIndex;
    private byte ioCard;
    private byte channel;
    private short manufacturerId;
    private short expandedDeviceTypeCode;
    private byte[] deviceId = new byte[] { 0, 0, 0 };
    private byte universalCommandRevision;
    private String longTag = "uninitialized";

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public Command084Response() {}

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();

        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.subDeviceIndex)));
        encodedBytes.add(this.ioCard);
        encodedBytes.add(this.channel);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.manufacturerId)));
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.expandedDeviceTypeCode)));
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(this.deviceId));
        encodedBytes.add(this.universalCommandRevision);

        String paddedString = String.format("%32s", this.longTag);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(paddedString.getBytes()));

        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) throws EngineeringUnitsException {
        if (bytesToDecode.size() != TOTAL_BYTE_COUNT) {
            throw new IllegalArgumentException("Expected a length of " + TOTAL_BYTE_COUNT + ", but received "
                    + bytesToDecode.size());
        }

        this.subDeviceIndex = HartUtilities.shortFromBytes(HartUtilities
                .byteListToByteArray(bytesToDecode.subList(0, 2)));
        this.ioCard = bytesToDecode.get(2);
        this.channel = bytesToDecode.get(3);
        this.manufacturerId = HartUtilities.shortFromBytes(HartUtilities
                .byteListToByteArray(bytesToDecode.subList(4, 6)));
        this.expandedDeviceTypeCode = HartUtilities.shortFromBytes(HartUtilities
                .byteListToByteArray(bytesToDecode.subList(6, 8)));
        this.deviceId = HartUtilities.byteListToByteArray(bytesToDecode.subList(8, 11));
        this.universalCommandRevision = bytesToDecode.get(11);

        byte[] byteArray = HartUtilities.byteListToByteArray(bytesToDecode.subList(12, 44));
        try {
            this.longTag = new String(byteArray, "ISO-8859-1").trim();
        } catch (UnsupportedEncodingException e) {
            this.log.log(Level.WARNING, "Unsupported encoding exception", e);
            this.longTag = "uninitialized";
        }
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return TOTAL_BYTE_COUNT;
    }

    /**
     * @return the subDeviceIndex
     */
    public short getSubDeviceIndex() {
        return this.subDeviceIndex;
    }

    /**
     * @param subDeviceIndex
     *            the subDeviceIndex to set
     */
    public void setSubDeviceIndex(short subDeviceIndex) {
        this.subDeviceIndex = subDeviceIndex;
    }

    /**
     * @return the ioCard
     */
    public byte getIoCard() {
        return this.ioCard;
    }

    /**
     * @param ioCard
     *            the ioCard to set
     */
    public void setIoCard(byte ioCard) {
        this.ioCard = ioCard;
    }

    /**
     * @return the channel
     */
    public byte getChannel() {
        return this.channel;
    }

    /**
     * @param channel
     *            the channel to set
     */
    public void setChannel(byte channel) {
        this.channel = channel;
    }

    /**
     * @return the manufacturerId
     */
    public short getManufacturerId() {
        return this.manufacturerId;
    }

    /**
     * @param manufacturerId
     *            the manufacturerId to set
     */
    public void setManufacturerId(short manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    /**
     * @return the expandedDeviceTypeCode
     */
    public short getExpandedDeviceTypeCode() {
        return this.expandedDeviceTypeCode;
    }

    /**
     * @param expandedDeviceTypeCode
     *            the expandedDeviceTypeCode to set
     */
    public void setExpandedDeviceTypeCode(short expandedDeviceTypeCode) {
        this.expandedDeviceTypeCode = expandedDeviceTypeCode;
    }

    /**
     * @return the deviceId
     */
    public byte[] getDeviceId() {
        return this.deviceId;
    }

    /**
     * @param deviceId
     *            the deviceId to set
     */
    public void setDeviceId(byte[] deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the universalCommandRevision
     */
    public byte getUniversalCommandRevision() {
        return this.universalCommandRevision;
    }

    /**
     * @param universalCommandRevision
     *            the universalCommandRevision to set
     */
    public void setUniversalCommandRevision(byte universalCommandRevision) {
        this.universalCommandRevision = universalCommandRevision;
    }

    /**
     * @return the longTag
     */
    public String getLongTag() {
        return this.longTag;
    }

    /**
     * @param longTag
     *            the longTag to set
     */
    public void setLongTag(String longTag) {
        this.longTag = longTag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.channel;
        result = prime * result + ((this.commandRegistry == null) ? 0 : this.commandRegistry.hashCode());
        result = prime * result + Arrays.hashCode(this.deviceId);
        result = prime * result + this.expandedDeviceTypeCode;
        result = prime * result + ((this.frameType == null) ? 0 : this.frameType.hashCode());
        result = prime * result + this.ioCard;
        result = prime * result + ((this.longTag == null) ? 0 : this.longTag.hashCode());
        result = prime * result + this.manufacturerId;
        result = prime * result + this.subDeviceIndex;
        result = prime * result + this.universalCommandRevision;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command084Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command084Response other = (Command084Response) obj;
        return this.equals(other);
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command084Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command084Response other = (Command084Response) obj;
        if (this.channel != other.channel) {
            return false;
        }
        if (this.commandRegistry != other.commandRegistry) {
            return false;
        }
        if (!Arrays.equals(this.deviceId, other.deviceId)) {
            return false;
        }
        if (this.expandedDeviceTypeCode != other.expandedDeviceTypeCode) {
            return false;
        }
        if (this.frameType != other.frameType) {
            return false;
        }
        if (this.ioCard != other.ioCard) {
            return false;
        }
        if (this.longTag == null) {
            if (other.longTag != null) {
                return false;
            }
        } else if (!this.longTag.equals(other.longTag)) {
            return false;
        }
        if (this.manufacturerId != other.manufacturerId) {
            return false;
        }
        if (this.subDeviceIndex != other.subDeviceIndex) {
            return false;
        }
        if (this.universalCommandRevision != other.universalCommandRevision) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Command084Response [commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", subDeviceIndex=" + this.subDeviceIndex
                + ", ioCard=" + this.ioCard
                + ", channel=" + this.channel
                + ", manufacturerId=" + this.manufacturerId
                + ", expandedDeviceTypeCode=" + this.expandedDeviceTypeCode
                + ", deviceId=" + Arrays.toString(this.deviceId)
                + ", universalCommandRevision=" + this.universalCommandRevision
                + ", longTag=" + this.longTag
                + "]";
    }
}
