package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;

/**
 * This enumeration keeps track of all the known engineering units. Specifically, it keeps track of the ID, a long human
 * readable units string, and a short protocolVersion of the same units string.
 */
public enum EngineeringUnitsType {
    UNITS_001("inches of water at 20 C (68 F)", "inH2O (20 C or 68 F"),
    UNITS_002("inches of mercury at 0 C (32 F)", "inHg (0 C or 32 F)"),
    UNITS_003("feet of water at 20 C (68 F)", "ftH2O (20 C or 68 F)"),
    UNITS_004("millimeters of water at 20 C (68 F)", "mmH2O (20 C or 68 F)"),
    UNITS_005("millimeters of mercury at 0 C (32 FC", "mmHg (0 C or 32 F)"),
    UNITS_006("pounds per square inch", "lbf/in^2"),
    UNITS_007("bars", "bar"),
    UNITS_008("millibars", "mbar"),
    UNITS_009("grams per square centimeter", "g/cm^2"),
    UNITS_010("kilograms per square centimeter", "kg/cm^2"),
    UNITS_011("pascals", "Pa"),
    UNITS_012("kilopascals", "kPa"),
    UNITS_013("torr", "torr"),
    UNITS_014("atmospheres", "atm"),
    UNITS_015("cubic feet per minute", "ft^3/min"),
    UNITS_016("gallons per minute", "usg/min"),
    UNITS_017("liters per minute", "L/min"),
    UNITS_018("imperial gallons per minute", "impgal/min"),
    UNITS_019("cubic meter per hour", "m^3/h"),
    UNITS_020("feet per second", "ft/s"),
    UNITS_021("meters per second", "m/s"),
    UNITS_022("gallons per second", "usg/s"),
    UNITS_023("million gallons per day", "million usg/d"),
    UNITS_024("liters per second", "L/s"),
    UNITS_025("million liters per day", "ML/day"),
    UNITS_026("cubic feet per second", "ft^3/s"),
    UNITS_027("cubic feet per day", "ft^3/d"),
    UNITS_028("cubic meters per second", "m^3/s"),
    UNITS_029("cubic meters per day", "m^3/d"),
    UNITS_030("imperial gallons per hour", "impgal/h"),
    UNITS_031("imperial gallons per day", "impgal/d"),
    UNITS_032("Degrees Celsius", "C"),
    UNITS_033("Degrees Fahrenheit", "F"),
    UNITS_034("Degrees Rankine", "degR"),
    UNITS_035("Kelvin", "degK"),
    UNITS_036("millivolts", "mV"),
    UNITS_037("ohms", "ohm"),
    UNITS_038("hertz", "hz"),
    UNITS_039("milliamperes", "mA"),
    UNITS_040("gallons", "usg"),
    UNITS_041("liters", "L"),
    UNITS_042("imperial gallons", "impgal"),
    UNITS_043("cubic meters", "m^3"),
    UNITS_044("feet", "ft"),
    UNITS_045("meters", "m"),
    UNITS_046("barrels", "bbl"),
    UNITS_047("inches", "in"),
    UNITS_048("centimeters", "cm"),
    UNITS_049("millimeters", "mm"),
    UNITS_050("minutes", "min"),
    UNITS_051("seconds", "s"),
    UNITS_052("hours", "h"),
    UNITS_053("days", "d"),
    UNITS_054("centistokes", "centistokes"),
    UNITS_055("centipoise", "cP"),
    UNITS_056("microsiemens", "microsiemens"),
    UNITS_057("percent", "%"),
    UNITS_058("volts", "V"),
    UNITS_059("pH", "pH"),
    UNITS_060("grams", "g"),
    UNITS_061("kilograms", "kg"),
    UNITS_062("metric tons", "t"),
    UNITS_063("pounds", "lb"),
    UNITS_064("short tons", "short ton"),
    UNITS_065("long tons", "long ton"),
    UNITS_066("milli siemens per centimeter", "millisiemens/cm"),
    UNITS_067("micro siemens per centimeter", "microsiemens/cm"),
    UNITS_068("newton", "N"),
    UNITS_069("newton meter", "N m"),
    UNITS_070("grams per second", "g/s"),
    UNITS_071("grams per minute", "g/min"),
    UNITS_072("grams per hour", "g/h"),
    UNITS_073("kilograms per second", "kg/s"),
    UNITS_074("kilograms per minute", "kg/min"),
    UNITS_075("kilograms per hour", "kg/h"),
    UNITS_076("kilograms per day", "kg/d"),
    UNITS_077("metric tons per minute", "t/min"),
    UNITS_078("metric tons per hour", "t/h"),
    UNITS_079("metric tons per day", "t/d"),
    UNITS_080("pounds per second", "lb/s"),
    UNITS_081("pounds per minute", "lb/min"),
    UNITS_082("pounds per hour", "lb/h"),
    UNITS_083("pounds per day", "lb/d"),
    UNITS_084("short tons per minute", "short ton/min"),
    UNITS_085("short tons per hour", "short ton/h"),
    UNITS_086("short tons per day", "short ton/d"),
    UNITS_087("long tons per hour", "long ton/h"),
    UNITS_088("long tons per day", "long ton/d"),
    UNITS_089("deka therm", "Dth"),
    UNITS_090("specific gravity units", "specific gravity units"),
    UNITS_091("grams per cubic centimeter", "g/cm^3"),
    UNITS_092("kilograms per cubic meter", "kg/m^3"),
    UNITS_093("pounds per gallon", "lb/usg"),
    UNITS_094("pounds per cubic feet", "lb/ft^3"),
    UNITS_095("grams per milliliter", "g/mL"),
    UNITS_096("kilograms per liter", "kg/L"),
    UNITS_097("grams per liter", "g/L"),
    UNITS_098("pounds per cubic inch", "lb/cubic in"),
    UNITS_099("short tons per cubic yard", "short ton/yd^3"),
    UNITS_100("degrees twaddell", "degTw"),
    UNITS_101("degrees brix", "degBx"),
    UNITS_102("degrees baume heavy", "BH"),
    UNITS_103("degrees baume light", "BL"),
    UNITS_104("degrees API", "degAPI"),
    UNITS_105("percent solids per weight", "% solid/weight"),
    UNITS_106("percent solids per volume", "% solid/volume"),
    UNITS_107("degrees balling", "degrees balling"),
    UNITS_108("proof per volume", "proof/volume"),
    UNITS_109("proof per mass", "proof/mass"),
    UNITS_110("bushels", "bushel"),
    UNITS_111("cubic yards", "yd^3"),
    UNITS_112("cubic feet", "ft^3"),
    UNITS_113("cubic inches", "cubic in"),
    UNITS_114("inches per second", "in/s"),
    UNITS_115("inches per minute", "in/min"),
    UNITS_116("feet per minute", "ft/min"),
    UNITS_117("degrees per second", "deg/s"),
    UNITS_118("revolutions per second", "rev/s"),
    UNITS_119("revolutions per minute", "rpm"),
    UNITS_120("meters per hour", "m/hr"),
    UNITS_121("normal cubic meter per hour", "normal m^3/h"),
    UNITS_122("normal liter per hour", "normal L/h"),
    UNITS_123("standard cubic feet per minute", "standard ft^3/min"),
    UNITS_124("bbl liq", "bbl liq"),
    UNITS_125("ounce", "oz"),
    UNITS_126("foot pound force", "ft lb force"),
    UNITS_127("kilo watt", "kW"),
    UNITS_128("kilo watt hour", "kW h"),
    UNITS_129("horsepower", "hp"),
    UNITS_130("cubic feet per hour", "ft^3/h"),
    UNITS_131("cubic meters per minute", "m^3/min"),
    UNITS_132("barrels per second", "bbl/s"),
    UNITS_133("barrels per minute", "bbl/min"),
    UNITS_134("barrels per hour", "bbl/h"),
    UNITS_135("barrels per day", "bbl/d"),
    UNITS_136("gallons per hour", "usg/h"),
    UNITS_137("imperial gallons per second", "impgal/s"),
    UNITS_138("liters per hour", "L/h"),
    UNITS_139("parts per million", "ppm"),
    UNITS_140("mega calorie per hour", "Mcal/h"),
    UNITS_141("mega joule per hour", "MJ/h"),
    UNITS_142("british thermal unit per hour", "BTU/h"),
    UNITS_143("degrees", "degrees"),
    UNITS_144("radian", "rad"),
    UNITS_145("inches of water at 15.6 C (60 F)", "inH2O (15.6 C or 60 F)"),
    UNITS_146("micrograms per liter", "micrograms/L"),
    UNITS_147("micrograms per cubic meter", "micrograms/cubic m"),
    UNITS_148("percent consistency", "% consistency"),
    UNITS_149("volume percent", "volume %"),
    UNITS_150("percent steam quality", "% steam quality"),
    UNITS_151("feet in sixteenths", "ft in sixteenths"),
    UNITS_152("cubic feet per pound", "ft^3/lb"),
    UNITS_153("picofarads", "pF"),
    UNITS_154("mililiters per liter", "mL/L"),
    UNITS_155("microliters per liter", "microliters/L"),
    UNITS_156("percent plato", "% plato"),
    UNITS_157("percent lower explosion level", "% lower explosion level"),
    UNITS_158("mega calorie", "Mcal"),
    UNITS_159("Kohms", "kohm"),
    UNITS_160("mega joule", "MJ"),
    UNITS_161("british thermal unit", "BTU"),
    UNITS_162("normal cubic meter normal", "cubic m"),
    UNITS_163("normal liter", "normal L"),
    UNITS_164("standard cubic feet", "normal ft^3"),
    UNITS_165("parts per billion", "parts/billion"),
    UNITS_235("gallons per day", "usg/d"),
    UNITS_236("hectoliters", "hL"),
    UNITS_237("megapascals", "MPa"),
    UNITS_238("inches of water at 4 C (39.2 F)", "inH2O (4 C or 39.2 F)"),
    UNITS_239("millimeters of water at 4 C (39.2 F)", "mmH2O (4 C or 39.2 F)"),
    UNITS_240("manufacturer specific", "manufacturer specific"),
    UNITS_250("not used", "not used"),
    UNITS_251("dimensionless", ""),
    UNITS_252("unknown", "unknown");

    /**
     * A longer, human readable string that describes the units.
     */
    private final String longUnits;

    /**
     * An abbreviated, human readable string for the same units described in the {@link #longUnits} property.
     */
    private final String shortUnits;

    /**
     * Constructor.
     * 
     * @param longUnits
     *            Long, human readable units.
     * @param shortUnits
     *            Abbreviated units.
     */
    private EngineeringUnitsType(String longUnits, String shortUnits) {
        this.longUnits = longUnits;
        this.shortUnits = shortUnits;
    }

    /**
     * Returns a human readable units string, long protocolVersion. See {@link #getShortUnits()}.
     * 
     * @return The longer units string.
     */
    public String getLongUnits() {
        return this.longUnits;
    }

    /**
     * Returns an abbreviated, human readable units string, shorter protocolVersion. See {@link #getLongUnits()}.
     * 
     * @return The abbreviated units string.
     */
    public String getShortUnits() {
        return this.shortUnits;
    }

    /**
     * Determine engineering units from input byte.
     *
     * @param engineeringUnitsByte
     *            The units byte.
     * @return The {@link EngineeringUnitsType} instance found inside the units byte.
     * @throws EngineeringUnitsException
     *             If the argument does not contain a recognized units code.
     */
    public static EngineeringUnitsType fromEngineeringUnitsByte(byte engineeringUnitsByte)
            throws EngineeringUnitsException {
        for (EngineeringUnitsType unitCode : EngineeringUnitsType.values()) {
            byte testByte = unitCode.toEngineeringUnitsByte();

            if (testByte == engineeringUnitsByte) {
                return unitCode;
            }
        }
        throw new EngineeringUnitsException(
                "An unrecognized engineering units was found in engineeringUnitsByte byte: " + engineeringUnitsByte);
    }

    /**
     * Return the engineering units byte.
     * 
     * @return The engineering units byte.
     */
    public byte toEngineeringUnitsByte() {
        return this.getEngineeringUnitsId();
    }

    /**
     * Return the engineering units code number, as defined in the HART specification.
     *
     * @return The engineering units code number.
     */
    public byte getEngineeringUnitsId() {
        String numericPart = this.name().substring(this.name().indexOf('_') + 1);
        return Integer.valueOf(numericPart).byteValue();
    }

}
