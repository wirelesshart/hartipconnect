package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * This class implements the Byte counting state which knows how to read and create the "Byte Count" bytes in the
 * packet. This only needs one byte as input and produces one byte of output. The Byte Count determines the size of the
 * data packet. Please refer to {@link DataState} for additional information.
 */
class ByteCountState implements State {

    ByteCountState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) {
        context.setDataByteCount(newByte);

        context.setActiveState(new DataState());
    }

    @Override
    public void encodePacket(HartPdu requestHartPdu, StateContextForEncoder context) {
        CommandData packet = requestHartPdu.getCommandData();

        final int dataPacketByteCount;
        if (packet != null) {
            dataPacketByteCount = packet.getTotalDataByteCount();
        } else {
            dataPacketByteCount = 0;
        }

        /* For STX frames, the total byte count is simply the size of the data packet.
         * 
         * For Non STX Frames, in addition to the data packet size, there are two more bytes that may be added. The
         * first of these bytes may contain a Communication Error byte if a communication error was encountered, or the
         * actual status response from executing the command, respectively. The second byte contains a device status
         * byte. After these two initial bytes, then the data packet is appended (if no communication errors were
         * encountered) or no data is appended (if a communication error was encountered).
         * 
         * For further information please refer to the Data state */
        final int totalByteCount;
        if (requestHartPdu.getFrameType() == FrameType.STX) {
            totalByteCount = dataPacketByteCount;
        } else {
            if (requestHartPdu.getCommunicationErrors() == null || requestHartPdu.getCommunicationErrors().length == 0) {
                totalByteCount = 2 + dataPacketByteCount;
            } else {
                totalByteCount = 2;
            }
        }

        context.setDataByteCount(totalByteCount);

        context.getOutputBytes().add((byte) totalByteCount);

        context.setActiveState(new DataState());
    }

}
