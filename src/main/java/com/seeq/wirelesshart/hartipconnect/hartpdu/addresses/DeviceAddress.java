package com.seeq.wirelesshart.hartipconnect.hartpdu.addresses;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

/**
 * This is used to identify a Device as it shall be stored inside the {@link HartPdu} class, dependent on the
 * {@link AddressType} instance. The recognized HART address types are one of {@link LongFrameAddress} and
 * {@link ShortFrameAddress}, both of which should implement this interface.
 */
public interface DeviceAddress {

    /**
     * Return the {@link AddressType} for this addressing.
     *
     * @return The address type.
     */
    public AddressType getAddressType();

    /**
     * Get if this is the primary master.
     *
     * @return True if this is the primary master, otherwise false denotes a secondary master.
     */
    public boolean isPrimaryMaster();

    /**
     * Set whether this is the primary master or secondary master.
     *
     * @param isPrimaryMaster
     *            If true, then primary master. False denotes a secondary master.
     */
    public void setPrimaryMaster(boolean isPrimaryMaster);

    /**
     * Is the device in burst mode?
     *
     * @return True if the device is in burst mode, false otherwise.
     */
    public boolean isDeviceInBurstMode();

    /**
     * Set whether the target device is in burst mode.
     *
     * @param isDeviceInBurstMode
     *            True if the target device is in burst mode, false otherwise.
     */
    public void setDeviceInBurstMode(boolean isDeviceInBurstMode);

    /**
     * Hash code implementation.
     * 
     * @return The hashcode.
     */
    @Override
    public int hashCode();

    /**
     * Equality.
     * 
     * @param obj
     *            The object against which we are testing for equality.
     * @return True if the two objects are equal.
     */
    @Override
    public boolean equals(Object obj);

}
