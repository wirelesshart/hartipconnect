package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * This class is used as a generic {@link CommandData} packet, which acts as the data needed for commands embedded
 * inside a {@link HartPdu}. Specifically, this class is used as a default command, such that if no specific version of
 * a command is defined, this will act as a generic command, taking in any number of bytes and storing them --
 * unprocessed. It would then be the responsibility of the user to implement their own implementation of the command by
 * interpreting the stored byte array.
 */
public class GenericCommandData implements CommandData {

    protected CommandRegistry commandRegistry = null;
    private FrameType frameType;

    protected byte[] bytes;

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public GenericCommandData() {}

    /**
     * Get the raw byte storage, untouched.
     * 
     * @return The internal byte storage.
     */
    public byte[] getRawBytes() {
        return this.bytes;
    }

    /**
     * Constructor, with {@link FrameType} and {@link CommandRegistry} initialized.
     * 
     * @param commandRegistry
     *            The Command Type.
     * @param frameType
     *            The Frame Type.
     */
    public GenericCommandData(CommandRegistry commandRegistry, FrameType frameType) {
        this.commandRegistry = commandRegistry;
        this.frameType = frameType;
    }

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation, or null if there is nothing to represent.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();
        if (this.bytes != null) {
            for (byte encodeByte : this.bytes) {
                encodedBytes.add(encodeByte);
            }
        }
        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation, or null, if no data is expected.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) {
        if (bytesToDecode == null || bytesToDecode.isEmpty()) {
            this.bytes = null;
            return;
        }
        this.bytes = new byte[bytesToDecode.size()];
        for (int index = 0; index < bytesToDecode.size(); index++) {
            this.bytes[index] = bytesToDecode.get(index);
        }
    }

    /**
     * Set the {@link CommandRegistry} instance.
     * 
     * <b>Note:</b> This is only supported for the {@link GenericCommandData} class and must be called during
     * initialization.
     * 
     * @param commandRegistry
     *            The {@link CommandRegistry} instance.
     */
    public void setCommandType(CommandRegistry commandRegistry) {
        this.commandRegistry = commandRegistry;
    }

    /**
     * Set the frame type.
     * 
     * <b>Note:</b> This is only supported for the {@link GenericCommandData} class, and must be called during
     * initialization.
     * 
     * @param frameType
     *            The{@link FrameType} instance.
     */
    public void setFrameType(FrameType frameType) {
        this.frameType = frameType;
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public boolean equals(CommandData other) {
        if (this == other) {
            return true;
        } else if (other == null) {
            return false;
        } else if (!GenericCommandData.class.isAssignableFrom(other.getClass())) {
            return false;
        } else if (!Arrays.equals(this.bytes, ((GenericCommandData) other).bytes)) {
            return false;
        } else if (this.commandRegistry != ((GenericCommandData) other).commandRegistry) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("GenericCommandData [commandRegistry=").append(this.commandRegistry);

        if (this.bytes == null) {
            builder.append(", bytes(0)=");
            builder.append("null");
        } else {
            builder.append(", bytes(" + this.bytes.length + ")=");
            for (byte outByte : this.bytes) {
                builder.append(String.format("0x%x ", outByte));
            }
        }

        builder.append("]");
        return builder.toString();
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return this.toBytes().size();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(this.bytes);
        result = prime * result + ((this.commandRegistry == null) ? 0 : this.commandRegistry.hashCode());
        result = prime * result + ((this.frameType == null) ? 0 : this.frameType.hashCode());
        return result;
    }
}
