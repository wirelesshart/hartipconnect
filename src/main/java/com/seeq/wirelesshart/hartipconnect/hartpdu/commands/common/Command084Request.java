package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * The Request packet for Command #84 -- Read Sub-Device Identity Summary
 * 
 * This command request takes only an index to the device of interest.
 * 
 */
public class Command084Request implements CommandData {

    protected static final int TOTAL_BYTE_COUNT = 2;
    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_084;
    private final FrameType frameType = FrameType.STX;

    private short deviceNumber;

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public Command084Request() {}

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();

        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.deviceNumber)));

        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) throws EngineeringUnitsException {
        if (bytesToDecode.size() != TOTAL_BYTE_COUNT) {
            throw new IllegalArgumentException("Expected a length of " + TOTAL_BYTE_COUNT + ", but received "
                    + bytesToDecode.size());
        }

        this.deviceNumber = HartUtilities
                .shortFromBytes(HartUtilities.byteListToByteArray(bytesToDecode.subList(0, 2)));
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return TOTAL_BYTE_COUNT;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.deviceNumber;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command084Request.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command084Request other = (Command084Request) obj;
        return this.equals(other);
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command084Request.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Command084Request other = (Command084Request) obj;
        if (this.deviceNumber != other.deviceNumber) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Command084Request ["
                + "commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", deviceNumber=" + this.deviceNumber
                + "]";
    }

    /**
     * Get the index of the device being queried.
     * 
     * @return The device index.
     */
    public short getDeviceNumber() {
        return this.deviceNumber;
    }

    /**
     * Set the index of the device being queried.
     * 
     * @param deviceNumber
     *            The device index.
     */
    public void setDeviceNumber(short deviceNumber) {
        this.deviceNumber = deviceNumber;
    }
}
