package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;

/**
 * Class that implements the state that parses the Delimiter byte. Specifically, it encodes/decodes the following
 * information:
 * 
 * <ul>
 * <li>The address type ({@link AddressType})</li>
 * <li>Physical layer type ({@link PhysicalLayerType})</li>
 * <li>Frame type ({@link FrameType})
 * <li>Number of expansion bytes</li>
 * </ul>
 */
class DelimeterState implements State {
    private static final byte LOWEST_TWO_BITS_MASK = (byte) 0x03;

    DelimeterState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        context.setOutputHartPdu(new HartPdu());

        context.getOutputHartPdu().setDeviceAddress(AddressType.fromDelimeterByte(newByte).createNewClassInstance());
        context.getOutputHartPdu().setPhysicalLayerType(PhysicalLayerType.fromDelimeterByte(newByte));
        context.getOutputHartPdu().setFrameType(FrameType.fromDelimeterByte(newByte));

        // extract the number of expansion bytes.
        context.setNumberOfExpansionBytes((newByte >> 5) & LOWEST_TWO_BITS_MASK);

        context.setActiveState(new AddressState());
    }

    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) {
        int numberOfExpansionBytes = hartPdu.getExpansionBytes() == null ? 0 : hartPdu.getExpansionBytes().length;

        byte delimeterByte = hartPdu.getDeviceAddress().getAddressType().appendToDelimeterByte((byte) 0x00);
        delimeterByte = hartPdu.getPhysicalLayerType().appendToDelimeterByte(delimeterByte);
        delimeterByte = hartPdu.getFrameType().appendToDelimeterByte(delimeterByte);

        // clear the lowest two bits prior to setting them.
        delimeterByte &= (byte) ~(LOWEST_TWO_BITS_MASK << 5);
        delimeterByte |= (byte) ((numberOfExpansionBytes & LOWEST_TWO_BITS_MASK) << 5);

        context.getOutputBytes().clear();
        context.getOutputBytes().add(delimeterByte);

        context.setActiveState(new AddressState());
    }
}
