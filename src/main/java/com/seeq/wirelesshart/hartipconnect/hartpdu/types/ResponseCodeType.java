package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.ResponseException;

/**
 * This enumerates the recognized response codes to be returned by the remote device, and their associated
 * {@link ResponseClassificationType}.
 */
public enum ResponseCodeType {

    RC_000(ResponseClassificationType.IS_NOTIFICATION, "Success"),
    RC_001(ResponseClassificationType.IS_ERROR_SINGLE, "Undefined"),
    RC_002(ResponseClassificationType.IS_ERROR_SINGLE, "Invalid Selection"),
    RC_003(ResponseClassificationType.IS_ERROR_SINGLE, "Passed Parameter Too Large"),
    RC_004(ResponseClassificationType.IS_ERROR_SINGLE, "Passed Parameter Too Small"),
    RC_005(ResponseClassificationType.IS_ERROR_SINGLE, "Too Few Data Bytes Received"),
    RC_006(ResponseClassificationType.IS_ERROR_SINGLE, "Device Specific Command Error"),
    RC_007(ResponseClassificationType.IS_ERROR_SINGLE, "In Write Protect Mode"),
    RC_008(ResponseClassificationType.IS_WARNING_MULTI,
            "Update Failure"
                    + "/Set to Nearest Possible Value"
                    + "/All but running delayed responses flushed"),
    RC_009(ResponseClassificationType.IS_ERROR_MULTI,
            "Lower Range Value Too High"
                    + "/Applied Process Too High"
                    + "/Not In Proper Current Mode"),
    RC_010(ResponseClassificationType.IS_ERROR_MULTI,
            "Lower Range Value Too Low"
                    + "/Applied Process Too Low"
                    + "/Invalid Local Panel Lock Code"),
    RC_011(ResponseClassificationType.IS_ERROR_MULTI,
            "Upper Range Value Too High"
                    + "/In Multi-drop Mode/Invalid Device Variable Code"
                    + "/Trim Error, Excess Correction Attempted"
                    + "/Cannot Lock Panel"),
    RC_012(ResponseClassificationType.IS_ERROR_MULTI,
            "Upper Range Value Too Low"
                    + "/Invalid Units Code"
                    + "/Invalid Slot Number"
                    + "/Invalid Mode Selection"),
    RC_013(ResponseClassificationType.IS_ERROR_MULTI,
            "Invalid Transfer Function Code"
                    + "/Upper and Lower Range Values Out of Limits"
                    + "/Computation Error"
                    + "/Command Number Not Supported"),
    RC_014(ResponseClassificationType.IS_WARNING_MULTI,
            "Span Too Small"
                    + "/New Lower Range Value Pushed Upper Range Value Over Transducer Limit"),
    RC_015(ResponseClassificationType.IS_ERROR_MULTI, "Invalid Analog Channel Code Number"),
    RC_016(ResponseClassificationType.IS_ERROR_SINGLE, "Access Restricted"),
    RC_017(ResponseClassificationType.IS_ERROR_SINGLE, "Invalid Device Variable Index"),
    RC_018(ResponseClassificationType.IS_ERROR_SINGLE, "Invalid Units Code"),
    RC_019(ResponseClassificationType.IS_ERROR_SINGLE, "Device Variable Index Not Allowed"),
    RC_020(ResponseClassificationType.IS_ERROR_SINGLE, "Invalid Extended Command Number"),
    RC_021(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_022(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_023(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_024(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_025(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_026(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_027(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_028(ResponseClassificationType.IS_ERROR_MULTI, "Invalid Range Units Code"),
    RC_029(ResponseClassificationType.IS_ERROR_MULTI, "(Command Specific)"),
    RC_030(ResponseClassificationType.IS_WARNING_MULTI, "(Command Specific)"),
    RC_031(ResponseClassificationType.IS_WARNING_MULTI, "(Command Specific)"),
    RC_032(ResponseClassificationType.IS_ERROR_SINGLE, "Busy"),
    RC_033(ResponseClassificationType.IS_ERROR_SINGLE, "Delayed Response Initiated"),
    RC_034(ResponseClassificationType.IS_ERROR_SINGLE, "Delayed Response Running"),
    RC_035(ResponseClassificationType.IS_ERROR_SINGLE, "Delayed Response Dead"),
    RC_036(ResponseClassificationType.IS_ERROR_SINGLE, "Delayed Response Conflict"),
    RC_037(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_038(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_039(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_040(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_041(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_042(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_043(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_044(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_045(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_046(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_047(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_048(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_049(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_050(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_051(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_052(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_053(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_054(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_055(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_056(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_057(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_058(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_059(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_060(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_061(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_062(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_063(ResponseClassificationType.IS_ERROR_SINGLE, "Reserved"),
    RC_064(ResponseClassificationType.IS_ERROR_SINGLE, "Command Not Implemented"),
    RC_065(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_066(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_067(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_068(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_069(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_070(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_071(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_072(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_073(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_074(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_075(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_076(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_077(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_078(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_079(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_080(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_081(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_082(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_083(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_084(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_085(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_086(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_087(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_088(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_089(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_090(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_091(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_092(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_093(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_094(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_095(ResponseClassificationType.IS_ERROR_MULTI, "Reserved"),
    RC_096(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_097(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_098(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_099(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_100(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_101(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_102(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_103(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_104(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_105(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_106(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_107(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_108(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_109(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_110(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_111(ResponseClassificationType.IS_WARNING_SINGLE, "Reserved"),
    RC_112(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_113(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_114(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_115(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_116(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_117(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_118(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_119(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_120(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_121(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_122(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_123(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_124(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_125(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_126(ResponseClassificationType.IS_WARNING_MULTI, "Reserved"),
    RC_127(ResponseClassificationType.IS_WARNING_MULTI, "Reserved");

    private final ResponseClassificationType classification;
    private final String humanString;

    private ResponseCodeType(ResponseClassificationType classification, String humanString) {
        this.classification = classification;
        this.humanString = humanString;
    }

    /**
     * Code ID for this response code.
     *
     * @return the codeID
     */
    public byte getCodeID() {
        String integerPart = this.name().substring(this.name().indexOf('_') + 1);
        return Byte.valueOf(integerPart).byteValue();
    }

    /**
     * Human readable string for this error message.
     *
     * @return the humanString
     */
    public String getHumanString() {
        return this.humanString;
    }

    /**
     * Classification for the response.
     *
     * @return A {@link ResponseClassificationType} instance.
     */
    public ResponseClassificationType getClassification() {
        return this.classification;
    }

    /**
     * Extract the response code from the input byte.
     *
     * @param responseByte
     *            The byte to test for a response code.
     *
     * @return The response code.
     *
     * @throws HartSystemException
     *             Thrown if the supplied responseByte does not contain a valid {@link ResponseCodeType} byte.
     */
    public static ResponseCodeType fromResponseByte(byte responseByte) throws HartSystemException {
        for (ResponseCodeType type : ResponseCodeType.values()) {
            if (responseByte == type.getCodeID()) {
                return type;
            }
        }
        throw new ResponseException("Unknown response code in byte " + responseByte);
    }

    /**
     * Generate a response byte. This is intended to be a symmetric to {@link ResponseCodeType#fromResponseByte} above.
     *
     * @return The response byte.
     */
    public byte toResponseCodeByte() {
        return (byte) (0xff & this.getCodeID());
    }
}
