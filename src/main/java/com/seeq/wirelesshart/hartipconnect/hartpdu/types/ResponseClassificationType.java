package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.seeq.wirelesshart.hartipconnect.exceptions.response.ErrorMultiResponseException;
import com.seeq.wirelesshart.hartipconnect.exceptions.response.ErrorSingleResponseException;
import com.seeq.wirelesshart.hartipconnect.exceptions.response.HartResponseException;

/**
 * Types of Response codes.
 */
public enum ResponseClassificationType {

    /**
     * Device reported a single definition error during the execution of the command.
     */
    IS_ERROR_SINGLE(ErrorSingleResponseException.class),

    /**
     * Device reported a multi-definition error during the execution of the command.
     */
    IS_ERROR_MULTI(ErrorMultiResponseException.class),

    /**
     * Device reported a single definition warning during the execution of the command.
     */
    IS_WARNING_SINGLE,

    /**
     * Device reported a multi-definition warning during the execution of the command.
     */
    IS_WARNING_MULTI,

    /**
     * Device reported success during the execution of the command. Here, the {@link ResponseCodeType#RC_000} will be
     * returned along with the command output data.
     */
    IS_NOTIFICATION;

    private final Class<? extends HartResponseException> exceptionClass;

    /**
     * If an exception class is specified, then this error is intended to throw an Exception.
     * 
     * @param exceptionClass
     *            The class used as the Exception.
     */
    private ResponseClassificationType(Class<? extends HartResponseException> exceptionClass) {
        this.exceptionClass = exceptionClass;
    }

    /**
     * If no classes are specified, assume that this does not throw an exception.
     */
    private ResponseClassificationType() {
        this(null);
    }

    /**
     * Get the class type for this classification, or null if none.
     * 
     * @return The classification type.
     */
    public Class<? extends HartResponseException> getExceptionClass() {
        return this.exceptionClass;
    }

    /**
     * Create an instance of an Exception, if needed.
     * 
     * @return The exception instance, or null if none are required (eg. the classification is not an error).
     */
    public HartResponseException createNewInstanceOfException() {
        if (this.exceptionClass == null) {
            return null;
        } else {
            try {
                return this.exceptionClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                Logger.getLogger(ResponseClassificationType.class.getName()).log(Level.WARNING,
                        "Unable to instantiate class " + this.exceptionClass.getName(), e);
                throw new IllegalStateException("Unable to instantiate class " + this.exceptionClass.getName(), e);
            }
        }
    }
}
