package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericCommandData;

/**
 * This implements the Command state, which determines and interprets the command byte. Specifically, this tells us
 * which command is to be, or was, executed in the request/response packets, respectively.
 */
class CommandState implements State {

    CommandState() {}

    @Override
    public void decodeByte(byte commandByte, StateContextForDecoder context) throws HartException {
        CommandRegistry command = CommandRegistry.fromCommandByte(commandByte);

        /* Note that command 31 (extended command set) is not presently supported by this library. Command 31 uses the
         * data section to create its own commands. Fortunately, we don't need it for our first thrust into
         * wirelessHART. */
        if (command == CommandRegistry.COMMAND_031) {
            throw new UnsupportedOperationException(
                    "This protocolVersion of the library does not support extended command sets");
        }

        final CommandData packet = command.createNewInstanceOfStrategy(context.getOutputHartPdu().getFrameType(),
                context.getOutputHartPdu().getDeviceAddress());

        if (packet.getClass() == GenericCommandData.class) {
            ((GenericCommandData) packet).setCommandType(command);
            ((GenericCommandData) packet).setFrameType(context.getOutputHartPdu().getFrameType());
        }

        context.getOutputHartPdu().setCommandData(packet);

        context.getWorkspaceBytes().clear();

        context.setActiveState(new ByteCountState());
    }

    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) throws HartException {

        /* Note that command 31 (extended command set) is not presently supported by this library. Command 31 uses the
         * data section to create its own commands. Fortunately, we don't need it for our first thrust into
         * wirelessHART. */
        if (hartPdu.getCommandType() == CommandRegistry.COMMAND_031) {
            throw new UnsupportedOperationException(
                    "This protocolVersion of the library does not support extended command sets");
        }

        byte commandByte = hartPdu.getCommandType().getCommandID();

        context.getOutputBytes().add(commandByte);

        context.setActiveState(new ByteCountState());
    }

}
