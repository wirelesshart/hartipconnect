package com.seeq.wirelesshart.hartipconnect.hartpdu.commands;

import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * This interface represents a command data packet, used to store request or response data. The stored data is specific
 * to the individual response/request behaviors of each {@link CommandRegistry}.
 */
public interface CommandData {

    /**
     * Obtain the {@link CommandRegistry} instance for this packet.
     * 
     * @return The {@link CommandRegistry} instance.
     */
    public CommandRegistry getCommandType();

    /**
     * Obtain the {@link FrameType} instance for this packet. This is useful in differentiating from the Response and
     * Request types of packets.
     * 
     * @return The {@link FrameType} instance.
     */
    public FrameType getFrameType();

    /**
     * Test for equality against other {@link CommandData}
     * 
     * @param other
     *            The other command packet against which we are comparing.
     * @return true if both packets are equal.
     */
    public boolean equals(CommandData other);

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation. This should never return null and should only return an empty list if
     *         no data is needed.
     * @throws HartException
     *             If an exception occurs during the encoding process.
     */
    public List<Byte> toBytes() throws HartException;

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation. If null, it shall be treated the same as an empty list.
     * @throws HartException
     *             If an exception occurs during the decoding process.
     */
    public void fromBytes(List<Byte> bytesToDecode) throws HartException;

    /**
     * Get a byte count for our instance.
     *
     * @return The resulting number of data bytes.
     */
    public int getTotalDataByteCount();

    /**
     * Hash code implementation.
     * 
     * @return The hashcode.
     */
    @Override
    public int hashCode();

    /**
     * Equality.
     * 
     * @param obj
     *            The object against which we are testing for equality.
     * @return True if the two objects are equal.
     */
    @Override
    public boolean equals(Object obj);
}
