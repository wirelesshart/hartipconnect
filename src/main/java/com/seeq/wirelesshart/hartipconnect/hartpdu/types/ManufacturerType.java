package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.ManufacturerException;

/**
 * The list of recognized manufacturers and their manufacturer ID, as defined by the HART Communication Foundation.
 */
public enum ManufacturerType {
    MANUFACTURER_001("Acromag"),
    MANUFACTURER_002("Allen-Bradley"),
    MANUFACTURER_003("Ametek"),
    MANUFACTURER_004("Analog Devices"),
    MANUFACTURER_005("ABB"),
    MANUFACTURER_006("Beckman"),
    MANUFACTURER_007("Bell Microsenser"),
    MANUFACTURER_008("Bourns"),
    MANUFACTURER_009("Bristol Babcock"),
    MANUFACTURER_010("Brooks Instrument"),
    MANUFACTURER_011("Chessell"),
    MANUFACTURER_012("Combustion Engineering"),
    MANUFACTURER_013("Daniel Industries"),
    MANUFACTURER_014("Delta"),
    MANUFACTURER_015("Dieterich Standard"),
    MANUFACTURER_016("Dohrmann"),
    MANUFACTURER_017("Endress+Hauser"),
    MANUFACTURER_018("ABB"),
    MANUFACTURER_019("Fisher Controls"),
    MANUFACTURER_020("Foxboro"),
    MANUFACTURER_021("Fuji"),
    MANUFACTURER_022("ABB"),
    MANUFACTURER_023("Honeywell"),
    MANUFACTURER_024("ITT Barton"),
    MANUFACTURER_025("Thermo MeasureTech"),
    MANUFACTURER_026("ABB"),
    MANUFACTURER_027("Leeds & Northup"),
    MANUFACTURER_028("Leslie"),
    MANUFACTURER_029("M-System Co."),
    MANUFACTURER_030("Measurex"),
    MANUFACTURER_031("Micro Motion"),
    MANUFACTURER_032("Moore Industries"),
    MANUFACTURER_033("PRIME Measurement Products"),
    MANUFACTURER_034("Ohkura Electric"),
    MANUFACTURER_035("Paine"),
    MANUFACTURER_036("Rochester Instrument Systems"),
    MANUFACTURER_037("Ronan"),
    MANUFACTURER_038("Rosemount"),
    MANUFACTURER_039("Peek Measurement"),
    MANUFACTURER_040("Actaris Neptune"),
    MANUFACTURER_041("Sensall"),
    MANUFACTURER_042("Siemens"),
    MANUFACTURER_043("Weed"),
    MANUFACTURER_044("Toshiba"),
    MANUFACTURER_045("Transmation"),
    MANUFACTURER_046("Rosemount Analytic"),
    MANUFACTURER_047("Metso Automation"),
    MANUFACTURER_048("Flowserve"),
    MANUFACTURER_049("Varec"),
    MANUFACTURER_050("Viatran"),
    MANUFACTURER_051("Delta/Weed"),
    MANUFACTURER_052("Westinghouse"),
    MANUFACTURER_053("Xomox"),
    MANUFACTURER_054("Yamatake"),
    MANUFACTURER_055("Yokogawa"),
    MANUFACTURER_056("Nuovo Pignone"),
    MANUFACTURER_057("Promac"),
    MANUFACTURER_058("Exac Corporation"),
    MANUFACTURER_059("Mobrey"),
    MANUFACTURER_060("Arcom Control System"),
    MANUFACTURER_061("Princo"),
    MANUFACTURER_062("Smar"),
    MANUFACTURER_063("Foxboro Eckardt"),
    MANUFACTURER_064("Measurement Technology"),
    MANUFACTURER_065("Applied System Technologies"),
    MANUFACTURER_066("Samson"),
    MANUFACTURER_067("Sparling Instruments"),
    MANUFACTURER_068("Fireye"),
    MANUFACTURER_069("Krohne"),
    MANUFACTURER_070("Betz"),
    MANUFACTURER_071("Druck"),
    MANUFACTURER_072("SOR"),
    MANUFACTURER_073("Elcon Instruments"),
    MANUFACTURER_074("EMCO"),
    MANUFACTURER_075("Termiflex Corporation"),
    MANUFACTURER_076("VAF Instruments"),
    MANUFACTURER_077("Westlock Controls"),
    MANUFACTURER_078("Drexelbrook"),
    MANUFACTURER_079("Saab Tank Control"),
    MANUFACTURER_080("K-TEK"),
    MANUFACTURER_081("SENSIDYNE, INC"),
    MANUFACTURER_082("Draeger"),
    MANUFACTURER_083("Raytek"),
    MANUFACTURER_084("Siemens Milltronics PI"),
    MANUFACTURER_085("BTG"),
    MANUFACTURER_086("Magnetrol"),
    MANUFACTURER_087("Metso Automation"),
    MANUFACTURER_088("Siemens Milltronics PI"),
    MANUFACTURER_089("HELIOS"),
    MANUFACTURER_090("Anderson Instrument Company"),
    MANUFACTURER_091("INOR"),
    MANUFACTURER_092("ROBERTSHAW"),
    MANUFACTURER_093("PEPPERL+FUCHS"),
    MANUFACTURER_094("ACCUTECH"),
    MANUFACTURER_095("Flow Measurement"),
    MANUFACTURER_096("Courdon-Haenni"),
    MANUFACTURER_097("Knick"),
    MANUFACTURER_098("VEGA"),
    MANUFACTURER_099("MTS Systems Corp."),
    MANUFACTURER_100("Oval"),
    MANUFACTURER_101("Masoneilan-Dresser"),
    MANUFACTURER_102("BESTA"),
    MANUFACTURER_103("Ohmart"),
    MANUFACTURER_104("Harold Beck and Sons"),
    MANUFACTURER_105("rittmeyer instrumentation"),
    MANUFACTURER_106("Rossel Messtechnik"),
    MANUFACTURER_107("WIKA"),
    MANUFACTURER_108("Bopp & Reuther Heinrichs"),
    MANUFACTURER_109("PR Electronics"),
    MANUFACTURER_110("Jordan Controls"),
    MANUFACTURER_111("Valcom s.r.l."),
    MANUFACTURER_112("US ELECTRIC MOTORS"),
    MANUFACTURER_113("Apparatebau Hundsbach"),
    MANUFACTURER_114("Dynisco"),
    MANUFACTURER_115("Spriano"),
    MANUFACTURER_116("Direct Measurement"),
    MANUFACTURER_117("Klay Instruments"),
    MANUFACTURER_118("CiDRA CORP."),
    MANUFACTURER_119("MMG AM DTR"),
    MANUFACTURER_120("Buerkert Fluid Control Systems"),
    MANUFACTURER_121("AALIANT Process Mgt"),
    MANUFACTURER_122("PONDUS INSTRUMENTS"),
    MANUFACTURER_123("ZAP S.A. Ostrow Wielkopolski"),
    MANUFACTURER_124("GLI"),
    MANUFACTURER_125("Fisher-Rosemount Performance Technologies"),
    MANUFACTURER_126("Paper Machine Components"),
    MANUFACTURER_127("LABOM"),
    MANUFACTURER_128("Danfoss"),
    MANUFACTURER_129("Turbo"),
    MANUFACTURER_130("TOKYO KEISO"),
    MANUFACTURER_131("SMC"),
    MANUFACTURER_132("Status Instruments"),
    MANUFACTURER_133("Huakong"),
    MANUFACTURER_134("Duon System"),
    MANUFACTURER_135("Vortek Instruments, LLC"),
    MANUFACTURER_136("AG Crosby"),
    MANUFACTURER_137("Action Instruments"),
    MANUFACTURER_138("Keystone Controls"),
    MANUFACTURER_139("Thermo Electronic Co."),
    MANUFACTURER_140("ISE Magtech"),
    MANUFACTURER_141("Rueger"),
    MANUFACTURER_142("Mettler Toledo"),
    MANUFACTURER_143("Det-Tronics"),
    MANUFACTURER_144("Thermo MeasureTech"),
    MANUFACTURER_145("DeZURIK"),
    MANUFACTURER_146("Phase Dynamics"),
    MANUFACTURER_147("WELLTECH SHANGHAI"),
    MANUFACTURER_148("ENRAF"),
    MANUFACTURER_149("4tech ASA"),
    MANUFACTURER_150("Brandt Instruments"),
    MANUFACTURER_151("Nivelco"),
    MANUFACTURER_152("Camille Bauer"),
    MANUFACTURER_153("Metran"),
    MANUFACTURER_154("Milton Roy Co."),
    MANUFACTURER_155("PMV"),
    MANUFACTURER_156("Turck"),
    MANUFACTURER_157("Panametrics"),
    MANUFACTURER_158("R. Stahl"),
    MANUFACTURER_159("Analytical Technologies Inc."),
    MANUFACTURER_160("FINT"),
    MANUFACTURER_161("BERTHOLD"),
    MANUFACTURER_162("InterCorr"),
    MANUFACTURER_163("China BRICONTE Co Ltd"),
    MANUFACTURER_164("Electron Machine"),
    MANUFACTURER_165("Sierra Instruments"),
    MANUFACTURER_166("Fluid Components Intl"),
    MANUFACTURER_167("Solid AT"),
    MANUFACTURER_168("Meriam Instrument"),
    MANUFACTURER_169("Invensys"),
    MANUFACTURER_170("S-Products"),
    MANUFACTURER_171("Tyco Valves & Controls"),
    MANUFACTURER_172("Micro Matic Instrument A/S"),
    MANUFACTURER_173("J-Tec Associates"),
    MANUFACTURER_174("TRACERCO"),
    MANUFACTURER_175("AGAR"),
    MANUFACTURER_176("Phoenix Contact"),
    MANUFACTURER_177("Andean Instruments"),
    MANUFACTURER_178("American Level Instrument"),
    MANUFACTURER_179("Hawk"),
    MANUFACTURER_180("YTC"),
    MANUFACTURER_181("Pyromation Inc."),
    MANUFACTURER_182("Satron Instruments"),
    MANUFACTURER_183("BIFFI"),
    MANUFACTURER_184("SAIC"),
    MANUFACTURER_185("BD Sensors"),
    MANUFACTURER_186("Andean Instruments"),
    MANUFACTURER_187("Kemotron"),
    MANUFACTURER_188("APLISENS"),
    MANUFACTURER_189("Badger Meter"),
    MANUFACTURER_190("HIMA"),
    MANUFACTURER_191("GP:50"),
    MANUFACTURER_192("Kongsberg Maritime"),
    MANUFACTURER_193("ASA S.p.A."),
    MANUFACTURER_194("Hengesbach"),
    MANUFACTURER_195("Lanlian Instruments"),
    MANUFACTURER_196("Spectrum Controls"),
    MANUFACTURER_197("Kajaani Process Measurements"),
    MANUFACTURER_198("FAFNIR"),
    MANUFACTURER_199("SICK-MAIHAK"),
    MANUFACTURER_200("JSP Nova Paka"),
    MANUFACTURER_201("MESACON"),
    MANUFACTURER_202("Spirax Sarco Italy"),
    MANUFACTURER_203("L&J TECHNOLOGIES"),
    MANUFACTURER_204("Tecfluid S.A."),
    MANUFACTURER_205("Sailsors Instruments"),
    MANUFACTURER_206("Roost"),
    MANUFACTURER_207("KOSO"),
    MANUFACTURER_208("MJK"),
    MANUFACTURER_209("GE Energy"),
    MANUFACTURER_210("BW Technologies"),
    MANUFACTURER_211("HEINRICHS"),
    MANUFACTURER_212("SIC"),
    MANUFACTURER_213("HACH LANGE"),
    MANUFACTURER_214("Exalon Instruments"),
    MANUFACTURER_215("FAURE HERMAN"),
    MANUFACTURER_216("STI S.r.l."),
    MANUFACTURER_217("Manometr-Kharkiv"),
    MANUFACTURER_218("Dalian-Instruments"),
    MANUFACTURER_219("Spextrex"),
    MANUFACTURER_220("SIPAI Instruments"),
    MANUFACTURER_221("Advanced Flow"),
    MANUFACTURER_222("Rexa. Koso America"),
    MANUFACTURER_223("General Monitors, Inc."),
    MANUFACTURER_224("Manufacturer Expansion"),
    MANUFACTURER_249("HART Communication Foundation");

    private final String vendorName;

    /**
     * Constructor.
     * 
     * @param vendorName
     *            The name of the vendor.
     */
    private ManufacturerType(String vendorName) {
        this.vendorName = vendorName;
    }

    /**
     * Get the name of the vendor.
     * 
     * @return The vendor's name.
     */
    public String getManufacturerName() {
        return this.vendorName;
    }

    /**
     * Determine manufacturer type from the bit values.
     *
     * @param manufacturerID
     *            The manufacturer byte.
     * @return The {@link Manufacturer} instance found inside the manufacturer byte.
     * @throws ManufacturerException
     *             If the address type is not recognized in the incoming incoming manufacturer byte.
     */
    public static ManufacturerType fromID(byte manufacturerID) throws ManufacturerException {
        for (ManufacturerType type : ManufacturerType.values()) {
            byte testByte = type.getManufacturerID();

            if (testByte == manufacturerID) {
                return type;
            }
        }
        throw new ManufacturerException("An unrecognized manufacturer ID was found in manufacturerByte: "
                + manufacturerID);
    }

    /**
     * Return the manufacturer ID, as defined by the HART Communication Foundation.
     *
     * @return The manufacturer ID.
     */
    public byte getManufacturerID() {
        String numericPart = this.name().substring(this.name().indexOf('_') + 1);
        return Integer.valueOf(numericPart).byteValue();
    }

}
