package com.seeq.wirelesshart.hartipconnect.hartpdu;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * A set of utilities for manipulating byte arrays.
 */
public class HartUtilities {

    /**
     * Convert a float into a sequence of bytes.
     * 
     * @param floatValue
     *            The float value.
     * @return A List<Byte> instance.
     */
    public static byte[] floatToByteArray(float floatValue) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putFloat(floatValue);
        return buffer.array();
    }

    /**
     * Convert a list of bytes into a float.
     * 
     * @param bytesToConvert
     *            The byte List to convert into a float.
     * @return The corresponding float value.
     */
    public static Float floatFromBytes(byte[] bytesToConvert) {
        if (bytesToConvert == null) {
            throw new IllegalArgumentException("Expected 4 bytes, got " + null);
        }
        if (bytesToConvert.length != 4) {
            throw new IllegalArgumentException("Expected 4 bytes, got " + bytesToConvert.length);
        }
        return ByteBuffer.wrap(bytesToConvert).getFloat();
    }

    /**
     * Convert an integer into a sequence of bytes.
     * 
     * @param intValue
     *            The integer value.
     * @return A List<Byte> instance.
     */
    public static byte[] intToByteArray(int intValue) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(intValue);
        return buffer.array();
    }

    /**
     * Convert a list of bytes into an integer.
     * 
     * @param bytesToConvert
     *            The byte List to convert into an integer.
     * @return The corresponding integer value.
     */
    public static Integer intFromBytes(byte[] bytesToConvert) {
        if (bytesToConvert == null) {
            throw new IllegalArgumentException("Expected 4 bytes, got " + null);
        }
        if (bytesToConvert.length != 4) {
            throw new IllegalArgumentException("Expected 4 bytes, got " + bytesToConvert.length);
        }
        return ByteBuffer.wrap(bytesToConvert).getInt();
    }

    /**
     * Convert a short value to a byte array.
     * 
     * @param shortValue
     *            The short to convert into a byte array.
     * @return The corresponding output byte array.
     */
    public static byte[] shortToByteArray(short shortValue) {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
        buffer.putShort(shortValue);
        return buffer.array();
    }

    /**
     * The input byte array to convert into a short.
     * 
     * @param bytesToConvert
     *            The byte array to convert into a short. This must be 2 bytes long.
     * @return The corresponding Short value.
     */
    public static Short shortFromBytes(byte[] bytesToConvert) {
        if (bytesToConvert == null) {
            throw new IllegalArgumentException("Expected 2 bytes, got " + null);
        }
        if (bytesToConvert.length != Short.BYTES) {
            throw new IllegalArgumentException("Expected 2 bytes, got " + bytesToConvert.length);
        }
        return ByteBuffer.wrap(bytesToConvert).getShort();
    }

    /**
     * Convert a List<Byte> into a byte[] array. Notice that List<Byte>.toArray() does not unbox properly. Hence this
     * method works around this limitation.
     * 
     * @param byteList
     *            The List<Byte> instance.
     * @return The output byte[] array. Null if List<Byte> is null or empty.
     */
    public static byte[] byteListToByteArray(List<Byte> byteList) {
        if (byteList == null || byteList.isEmpty()) {
            return null;
        }

        byte[] outArray = new byte[byteList.size()];
        for (int index = 0; index < byteList.size(); index++) {
            outArray[index] = byteList.get(index);
        }

        return outArray;
    }

    /**
     * Convert a byte[] array into a List<Byte>.
     * 
     * @param byteArray
     *            The byte[] array.
     * @return The List<Byte> instance.
     */
    public static List<Byte> byteArrayToByteList(byte[] byteArray) {
        List<Byte> byteList = new ArrayList<>();
        if (byteArray != null) {
            for (byte newByte : byteArray) {
                byteList.add(newByte);
            }
        }
        return byteList;
    }
}
