package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;

/**
 * This class implements the default State interface, used internally by the state machine to encode, decode, and
 * validate interactions.
 */
public interface State {

    /**
     * Process a single incoming byte.
     *
     * @param newByte
     *            Byte to process.
     * @param context
     *            The context in which the byte should be applied.
     * @throws HartException
     *             If the decoding process fails.
     */
    void decodeByte(byte newByte, StateContextForDecoder context) throws HartException;

    /**
     * Convert an incoming {@link HartPdu} into bytes.
     *
     * @param hartPdu
     *            Incoming {@link HartPdu} instance.
     * @param context
     *            The {@link StateContext} instance.
     * @throws HartException
     *             If the encoding process fails.
     */
    void encodePacket(HartPdu hartPdu, StateContextForEncoder context) throws HartException;
}
