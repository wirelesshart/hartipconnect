package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;

/**
 * This class implements the state that encodes/decodes the check byte. The Check Byte is calculated as the XOR of all
 * the other bytes in the byte array.
 */
class CheckByteState implements State {

    CheckByteState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // Make sure the checkbyte is correct.
        byte checkByte = 0x00;

        for (Byte packetByte : context.getRawIncomingBytes()) {
            checkByte ^= packetByte.byteValue();
        }

        if (checkByte != 0) {
            throw new LongitudinalParityErrorException("The check byte does not match encoded data.");
        }

        context.getWorkspaceBytes().clear();

        context.setActiveState(null);
    }

    /**
     * Encode the {@link HartPdu} instance into a byte array. Note that at the end of this method, all the
     * {@link StateContext#workspaceBytes} array shall be copied into {@link StateContext#allBytes}.
     */
    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) throws HartException {
        byte checkByte = 0x00;

        for (Byte packetByte : context.getOutputBytes()) {
            checkByte ^= packetByte.byteValue();
        }

        context.getOutputBytes().add(checkByte);

        context.setActiveState(null);
    }

}
