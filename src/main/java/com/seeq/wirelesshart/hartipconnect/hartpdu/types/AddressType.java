package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;

/**
 * Supported address types from the HART Protocol.
 */
public enum AddressType {

    /**
     * The address is a polling address, defined by a single byte.
     */
    IS_POLLING_0_BYTE,

    /**
     * The address is a unique, five byte address.
     */
    IS_UNIQUE_5_BYTE;

    /**
     * Determine address type from the bit value.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return The {@link AddressType} instance found inside the delimeter byte.
     */
    public static AddressType fromDelimeterByte(byte delimeterByte) {
        return (((delimeterByte >> 7) & 0x01) == 0x01)
                ? AddressType.IS_UNIQUE_5_BYTE
                : AddressType.IS_POLLING_0_BYTE;
    }

    /**
     * Set the bit that controls the address type.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return A new delimeter byte that contains the new bit.
     */
    public byte appendToDelimeterByte(byte delimeterByte) {
        final byte outByte;
        if (this == AddressType.IS_UNIQUE_5_BYTE) {
            outByte = (byte) (delimeterByte | (0x01 << 7));
        } else {
            outByte = (byte) (delimeterByte & ~(0x01 << 7));
        }
        return outByte;
    }

    /**
     * Create a new instance of the implementing class.
     *
     * @return A new {@link DeviceAddress} instance.
     */
    public DeviceAddress createNewClassInstance() {
        if (this == AddressType.IS_UNIQUE_5_BYTE) {
            return new LongFrameAddress();
        } else {
            return new ShortFrameAddress();
        }
    }
}
