package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

/**
 * The Response packet for Command #074 -- Read I/O System Capabilities.
 * 
 * The following variables are expected in the response:
 * 
 * <table border="1" style="width:100%">
 * <tr>
 * <th>Number of Bytes</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Maximum Number of I/O Cards (must be ≥1)</td>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Maximum Number of Channels per I/O Card (must be ≥1)</td>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Maximum Number of Sub-Devices per Channel (must be ≥1)</td>
 * </tr>
 * <tr>
 * <td>2</td>
 * <td>Number of devices detected (the count includes the I/O system itself)</td>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Maximum number of delayed responses supported by the I/O system. (must be ≥ 2)</td>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Master mode for communication on channels. 1 = Primary Master (default)</td>
 * </tr>
 * <tr>
 * <td>1</td>
 * <td>Retry count to use when sending commands to a sub-device. (Range 2 ≤ value ≤ 5)</td>
 * </tr>
 * </table>
 */
public class Command074Response implements CommandData {

    protected static final int TOTAL_BYTE_COUNT = 8;
    private final CommandRegistry commandRegistry = CommandRegistry.COMMAND_074;
    private final FrameType frameType = FrameType.ACK;

    private byte maximumNumberOfIoCards;
    private byte maximumNumberOfChannelsPerIoCard;
    private byte maximumNumberOfSubDevicesPerChannel;
    private short numberOfDevicesDetectedInclIoSystem;
    private byte maximumNumberOfDelayedResponsesSupported;
    private boolean primaryMaster;
    private byte retryCountToUseWhenSendingCommandsToASubDevice;

    /**
     * Constructor. The internal state is loaded via the {@link #fromBytes(List)} method or other setters/getters.
     */
    public Command074Response() {}

    /**
     * Return the entire internal byte representation for this {@link CommandData}.
     * 
     * @return The internal byte representation.
     */
    @Override
    public List<Byte> toBytes() {
        List<Byte> encodedBytes = new ArrayList<Byte>();

        encodedBytes.add(this.maximumNumberOfIoCards);
        encodedBytes.add(this.maximumNumberOfChannelsPerIoCard);
        encodedBytes.add(this.maximumNumberOfSubDevicesPerChannel);
        encodedBytes.addAll(HartUtilities.byteArrayToByteList(HartUtilities
                .shortToByteArray(this.numberOfDevicesDetectedInclIoSystem)));
        encodedBytes.add(this.maximumNumberOfDelayedResponsesSupported);
        encodedBytes.add(this.primaryMaster ? (byte) 0x01 : (byte) 0x0);
        encodedBytes.add(this.retryCountToUseWhenSendingCommandsToASubDevice);

        return encodedBytes;
    }

    /**
     * Set the internal byte representation for this {@link CommandData}.
     * 
     * @param bytesToDecode
     *            The internal byte representation.
     * @throws EngineeringUnitsException
     *             If the incoming byte does not contain a valid engineering units code as defined by the HART
     *             Communications Foundation.
     */
    @Override
    public void fromBytes(List<Byte> bytesToDecode) throws EngineeringUnitsException {
        if (bytesToDecode.size() != TOTAL_BYTE_COUNT) {
            throw new IllegalArgumentException("Expected a length of " + TOTAL_BYTE_COUNT + ", but received "
                    + bytesToDecode.size());
        }

        this.maximumNumberOfIoCards = bytesToDecode.get(0);
        this.maximumNumberOfChannelsPerIoCard = bytesToDecode.get(1);
        this.maximumNumberOfSubDevicesPerChannel = bytesToDecode.get(2);
        this.numberOfDevicesDetectedInclIoSystem = HartUtilities.shortFromBytes(HartUtilities
                .byteListToByteArray(bytesToDecode.subList(3, 5)));
        this.maximumNumberOfDelayedResponsesSupported = bytesToDecode.get(5);
        this.primaryMaster = bytesToDecode.get(6) == 0x00 ? false : true;
        this.retryCountToUseWhenSendingCommandsToASubDevice = bytesToDecode.get(7);
    }

    @Override
    public CommandRegistry getCommandType() {
        return this.commandRegistry;
    }

    @Override
    public FrameType getFrameType() {
        return this.frameType;
    }

    @Override
    public int getTotalDataByteCount() {
        return TOTAL_BYTE_COUNT;
    }

    /**
     * Get the maximum number of IO cards supported by the device.
     * 
     * @return The maximum number of IO cards.
     */
    public byte getMaximumNumberOfIoCards() {
        return this.maximumNumberOfIoCards;
    }

    /**
     * Set the maximum number of cards supported by the IO device.
     * 
     * @param maximumNumberOfIoCards
     *            The maximum number of cards.
     */
    public void setMaximumNumberOfIoCards(byte maximumNumberOfIoCards) {
        this.maximumNumberOfIoCards = maximumNumberOfIoCards;
    }

    /**
     * Get the maximum number of channels supported per IO card.
     * 
     * @return The maximum number of channels.
     */
    public byte getMaximumNumberOfChannelsPerIoCard() {
        return this.maximumNumberOfChannelsPerIoCard;
    }

    /**
     * Set the maximum number of channels supported per IO card.
     * 
     * @param maximumNumberOfChannelsPerIoCard
     *            Maximum number of channels.
     */
    public void setMaximumNumberOfChannelsPerIoCard(byte maximumNumberOfChannelsPerIoCard) {
        this.maximumNumberOfChannelsPerIoCard = maximumNumberOfChannelsPerIoCard;
    }

    /**
     * Get the maximum number of sub devices supported per channel.
     * 
     * @return The maximum number of sub devices supported.
     */
    public byte getMaximumNumberOfSubDevicesPerChannel() {
        return this.maximumNumberOfSubDevicesPerChannel;
    }

    /**
     * Set the maximum number of sub devices supported per channel.
     * 
     * @param maximumNumberOfSubDevicesPerChannel
     *            The maximum number of subdevices.
     */
    public void setMaximumNumberOfSubDevicesPerChannel(byte maximumNumberOfSubDevicesPerChannel) {
        this.maximumNumberOfSubDevicesPerChannel = maximumNumberOfSubDevicesPerChannel;
    }

    /**
     * Get the number of devices detected, including the IO System itself. For example, when polling the WirelessHART
     * gateway (in this case the "IO System"), this number denotes the total number of WirelessHART devices detected,
     * including the WirelessHART gateway itself.
     * 
     * @return The number of devices detected, including the IO System itself.
     */
    public short getNumberOfDevicesDetectedInclIoSystem() {
        return this.numberOfDevicesDetectedInclIoSystem;
    }

    /**
     * Set the number of devices detected, including the IO System.
     * 
     * @param numberOfDevicesDetectedInclIoSystem
     *            The number of devices detected.
     */
    public void setNumberOfDevicesDetectedInclIoSystem(short numberOfDevicesDetectedInclIoSystem) {
        this.numberOfDevicesDetectedInclIoSystem = numberOfDevicesDetectedInclIoSystem;
    }

    /**
     * Get the maximum number of delayed responses that are supported by the IO system.
     * 
     * @return The maximum number of delayed responses.
     */
    public byte getMaximumNumberOfDelayedResponsesSupported() {
        return this.maximumNumberOfDelayedResponsesSupported;
    }

    /**
     * Set the maximum number of delayed responses supported by the IO System.
     * 
     * @param maximumNumberOfDelayedResponsesSupported
     *            The maximum number of responses.
     */
    public void setMaximumNumberOfDelayedResponsesSupported(byte maximumNumberOfDelayedResponsesSupported) {
        this.maximumNumberOfDelayedResponsesSupported = maximumNumberOfDelayedResponsesSupported;
    }

    /**
     * Determine the master mode of the IO System.
     * 
     * @return True if it is the primary master. False if it is the secondary master.
     */
    public boolean isPrimaryMaster() {
        return this.primaryMaster;
    }

    /**
     * Determine the master mode of the IO System.
     * 
     * @param primaryMaster
     *            True if the IO system is the primary master. False if it is the secondary master.
     */
    public void setPrimaryMaster(boolean primaryMaster) {
        this.primaryMaster = primaryMaster;
    }

    /**
     * Get the retry count used when sending commands to a sub-device.
     * 
     * @return The retry count.
     */
    public byte getRetryCountToUseWhenSendingCommandsToASubDevice() {
        return this.retryCountToUseWhenSendingCommandsToASubDevice;
    }

    /**
     * Set the retry count to use when sending commands to a sub-device.
     * 
     * @param retryCountToUseWhenSendingCommandsToASubDevice
     *            The retry count.
     */
    public void setRetryCountToUseWhenSendingCommandsToASubDevice(byte retryCountToUseWhenSendingCommandsToASubDevice) {
        this.retryCountToUseWhenSendingCommandsToASubDevice = retryCountToUseWhenSendingCommandsToASubDevice;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.maximumNumberOfChannelsPerIoCard;
        result = prime * result + this.maximumNumberOfDelayedResponsesSupported;
        result = prime * result + this.maximumNumberOfIoCards;
        result = prime * result + this.maximumNumberOfSubDevicesPerChannel;
        result = prime * result + this.numberOfDevicesDetectedInclIoSystem;
        result = prime * result + (this.primaryMaster ? 1231 : 1237);
        result = prime * result + this.retryCountToUseWhenSendingCommandsToASubDevice;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command074Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        Command074Response other = (Command074Response) obj;
        return this.equals(other);
    }

    @Override
    public boolean equals(CommandData obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!Command074Response.class.isAssignableFrom(obj.getClass())) {
            return false;
        }

        Command074Response other = (Command074Response) obj;
        if (this.maximumNumberOfChannelsPerIoCard != other.maximumNumberOfChannelsPerIoCard) {
            return false;
        }
        if (this.maximumNumberOfDelayedResponsesSupported != other.maximumNumberOfDelayedResponsesSupported) {
            return false;
        }
        if (this.maximumNumberOfIoCards != other.maximumNumberOfIoCards) {
            return false;
        }
        if (this.maximumNumberOfSubDevicesPerChannel != other.maximumNumberOfSubDevicesPerChannel) {
            return false;
        }
        if (this.numberOfDevicesDetectedInclIoSystem != other.numberOfDevicesDetectedInclIoSystem) {
            return false;
        }
        if (this.primaryMaster != other.primaryMaster) {
            return false;
        }
        if (this.retryCountToUseWhenSendingCommandsToASubDevice != other.retryCountToUseWhenSendingCommandsToASubDevice) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Command074Response ["
                + "commandRegistry=" + this.commandRegistry
                + ", frameType=" + this.frameType
                + ", maximumNumberOfIoCards=" + this.maximumNumberOfIoCards
                + ", maximumNumberOfChannelsPerIoCard=" + this.maximumNumberOfChannelsPerIoCard
                + ", maximumNumberOfSubDevicesPerChannel=" + this.maximumNumberOfSubDevicesPerChannel
                + ", numberOfDevicesDetectedInclIoSystem=" + this.numberOfDevicesDetectedInclIoSystem
                + ", maximumNumberOfDelayedResponsesSupported=" + this.maximumNumberOfDelayedResponsesSupported
                + ", primaryMaster=" + this.primaryMaster
                + ", retryCountToUseWhenSendingCommandsToASubDevice="
                + this.retryCountToUseWhenSendingCommandsToASubDevice
                + "]";
    }
}
