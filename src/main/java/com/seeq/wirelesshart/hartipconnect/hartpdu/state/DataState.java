package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.CommunicationErrorType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;

/**
 * This state implements the inspection of the response or communication error codes.
 * 
 * The structure of the data packet is determined by the {@link FrameType} and the {@link CommandRegistry}, and its size is
 * determined by the byte count from {@link ByteCountState}.
 * 
 * Specifically:
 * 
 * <ul>
 * 
 * <li>If the frame type is an STX frame, then the entire data packet contains exclusively any arguments needed by the
 * command.</li>
 * <li>If the frame type is an ACK or a BACK, then the data packet should minimally contain 2 bytes. In the first byte,
 * if the bit is set, then there is a Communication Error (See {@link CommunicationErrorType}) and the entire first byte
 * must be processed to determine the types of errors that are reported, otherwise it is a Response code (See
 * {@link ResponseCodeType}) byte and must be processed as such. The second byte contains information about the device
 * itself (See {@link DeviceStatusType}). The remainder of the bytes contain the result of the execution of the command,
 * if any.</li>
 * 
 * </ul>
 * 
 * 
 */
class DataState implements State {

    DataState() {}

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // If there is no data to decode, then don't process this byte! This is our check-byte! Send it to the Check
        // Byte State! Otherwise, process this byte locally.
        if (context.getDataByteCount() == 0) {
            context.setActiveState(new CheckByteState());
            context.getActiveState().decodeByte(newByte, context);
        } else {
            // Add our byte to our workspace.
            context.getWorkspaceBytes().add(newByte);

            // If our data packet is complete (the number of bytes in the workspace is the same as the number of bytes
            // we expected) then process the data packet.
            if (context.getWorkspaceBytes().size() == context.getDataByteCount()) {

                if (context.getOutputHartPdu().getFrameType() != FrameType.STX) {
                    this.processErrorOrResponseByte(context);
                    this.processDeviceStatusByte(context);
                }

                this.receiveData(context);

                // switch state.
                context.getWorkspaceBytes().clear();
                context.setActiveState(new CheckByteState());
            }

        }
    }

    /**
     * Receive the data content.
     * 
     * @param context
     *            The context for the decoding state machine.
     * @throws HartException
     */
    private void receiveData(StateContextForDecoder context) throws HartException {
        // determine the byte array that is still left to decode.
        List<Byte> bytesToDecode = context.getOutputHartPdu().getFrameType() == FrameType.STX
                ? context.getWorkspaceBytes().subList(0, context.getWorkspaceBytes().size())
                : context.getWorkspaceBytes().subList(2, context.getWorkspaceBytes().size());

        // Decode the byte array, but only if no communication errors were found.
        if (context.getOutputHartPdu().getCommunicationErrors() != null) {
            context.getOutputHartPdu().setCommandData(null);
        } else {
            CommandData packet = context.getOutputHartPdu().getCommandData();
            packet.fromBytes(bytesToDecode);
            context.getOutputHartPdu().setCommandData(packet);
        }
    }

    /**
     * Process the first byte.
     * 
     * First, check the first bit in the data packet. If it indicates an error condition, then the byte contains a
     * communication error.
     * 
     * Then, parse the byte as either a communication byte or as a response byte, depending on what we learned from our
     * step above.
     * 
     * Finally, set the pertinent information in the output HartPdu.
     * 
     * @param context
     *            The context of the decoding state machine.
     * @throws HartSystemException
     *             If the supplied byte is not a valid error or response byte.
     */
    private void processErrorOrResponseByte(StateContextForDecoder context) throws HartSystemException {
        byte firstByte = context.getWorkspaceBytes().get(0);

        // check for an error condition:
        if (CommunicationErrorType.isErrorInErrorByte(firstByte)) {
            // found a communication error condition. In such an instance, the response is null since we never managed
            // to hit the remote device -- hence nothing was processed.
            CommunicationErrorType[] commErrors = CommunicationErrorType.fromCommunicationErrorByte(firstByte);
            context.getOutputHartPdu().setCommunicationErrors(commErrors);
            context.getOutputHartPdu().setResponseCodeType(null);
        } else {
            // did not find an communication error condition. This must be a Response. Responses don't have any
            // communication errors.
            context.getOutputHartPdu().setResponseCodeType(ResponseCodeType.fromResponseByte(firstByte));
            context.getOutputHartPdu().setCommunicationErrors(null);
        }
    }

    /**
     * Receive the device status information. This is obtained from the second byte.
     * 
     * @param context
     *            The context of the decoding state machine.
     */
    private void processDeviceStatusByte(StateContextForDecoder context) {
        byte secondByte = context.getWorkspaceBytes().get(1);
        context.getOutputHartPdu().setDeviceStatus(DeviceStatusType.fromDeviceStatusByte(secondByte));
    }

    @Override
    public void encodePacket(HartPdu hartPdu, StateContextForEncoder context) throws HartException {
        final List<Byte> dataBytes;
        if (hartPdu.getFrameType() == FrameType.STX) {
            CommandData packet = hartPdu.getCommandData();
            dataBytes = packet.toBytes();
        } else {

            if (hartPdu.getCommunicationErrors() != null && hartPdu.getCommunicationErrors().length > 0) {
                dataBytes = new ArrayList<>(context.getDataByteCount());

                byte errorByte = CommunicationErrorType.toCommunicationErrorByte(hartPdu.getCommunicationErrors());
                dataBytes.add(errorByte);

                byte fieldDeviceStatusByte = DeviceStatusType.toDeviceStatusByte(hartPdu.getDeviceStatus());
                dataBytes.add(fieldDeviceStatusByte);
            } else {
                dataBytes = new ArrayList<>(context.getDataByteCount());

                byte responseCodeByte = hartPdu.getResponseCodeType().toResponseCodeByte();
                dataBytes.add(responseCodeByte);

                byte fieldDeviceStatusByte = DeviceStatusType.toDeviceStatusByte(hartPdu.getDeviceStatus());
                dataBytes.add(fieldDeviceStatusByte);

                CommandData strategy = hartPdu.getCommandData();
                dataBytes.addAll(strategy.toBytes());
            }

        }

        if (dataBytes != null) {
            context.getOutputBytes().addAll(dataBytes);
        }

        context.setActiveState(new CheckByteState());
    }

}
