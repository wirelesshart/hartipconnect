package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.PhysicalLayerException;

/**
 * Supported Physical layer types from the protocol.
 */
public enum PhysicalLayerType {

    /**
     * The physical layer is asynchronous, (e.g. FSK, RS-485).
     */
    IS_ASYNCHRONOUS(0),

    /**
     * The physical layer is synchronous, (e.g. PSK).
     */
    IS_SYNCHRONOUS(1);

    private final int value;

    private PhysicalLayerType(int value) {
        this.value = value;
    }

    /**
     * Determine address type from the bit value.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return The {@link AddressType} instance found inside the delimeter byte.
     * @throws PhysicalLayerException
     *             If the address type is not recognized in the incoming delimeter byte.
     */
    public static PhysicalLayerType fromDelimeterByte(byte delimeterByte) throws PhysicalLayerException {
        for (PhysicalLayerType type : PhysicalLayerType.values()) {
            if (((delimeterByte & 0x18) >> 3) == type.getValue()) {
                return type;
            }
        }
        throw new PhysicalLayerException(String.format(
                "An unrecognized physical layer type was found in delimeter byte: 0x%x", delimeterByte));
    }

    /**
     * Return the value representation of this {@link PhysicalLayerType} instance.
     *
     * @return The {@link PhysicalLayerType} value representation in the Delimeter byte.
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Set the appropriate bits in the delimeter byte.
     *
     * @param delimeterByte
     *            The delimeter byte.
     * @return The new delimeterByte.
     */
    public byte appendToDelimeterByte(byte delimeterByte) {
        byte clearedByte = (byte) (delimeterByte & ~(0x03 << 3));
        return (byte) (clearedByte | ((this.value & 0x03) << 3));
    }

}
