package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;

/**
 * Manipulate the API protocolVersion byte.
 * <p>
 * As of the time of this writing, it is expected that the protocolVersion is identically equal to 1, and nothing more.
 */
class VersionState implements State {

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {

        // This library only supports protocol version 1.
        if (newByte != (byte) 0x01) {
            throw new HartSystemException(
                    "The protocol version is not supported by this library. Expected 1, Received " + newByte);
        }

        context.getOutputHartIp().setProtocolVersion(newByte);

        context.setActiveState(new MessageTypeState());
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {
        byte outByte = hartIp.getProtocolVersion();

        context.getOutputBytes().add(outByte);

        context.setActiveState(new MessageTypeState());
    }
}
