package com.seeq.wirelesshart.hartipconnect.hartip.state;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to maintain the state of the processing engine, for encoding operations.
 */
public class StateContextForEncoder extends StateContext {
    private final List<Byte> outputBytes = new ArrayList<>();

    /**
     * Get the total output bytes accumulated across the build process.
     * 
     * @return The encoded output bytes.
     */
    public List<Byte> getOutputBytes() {
        return this.outputBytes;
    }

}
