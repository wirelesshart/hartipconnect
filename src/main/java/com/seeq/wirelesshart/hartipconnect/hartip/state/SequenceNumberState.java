package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;

/**
 * Class used to identify the Command Sequence Number. Every HART-IP packet sent and received is identified by a unique
 * counter. This counter is used to uniquely match the request with the matching response.
 */
class SequenceNumberState implements State {

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // Collect incoming bytes. Don't process them.
        context.getWorkspaceBytes().add(newByte);

        // Now that we have a sufficient number of bytes, process them.
        if (context.getWorkspaceBytes().size() == Short.BYTES) {
            byte[] byteArray = HartUtilities.byteListToByteArray(context.getWorkspaceBytes());
            short sequenceNumber = HartUtilities.shortFromBytes(byteArray);

            context.getOutputHartIp().setSequenceNumber(sequenceNumber);

            context.getWorkspaceBytes().clear();

            context.setActiveState(new ByteCountState());
        }
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {

        byte[] byteArray = HartUtilities.shortToByteArray(hartIp.getSequenceNumber());

        context.getOutputBytes().add(byteArray[0]);
        context.getOutputBytes().add(byteArray[1]);

        context.setActiveState(new ByteCountState());

    }
}
