package com.seeq.wirelesshart.hartipconnect.hartip.messagebody;

/**
 * This interface describes the main body content of a HartIp packet. This is used to model the payload of a HART-IP
 * message.
 */
public interface MessageBody {

}
