package com.seeq.wirelesshart.hartipconnect.hartip.state;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;

/**
 * This class is used to maintain the state of the processing engine, for decoding operations.
 */
public class StateContextForDecoder extends StateContext {
    private final List<Byte> rawIncomingBytes = new ArrayList<>();
    private final List<Byte> workspaceBytes = new ArrayList<>();
    private final HartIp outputHartIp = new HartIp();
    private short byteCount;

    /**
     * Get the byte count. This byte count includes all bytes, including the header.
     * 
     * @return The byte count.
     */
    public short getByteCount() {
        return this.byteCount;
    }

    /**
     * Set the total byte count. This includes all bytes, including the header.
     * 
     * @param byteCount
     *            Total byte count.
     */
    public void setByteCount(short byteCount) {
        this.byteCount = byteCount;
    }

    /**
     * Get the total number of bytes received. This should be initialized and added to by the decoder itself.
     * 
     * @return The raw set of incoming bytes.
     */
    public List<Byte> getRawIncomingBytes() {
        return this.rawIncomingBytes;
    }

    /**
     * The output HartIP that is being decoded from bytes.
     * 
     * @return The {@link HartIp} instance.
     */
    public HartIp getOutputHartIp() {
        return this.outputHartIp;
    }

    /**
     * Get the workspace bytes. This is a flex space that can be added to by each of the states. Once the state finishes
     * using it, it should be cleared for use by the next state.
     * 
     * @return The List<Byte> of workspace bytes.
     */
    public List<Byte> getWorkspaceBytes() {
        return this.workspaceBytes;
    }

}
