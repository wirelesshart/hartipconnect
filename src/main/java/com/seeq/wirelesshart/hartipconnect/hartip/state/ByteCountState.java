package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.SessionInitialization;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduEncoder;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;

/**
 * Encode and decode the total number of bytes expected in the packet.
 */
class ByteCountState implements State {

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // Collect incoming bytes, don't process them.
        context.getWorkspaceBytes().add(newByte);

        // Once we have enough bytes, process them.
        if (context.getWorkspaceBytes().size() == Short.BYTES) {
            byte[] byteArray = HartUtilities.byteListToByteArray(context.getWorkspaceBytes());
            short totalBytes = HartUtilities.shortFromBytes(byteArray);

            context.setByteCount(totalBytes);

            context.getWorkspaceBytes().clear();

            if (totalBytes == MessageBodyState.NUMBER_OF_BYTES_IN_HEADER) {
                // nothing more to process. Signal completion.
                context.setActiveState(null);
            } else {
                context.setActiveState(new MessageBodyState());
            }
        }
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {
        short totalNumberOfBytes = MessageBodyState.NUMBER_OF_BYTES_IN_HEADER;

        final short additionalBytes;
        switch (hartIp.getMessageId()) {
        case TOKEN_PASSING_PDU:
            HartPdu hartPdu = (HartPdu) hartIp.getMessageBody();
            byte[] hartPduByteArray = HartPduEncoder.encode(hartPdu);
            additionalBytes = (short) hartPduByteArray.length;
            break;
        case KEEP_ALIVE:
            additionalBytes = 0;
            break;
        case SESSION_CLOSE:
            additionalBytes = 0;
            break;
        case SESSION_INIT:
            SessionInitialization sessionInitialization = (SessionInitialization) hartIp.getMessageBody();
            byte[] byteArray = sessionInitialization.toBytes();
            additionalBytes = (short) byteArray.length;
            break;
        case DISCOVERY:
        case WIRELESS_DIRECT_PDU:
            // No actual documentation is found on the structure of these.
            // TODO: efhilton 2015-04-23: Discuss with Mark N., Eric R.? Structure?
        default:
            throw new UnsupportedOperationException("The supplied Message ID is not yet supported: "
                    + hartIp.getMessageId().name());
        }

        totalNumberOfBytes += additionalBytes;

        byte[] byteArray = HartUtilities.shortToByteArray(totalNumberOfBytes);

        context.getOutputBytes().add(byteArray[0]);
        context.getOutputBytes().add(byteArray[1]);

        if (additionalBytes == 0) {
            // Nothing more to process. Signal completion.
            context.setActiveState(null);
        } else {
            context.setActiveState(new MessageBodyState());
        }
    }
}
