package com.seeq.wirelesshart.hartipconnect.hartip;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.state.StateContextForEncoder;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;

/**
 * This class encodes a {@link HartIp} into a series of bytes.
 */
public class HartIpEncoder {
    /**
     * Do not instantiate this class.
     */
    private HartIpEncoder() {};

    /**
     * Encode the incoming HartIp into a series of bytes.
     *
     * @param packetToEncode
     *            The {@link HartIp} instance that we want to encode.
     * @return An array of bytes.
     * @throws HartException
     *             If any number of errors are encountered during the encoding process.
     */
    public static byte[] encode(HartIp packetToEncode) throws HartException {
        packetToEncode.validate();

        StateContextForEncoder context = new StateContextForEncoder();

        while (context.getActiveState() != null) {
            context.getActiveState().encodePacket(packetToEncode, context);
        }

        return HartUtilities.byteListToByteArray(context.getOutputBytes());
    }
}
