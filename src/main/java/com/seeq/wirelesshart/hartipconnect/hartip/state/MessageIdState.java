package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageIdType;

/**
 * This State is used to manipulate the Message ID. The Message ID maps into the {@link MessageIdType}, and is used to
 * specify whether the message is used to initialize a session with the gateway, shut down a session, keep the
 * connection alive, or simply transmit HART-PDU packets.
 */
class MessageIdState implements State {

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {

        // The entire byte contains the Message ID type.
        context.getOutputHartIp().setMessageId(MessageIdType.fromByte(newByte));

        context.setActiveState(new StatusCodeState());

    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {

        // Set the Message ID byte.
        context.getOutputBytes().add(hartIp.getMessageId().getValue());

        context.setActiveState(new StatusCodeState());
    }

}
