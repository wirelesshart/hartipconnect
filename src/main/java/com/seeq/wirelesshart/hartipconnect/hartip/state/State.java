package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;

/**
 * This class implements the default State interface, used internally by the state machine to encode, decode, and
 * validate interactions.
 */
public interface State {

    /**
     * Process a single byte during the decoding process. It is the intent that the {@link State} class will be
     * encapsulated inside a for loop, where a new byte is fed to this method during each iteration.
     *
     * @param newByte
     *            Byte to process.
     * @param context
     *            The context in which the byte should be applied.
     * @throws HartException
     *             If the decoding process fails.
     */
    void decodeByte(byte newByte, StateContextForDecoder context) throws HartException;

    /**
     * Convert a {@link HartIp} into bytes.
     *
     * @param hartIp
     *            The {@link HartIp} instance.
     * @param context
     *            The resultant {@link StateContext} instance.
     * @throws HartException
     *             If the encoding process fails.
     */
    void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException;
}
