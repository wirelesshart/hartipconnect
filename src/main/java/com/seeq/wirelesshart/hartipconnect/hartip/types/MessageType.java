package com.seeq.wirelesshart.hartipconnect.hartip.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;

/**
 * This is an enumeration of the recognized message types as defined in the HART-IP protocol. Clients generally
 * initialize the message type to {@link #REQUEST}, and the Server initializes the message type to {@link #RESPONSE},
 * {@link #PUBLISH_NOTIFICATION}, or {@link NAK}.
 */
public enum MessageType {
    /**
     * The message type is a client initialized request.
     */
    REQUEST((byte) 0x00),

    /**
     * The message type is a response to a client initialized request.
     */
    RESPONSE((byte) 0x01),

    /**
     * The message type is a notification published after a subscription request.
     */
    PUBLISH_NOTIFICATION((byte) 0x02),

    /**
     * The message type denotes that the server was unable to process an incoming request.
     */
    NAK((byte) 0x0f);

    private final byte value;

    /**
     * Constructor.
     * 
     * @param value
     *            The bit value as defined by the HART Communications Foundation.
     */
    private MessageType(byte value) {
        this.value = (byte) (value & 0x0f);
    }

    /**
     * Get the byte value as stored in the HART-IP header.
     * 
     * @return The byte value.
     */
    public byte getValue() {
        return this.value;
    }

    /**
     * Given a value, return the corresponding {@link MessageType} instance.
     * 
     * @param testByte
     *            The input value of the {@link MessageType} that we care about. The value of interest should be
     *            encapsulated in the lower 4 bits.
     * @return The {@link MessageType} instance.
     * @throws HartSystemException
     *             If the input value does not have a corresponding {@link MessageType} instance.
     */
    public static MessageType fromByte(byte testByte) throws HartSystemException {
        byte testValue = (byte) (testByte & (0x0f));
        for (MessageType messageType : MessageType.values()) {
            if (testValue == messageType.getValue()) {
                return messageType;
            }
        }
        throw new HartSystemException(String.format("MessageType not found for value 0x%x", testByte));
    }
}
