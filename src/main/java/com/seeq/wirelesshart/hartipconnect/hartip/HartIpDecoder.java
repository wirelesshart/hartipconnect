package com.seeq.wirelesshart.hartipconnect.hartip;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.hartip.state.StateContextForDecoder;

/**
 * This class decodes a {@link HartIp} packet from a given set of bytes.
 */
public class HartIpDecoder {
    private StateContextForDecoder context = null;

    /**
     * Constructor.
     */
    public HartIpDecoder() {
        this.initialize();
    }

    /**
     * Given a byte array, decode these bytes into a {@link HartIp} packet.
     * 
     * @param bytesToDecode
     *            The byte array to decode.
     * @return The output {@link HartIp} instance, decoded from the incoming byte array.
     * @throws HartException
     *             If any number of errors are encountered during the decoding process.
     */
    public static HartIp decode(byte[] bytesToDecode) throws HartException {
        HartIpDecoder decoder = new HartIpDecoder();

        decoder.initialize();
        for (byte newByte : bytesToDecode) {
            decoder.decodeNextByte(newByte);

            if (decoder.processingCompleted()) {
                break;
            }
        }

        decoder.validateSuccessfulCompletion();

        return decoder.getResult();
    }

    /**
     * Initialize the decoding engine.
     */
    public void initialize() {
        this.context = null;
    }

    /**
     * Return the decoder context. If none is present, this will instantiate one.
     * 
     * @return The decoder's context.
     */
    public StateContextForDecoder getContext() {
        if (this.context == null) {
            this.context = new StateContextForDecoder();
        }
        return this.context;
    }

    /**
     * Decode the next byte, based on the present context of the decoding engine.
     * 
     * @param newByte
     *            The byte to process.
     * @throws HartException
     *             If the decoding engine encounters any errors during processing.
     */
    public void decodeNextByte(byte newByte) throws HartException {
        StateContextForDecoder decodingContext = this.getContext();
        decodingContext.getRawIncomingBytes().add(newByte);
        decodingContext.getActiveState().decodeByte(newByte, decodingContext);
    }

    /**
     * Validate that the processing completed as expected.
     * 
     * @throws LongitudinalParityErrorException
     *             If the number of bytes received was not equal to the number of bytes that were expected.
     * @throws IllegalStateException
     *             If any additional errors were encountered in the output HART-IP packet.
     */
    public void validateSuccessfulCompletion() throws LongitudinalParityErrorException, IllegalStateException {
        if (!this.processingCompleted()) {
            throw new LongitudinalParityErrorException("Number of bytes less than expected. Last state executed: "
                    + this.getContext().getActiveState().getClass());
        }

        this.getContext().getOutputHartIp().validate();
    }

    /**
     * Get the resulting HART-IP packet.
     * 
     * @return The decoded HART-IP packet.
     */
    public HartIp getResult() {
        return this.getContext().getOutputHartIp();
    }

    /**
     * Determine if the decoding process has been completed.
     * 
     * @return True if the process is completed. False otherwise.
     */
    public boolean processingCompleted() {
        return this.getContext().getActiveState() == null;
    }
}
