package com.seeq.wirelesshart.hartipconnect.hartip.state;

/**
 * This class is used to maintain the state of the processing engine.
 */
class StateContext {

    private State activeState;

    /**
     * Constructor.
     */
    public StateContext() {
        this.activeState = new VersionState();
    }

    /**
     * Get the active state.
     *
     * @return The presently running state.
     */
    public State getActiveState() {
        return this.activeState;
    }

    /**
     * Set the active state.
     *
     * @param activeState
     *            The active state.
     */
    public void setActiveState(State activeState) {
        this.activeState = activeState;
    }

}
