package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageType;

/**
 * The message type is found on the second byte. The message type is encoded in the lower 4 bits. The upper four bits
 * should be initialized to 0, and will be echoed back as zero also by the server.
 */
class MessageTypeState implements State {

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // Extract the MessageType from the lower 4 bits.
        context.getOutputHartIp().setMessageType(MessageType.fromByte(newByte));

        context.setActiveState(new MessageIdState());
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {
        // Set the lower 4 bits to contain our message type. The upper 4 bits should be identically zero.
        context.getOutputBytes().add(hartIp.getMessageType().getValue());

        context.setActiveState(new MessageIdState());

    }

}
