package com.seeq.wirelesshart.hartipconnect.hartip.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;

/**
 * Message identifier, as defined by the HART Communications Foundation (HCF). These are generally initialized by the
 * client, and echoed by the server.
 */
public enum MessageIdType {
    /**
     * Session initialization. This message is used to initiate a session between a Host application/client and a field
     * device or gateway server. This message must be exchanged with the server before other messages will be accepted
     * from the client. If requests are sent before a session has been initiated, the server will not respond even if
     * the message is valid in every other way.
     */
    SESSION_INIT((byte) 0x00),

    /**
     * This message is used by the HART Host application/client to request the server close a session.
     */
    SESSION_CLOSE((byte) 0x01),

    /**
     * The message is a keep alive message. This client must transmit this message periodically when no other
     * communications with the server are occurring to keep the session active/open. This message should be sent when
     * communication has been inactive for 95% the "Inactivity Close Time".
     */
    KEEP_ALIVE((byte) 0x02),

    /**
     * The message is a token passing PDU. This message embeds a full Token-Passing Data Link Layer formatted message as
     * the payload. This provides the server addressing information for routing the message to sub-devices of a
     * Multiplexer or Gateway device so Host applications/clients don’t need to be re-written. The format of the
     * response must take the same format as the request.
     */
    TOKEN_PASSING_PDU((byte) 0x03),

    /**
     * A HART Wireless/Direct PDU. As of the time of this writing, this appears in one document as a constant, but
     * there's no actual information about how it works, what it's for, or the support mechanism behind it. Hence, the
     * constant shall be defined here, but will not be supported in code until more information is found.
     */
    WIRELESS_DIRECT_PDU((byte) 0x04),

    /**
     * Support for the HART-IP Discovery Protocol is optional for clients and servers. The Discovery Protocol consists
     * of a HART-IP Client UDP broadcast followed by UDP unicast response from HART-IP servers. The Discovery UDP
     * packets consist of the standard HART-IP header with the Message ID set to 128 (Discovery).
     */
    DISCOVERY((byte) 0x80);

    private final byte value;

    /**
     * Constructor.
     * 
     * @param value
     *            The byte ID value as defined by the HART Communications Foundation (HCF).
     */
    private MessageIdType(byte value) {
        this.value = value;
    }

    /**
     * Get the message ID value, as defined by the HART Communications Foundation (HCF).
     * 
     * @return The message ID.
     */
    public byte getValue() {
        return this.value;
    }

    /**
     * Determine the {@link MessageIdType} instance from the supplied value.
     * 
     * @param testValue
     *            A supported message ID as defined by the HART Communications Foundation (HCF).
     * @return The corresponding {@link MessageIdType} instance.
     * @throws HartSystemException
     *             If the supplied value does not correspond to a recognized {@link MessageIdType}.
     */
    public static MessageIdType fromByte(byte testValue) throws HartSystemException {
        for (MessageIdType messageIdType : MessageIdType.values()) {
            if (testValue == messageIdType.getValue()) {
                return messageIdType;
            }
        }
        throw new HartSystemException(String.format("The MessageID of 0x%x is not supported.", testValue));
    }

}
