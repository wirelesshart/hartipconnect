package com.seeq.wirelesshart.hartipconnect.hartip;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * A constructor that keeps track of the Wireless HART Gateway's port and hostname information.
 */
public class HartGateway {
    private String host;
    private int port = 5094;
    private String description = "WirelessHART Gateway";

    /**
     * Constructor.
     * 
     * @param host
     *            The Gateway's host name or ip address.
     * @param port
     *            The port number.
     * @param description
     *            A human readable description for this WirelessHART Gateway or null if none is needed. For example:
     *            "Gateway located at Facility B, overlooking Distilling Process".
     * @throws URISyntaxException
     *             If either the hostname or port number do not have a valid syntax.
     */
    public HartGateway(String host, int port, String description) throws URISyntaxException {
        this.host = host;
        this.port = port;
        this.description = description;
        new URI(null, null, host, port, null, null, null);
    }

    /**
     * Constructor.
     * 
     * @param host
     *            The Gateway's Host name or IP address.
     * @param port
     *            The port number, for example, 5094.
     * @throws URISyntaxException
     *             If either the hostname or port number do not have a valid syntax.
     */
    public HartGateway(String host, int port) throws URISyntaxException {
        this(host, port, null);
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return this.port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
