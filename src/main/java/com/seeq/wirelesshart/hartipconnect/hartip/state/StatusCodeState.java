package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartip.types.StatusCodeType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;

/**
 * Obtain the status code for the interaction with the server. This status code is an instance of the
 * {@link StatusCodeType}, used to identify the status of the response to a given request. This is different from the
 * {@link DeviceStatusType} codes in that {@link StatusCodeType} is exclusive to TCP/UDP packets and TCP/UDP based
 * interactions, whereas the {@link DeviceStatusType} is used by each HART device to document internal states of the
 * device itself.
 */
class StatusCodeState implements State {
    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {
        // The entire byte contains the overall communication status.
        context.getOutputHartIp().setStatusCode(StatusCodeType.fromByte(newByte));

        context.setActiveState(new SequenceNumberState());
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {
        // Set the overall communication status.
        context.getOutputBytes().add(hartIp.getStatusCode().getValue());

        context.setActiveState(new SequenceNumberState());
    }
}
