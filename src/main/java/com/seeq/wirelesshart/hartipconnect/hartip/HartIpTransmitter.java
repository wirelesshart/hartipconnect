package com.seeq.wirelesshart.hartipconnect.hartip;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;

/**
 * This class is used to communicate with a remote gateway and transmit/receive {@link HartIp} packets. Note that this
 * class implements the {@link AutoCloseable} class.
 */
public class HartIpTransmitter implements AutoCloseable {
    private static final int SO_TIMEOUT_IN_MS = 20_000;
    private final Socket client;
    private final DataOutputStream out;
    private final DataInputStream in;

    /**
     * Open an unsecured communication port to the remote Gateway.
     * 
     * @param gateway
     *            The reference to the remote Gateway.
     * @throws UnknownHostException
     *             If the supplied host is not found.
     * @throws IOException
     *             If any of the IO Streams fail during the opening process.
     */
    public HartIpTransmitter(HartGateway gateway) throws UnknownHostException, IOException {
        this.client = new Socket(gateway.getHost(), gateway.getPort());

        this.client.setSoTimeout(SO_TIMEOUT_IN_MS);

        this.out = new DataOutputStream(this.client.getOutputStream());
        this.in = new DataInputStream(this.client.getInputStream());
    }

    /**
     * Submit a packet to the remote gateway.
     * 
     * @param requestPacket
     *            The request packet to transmit to the remote Gateway.
     * @return The Gateway's response.
     * @throws UnknownHostException
     *             If the remote host address is not available.
     * @throws IOException
     *             If any of the IO Streams return an error.
     * @throws HartException
     *             If problems are encountering during the encoding/decoding of the bytes sent/received from the
     *             Gateway.
     */
    public HartIp transmit(HartIp requestPacket) throws UnknownHostException, IOException, HartException {
        byte[] outArray = HartIpEncoder.encode(requestPacket);

        /* Write out a request to the server */
        for (byte b : outArray) {
            this.out.writeByte(b);
        }
        this.out.flush();

        /* Listen to the server's response and process it. */
        HartIpDecoder decoder = new HartIpDecoder();
        decoder.initialize();

        while (!decoder.processingCompleted()) {
            byte byteToProcess = this.in.readByte();
            decoder.decodeNextByte(byteToProcess);
        }

        decoder.validateSuccessfulCompletion();

        return decoder.getResult();
    }

    @Override
    public void close() throws IOException {
        if (this.out != null) {
            this.out.close();
        }
        if (this.in != null) {
            this.in.close();
        }
        if (this.client != null) {
            this.client.close();
        }
    }
}
