package com.seeq.wirelesshart.hartipconnect.hartip.messagebody;

import java.util.ArrayList;
import java.util.List;

import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;

/**
 * Packet sent to the server to initiate a session. Without this, the server will not accept any requests from the
 * client.
 */
public class SessionInitialization implements MessageBody {
    /**
     * Is the client a primary master?
     */
    private boolean primaryMaster = true;

    /**
     * Milliseconds of inactivity before a session is closed.
     */
    private int inactivityCloseTime = 60_000;

    /**
     * Return true if this is the primary master.
     * 
     * @return True if this is the primary master.
     */
    public boolean isPrimaryMaster() {
        return this.primaryMaster;
    }

    /**
     * Set to true if this is a primary master.
     * 
     * @param primaryMaster
     *            True if this is the primary master.
     */
    public void setPrimaryMaster(boolean primaryMaster) {
        this.primaryMaster = primaryMaster;
    }

    /**
     * Get the time, in milliseconds, before an inactive session is closed.
     * 
     * @return Time in milliseconds.
     */
    public int getInactivityCloseTime() {
        return this.inactivityCloseTime;
    }

    /**
     * Set the time, in milliseconds, before an inactive session is closed.
     * 
     * @param inactivityCloseTime
     *            Time, in milliseconds.
     */
    public void setInactivityCloseTime(int inactivityCloseTime) {
        this.inactivityCloseTime = inactivityCloseTime;
    }

    /**
     * Generate a new {@link SessionInitialization} instance from the given byte array. The byte array first contains
     * the primary master bit (0x01) in the first byte. Then, the next four bytes contain the inactivity timeout.
     * 
     * @param byteArray
     *            The byte array to decode into a {@link SessionInitialization} instance.
     * @return The newly decoded {@link SessionInitialization} instance.
     */
    public static SessionInitialization fromBytes(byte[] byteArray) {
        SessionInitialization instance = new SessionInitialization();

        instance.setPrimaryMaster((byteArray[0] & 0x01) == 0x01 ? true : false);

        byte[] intBytes = new byte[4];
        for (int index = 0; index < 4; index++) {
            intBytes[index] = byteArray[index + 1];
        }

        int inactivityCloseTime = HartUtilities.intFromBytes(intBytes);
        instance.setInactivityCloseTime(inactivityCloseTime);

        return instance;
    }

    /**
     * Generate a byte array from the internal state of this instance. The byte array first contains the primary master
     * bit (0x01) in the first byte. Then, the next four bytes contain the inactivity timeout.
     * 
     * @return The corresponding byte array.
     */
    public byte[] toBytes() {
        List<Byte> outList = new ArrayList<>();

        outList.add((byte) (this.primaryMaster ? 0x01 : 0x00));

        byte[] intBytes = HartUtilities.intToByteArray(this.getInactivityCloseTime());
        outList.addAll(HartUtilities.byteArrayToByteList(intBytes));

        return HartUtilities.byteListToByteArray(outList);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.inactivityCloseTime;
        result = prime * result + (this.primaryMaster ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        SessionInitialization other = (SessionInitialization) obj;
        if (this.inactivityCloseTime != other.inactivityCloseTime) {
            return false;
        }
        if (this.primaryMaster != other.primaryMaster) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SessionInitialization ["
                + "primaryMaster=" + this.primaryMaster
                + ", inactivityCloseTime=" + this.inactivityCloseTime
                + "]";
    }
}
