package com.seeq.wirelesshart.hartipconnect.hartip.types;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;

/**
 * Overall communication status for the message as defined by the HART Communication Foundation (HCF). Status values are
 * generally initialized to 0 by the Client, and then initialized with the actual value by the server.
 */
public enum StatusCodeType {
    SUCCESS((byte) 0, "No Error Occurred"),
    INVALID_SELECTION((byte) 2, "Invalid selection (Invalid Master Type)"),
    TOO_FEW_DATABYTES_RECEIVED((byte) 5, "Too few databytes received"),
    DEVICE_SPECIFIC_COMMAND_ERROR((byte) 6, "Device specific command error"),
    SET_TO_NEAREST_POSSIBLE_VALUE((byte) 8, "Set to Nearest Possible Value (Inactivity timer value)"),
    VERSION_NOT_SUPPORTED((byte) 14, "Version not supported"),
    ALL_AVAILABLE_SESSIONS_IN_USE_UNSUPPORTED_MESSAGE_ID((byte) 15,
            "All available sessions in use (Unsupported message ID)"),
    ACCESS_RESTRICTED_SESSION_ALREADY_ESTABLISHED((byte) 16,
            "Access restricted, Session already established (Server resources exhausted)");

    // TODO: efhilton 2015-04-23: Discuss with Mark N, Eric R: Status codes 15 and 16 seem suspiciously similar?

    private final byte value;
    private final String humanString;

    /**
     * Constructor.
     * 
     * @param value
     *            Value, as defined by the HCF.
     * @param humanString
     *            A human readable string.
     */
    private StatusCodeType(byte value, String humanString) {
        this.value = value;
        this.humanString = humanString;
    }

    /**
     * Get the value, as defined by the HART Communication Foundation.
     * 
     * @return The value.
     */
    public byte getValue() {
        return this.value;
    }

    /**
     * Get a human readable string for the given error code.
     * 
     * @return The human readable string.
     */
    public String getHumanString() {
        return this.humanString;
    }

    /**
     * Extract the {@link StatusCodeType} from the supplied byte code.
     * 
     * @param testValue
     *            The test value for which we want to find a corresponding status code.
     * @return The corresponding {@link StatusCodeType} instance for the given input value.
     * @throws HartSystemException
     *             If no matching {@link StatusCodeType} instance is found for the given input value.
     */
    public static StatusCodeType fromByte(byte testValue) throws HartSystemException {
        for (StatusCodeType statusCodeType : StatusCodeType.values()) {
            if (statusCodeType.getValue() == testValue) {
                return statusCodeType;
            }
        }
        throw new HartSystemException(String.format("Undefined HART-IP communications status code %d", testValue));
    }
}
