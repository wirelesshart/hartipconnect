package com.seeq.wirelesshart.hartipconnect.hartip.state;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.hartip.HartIp;
import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.MessageBody;
import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.SessionInitialization;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduEncoder;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;

/**
 * This state encodes/decodes the message body (payload), depending on the supplied Message ID.
 */
class MessageBodyState implements State {
    /**
     * The expected size of the HartIP header, in bytes.
     */
    public static final int NUMBER_OF_BYTES_IN_HEADER = 8;

    @Override
    public void decodeByte(byte newByte, StateContextForDecoder context) throws HartException {

        // If the byte count is just enough to hold the HART-IP envelope, then there is no message body to process, and
        // we are done.
        if (context.getByteCount() == NUMBER_OF_BYTES_IN_HEADER) {
            // There's nothing more to process.
            context.getOutputHartIp().setMessageBody(null);
            context.setActiveState(null);
        } else if (context.getByteCount() > NUMBER_OF_BYTES_IN_HEADER) {
            // Don't do anything for now. Just collect the incoming bytes for processing later.
            context.getWorkspaceBytes().add(newByte);

            // Once we collect enough bytes, process them, and jump to the next state.
            if (context.getWorkspaceBytes().size() == (context.getByteCount() - NUMBER_OF_BYTES_IN_HEADER)) {
                final MessageBody messageBody;

                switch (context.getOutputHartIp().getMessageId()) {
                case TOKEN_PASSING_PDU:
                    byte[] pduByteArray = HartUtilities.byteListToByteArray(context.getWorkspaceBytes());
                    messageBody = HartPduEncoder.decode(pduByteArray);
                    break;
                case KEEP_ALIVE:
                    messageBody = null;
                    break;
                case SESSION_CLOSE:
                    messageBody = null;
                    break;
                case SESSION_INIT:
                    byte[] initByteArray = HartUtilities.byteListToByteArray(context.getWorkspaceBytes());
                    messageBody = SessionInitialization.fromBytes(initByteArray);
                    break;
                case DISCOVERY:
                case WIRELESS_DIRECT_PDU:
                    // No documentation is presently found on the internal structure of these.
                    // TODO: efhilton 2015-04-23: Discuss with Mark N., Eric R.? Structure?
                default:
                    throw new UnsupportedOperationException("The supplied Message ID is not yet supported: "
                            + context.getOutputHartIp().getMessageId().name());
                }

                context.getOutputHartIp().setMessageBody(messageBody);

                context.getWorkspaceBytes().clear();
                context.setActiveState(null);
            }
        } else {
            // Not enough bytes were received.
            throw new LongitudinalParityErrorException("Expected at least " + NUMBER_OF_BYTES_IN_HEADER
                    + " bytes, but only " + context.getByteCount() + " bytes were received");
        }
    }

    @Override
    public void encodePacket(HartIp hartIp, StateContextForEncoder context) throws HartException {

        final byte[] byteArray;

        switch (hartIp.getMessageId()) {
        case TOKEN_PASSING_PDU:
            HartPdu hartPdu = (HartPdu) hartIp.getMessageBody();
            byteArray = HartPduEncoder.encode(hartPdu);
            break;
        case KEEP_ALIVE:
            byteArray = null;
            break;
        case SESSION_CLOSE:
            byteArray = null;
            break;
        case SESSION_INIT:
            SessionInitialization sessionInitialization = (SessionInitialization) hartIp.getMessageBody();
            byteArray = sessionInitialization.toBytes();
            break;
        case DISCOVERY:
        case WIRELESS_DIRECT_PDU:
            // No actual documentation is found on the structure of these.
            // TODO: efhilton 2015-04-23: Discuss with Mark N., Eric R.? Structure?
        default:
            throw new UnsupportedOperationException("The supplied Message ID is not yet supported: "
                    + hartIp.getMessageId().name());
        }

        context.getOutputBytes().addAll(HartUtilities.byteArrayToByteList(byteArray));

        context.setActiveState(null);
    }

}
