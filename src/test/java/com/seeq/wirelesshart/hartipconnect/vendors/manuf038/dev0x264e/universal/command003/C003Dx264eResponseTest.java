package com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.DeviceVariable;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.EngineeringUnitsType;
import com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003.C003Dx264eResponse;

public class C003Dx264eResponseTest {
    private final DeviceVariable test1 = new DeviceVariable(1.0f, EngineeringUnitsType.UNITS_001);
    private final DeviceVariable test2 = new DeviceVariable(2.0f, EngineeringUnitsType.UNITS_002);
    private final DeviceVariable test3 = new DeviceVariable(3.0f, EngineeringUnitsType.UNITS_003);
    private final DeviceVariable test4 = new DeviceVariable(4.0f, EngineeringUnitsType.UNITS_004);

    @Test
    public void test() {
        C003Dx264eResponse instance = new C003Dx264eResponse();
        assertNotNull(instance.getAmbientTemperature());
        assertNotNull(instance.getElectronicsTemperature());
        assertNotNull(instance.getNumberOfHealthyDevices());
        assertNotNull(instance.getNumberOfUnreachableDevices());

        instance.setAmbientTemperature(this.test1);
        instance.setElectronicsTemperature(this.test2);
        instance.setNumberOfHealthyDevices(this.test3);
        instance.setNumberOfUnreachableDevices(this.test4);

        assertEquals(this.test1, instance.getAmbientTemperature());
        assertEquals(this.test2, instance.getElectronicsTemperature());
        assertEquals(this.test3, instance.getNumberOfHealthyDevices());
        assertEquals(this.test4, instance.getNumberOfUnreachableDevices());

        assertTrue(instance.toString().matches(C003Dx264eResponse.class.getSimpleName() + " \\[.*\\]"));
    }
}
