package com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandDataFactory;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command003Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Rosemount0x264eCommand3FactoryTest {
    private static final short EXPANDED_DEVICE_TYPE_CODE = 0x264e;

    @Test
    public void testCreate() {
        // Register command 3 classes for the 1420 Generally, this would be done by a higher layer.
        CommandRegistry.COMMAND_003.registerResponseClass(EXPANDED_DEVICE_TYPE_CODE, C003Dx264eResponse.class);

        // Set up the frame address (minus the Unique Device Identifier)
        LongFrameAddress deviceAddress = new LongFrameAddress();
        deviceAddress.setExpandedDeviceTypeCode(EXPANDED_DEVICE_TYPE_CODE);

        CommandDataFactory instance = new CommandDataFactory();

        // Ensure that we build the correct response class.
        CommandData test = instance.create(CommandRegistry.COMMAND_003, FrameType.ACK, deviceAddress);
        assertNotNull(test);
        assertEquals(C003Dx264eResponse.class, test.getClass());

        // Ensure that we build the generic command 3 response.
        deviceAddress.setExpandedDeviceTypeCode((short) 0x1234);
        test = instance.create(CommandRegistry.COMMAND_003, FrameType.ACK, deviceAddress);
        assertNotNull(test);
        assertEquals(Command003Response.class, test.getClass());
        assertEquals(FrameType.ACK, test.getFrameType());
        assertEquals(CommandRegistry.COMMAND_003, test.getCommandType());

        // We do not support short frame addresses (only command 0 does).
        try {
            instance.create(CommandRegistry.COMMAND_003, FrameType.ACK, new ShortFrameAddress());
            fail("Command 3 does not support short frame addresses");
        } catch (IllegalArgumentException ex) {
            // all is well.
        }
    }
}
