package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;

public class DelimeterStateTest {

    @Test
    public void testRoundtripEncodingDecoding() throws Exception {
        HartPduTestAssistant.hartPduSweep(hartPdu -> this.testHartPdu(hartPdu));
    }

    private void testHartPdu(HartPdu hartPdu) throws HartException {
        StateContextForEncoder inputContext = new StateContextForEncoder();
        inputContext.setNumberOfExpansionBytes(hartPdu.getExpansionBytes() != null
                ? hartPdu.getExpansionBytes().length
                : 0);

        DelimeterState inputInstance = new DelimeterState();

        inputInstance.encodePacket(hartPdu, inputContext);

        assertNotNull(inputContext.getOutputBytes());
        assertTrue(inputContext.getOutputBytes().size() == 1);
        assertEquals(inputContext.getActiveState().getClass().getName(), AddressState.class.getName());

        byte newByte = inputContext.getOutputBytes().get(0);
        assertTrue(newByte != 0);

        StateContextForDecoder outputContext = new StateContextForDecoder();
        DelimeterState outputInstance = new DelimeterState();

        outputInstance.decodeByte(newByte, outputContext);

        assertNotNull(outputContext.getOutputHartPdu());
        assertNotNull(outputContext.getOutputHartPdu().getDeviceAddress()
                .getAddressType());

        assertEquals(hartPdu.getDeviceAddress().getAddressType(),
                outputContext.getOutputHartPdu().getDeviceAddress().getAddressType());
        assertEquals(inputContext.getNumberOfExpansionBytes(),
                outputContext.getNumberOfExpansionBytes());
        assertEquals(hartPdu.getFrameType(), outputContext.getOutputHartPdu().getFrameType());
        assertEquals(hartPdu.getPhysicalLayerType(), outputContext.getOutputHartPdu().getPhysicalLayerType());

        assertEquals(outputContext.getActiveState().getClass().getName(),
                AddressState.class.getName());
    }
}
