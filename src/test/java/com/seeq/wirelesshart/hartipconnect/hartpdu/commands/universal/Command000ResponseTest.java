package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command000Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Command000ResponseTest {

    @Test
    public void testToBytes() throws EngineeringUnitsException {
        Command000Response instance = new Command000Response();

        // Check the correct number of bytes are returned by default.
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertEquals(22, outBytes.size());

        // Test that we correctly reject wrong sized input byte arrays.
        try {
            instance.fromBytes(Arrays.asList(new Byte[] { 0x01, 0x02, 0x03, 0x04 }));
            fail("This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }

    @Test
    public void testToString() {
        Command000Response instance = new Command000Response();
        assertTrue(instance.toString().matches("Command000Response \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        Command000Response instance = new Command000Response();

        assertEquals(FrameType.ACK, instance.getFrameType());
    }

    @Test
    public void testGetTotalDataByteCount() {
        Command000Response instance = new Command000Response();
        List<Byte> outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(22, outBytes.size());
        assertEquals(22, instance.getTotalDataByteCount());
    }

    @Test
    public void testGetCommandType() {
        Command000Response instance = new Command000Response();

        assertEquals(CommandRegistry.COMMAND_000, instance.getCommandType());
    }

    @Test
    public void testRoundtrip() throws EngineeringUnitsException {
        Command000Response instance = new Command000Response();
        Command000Response built = new Command000Response();

        List<Byte> outList = instance.toBytes();
        built.fromBytes(outList);

        assertEquals(instance, built);
    }

}
