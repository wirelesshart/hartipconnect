package com.seeq.wirelesshart.hartipconnect.hartpdu.commands;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.CommandException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class CommandRegistryTest {
    private static final byte UNIMPLEMENTED_COMMAND = (byte) 0xff;

    @Test
    public void testFromCommandByte() {
        for (CommandRegistry instance : CommandRegistry.values()) {
            Byte testByte = instance.toCommandByte();
            Byte commandID = instance.getCommandID();
            String humanString = instance.getHumanString();
            Byte expCommandId = Integer.valueOf(instance.name().split("_")[1]).byteValue();

            assertNotNull(testByte);
            assertNotNull(humanString);

            assertTrue(testByte == commandID);
            assertEquals(expCommandId, commandID);

            try {
                CommandRegistry resultCommand = CommandRegistry.fromCommandByte(testByte);

                assertEquals(instance, resultCommand);
            } catch (CommandException e) {
                fail("This should not fail");
            }
        }

        try {
            CommandRegistry.fromCommandByte(UNIMPLEMENTED_COMMAND);
            fail("This should have failed");
        } catch (CommandException e) {
            // all is well!
        }
    }

    @Test
    public void testGetStrategyClass() {
        for (FrameType frameType : FrameType.values()) {
            for (boolean isPrimaryMaster : new boolean[] { true, false }) {
                for (boolean isDeviceInBurstMode : new boolean[] { true, false }) {
                    for (DeviceAddress deviceAddress : HartPduTestAssistant.buildDeviceAddressTestMatrix(
                            isPrimaryMaster, isDeviceInBurstMode)) {
                        for (CommandRegistry instance : CommandRegistry.values()) {
                            if (deviceAddress.getAddressType() == AddressType.IS_POLLING_0_BYTE) {
                                if (instance != CommandRegistry.COMMAND_000) {
                                    continue;
                                }
                            }
                            CommandData strategyInstance = instance.createNewInstanceOfStrategy(frameType,
                                    deviceAddress);
                            assertNotNull(strategyInstance);
                        }
                    }
                }
            }
        }
    }
}
