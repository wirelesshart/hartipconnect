package com.seeq.wirelesshart.hartipconnect.hartpdu.state;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

public class AddressStateTest {

    @Test
    public void testEncodeAndDecodeByte() throws HartException {
        HartPduTestAssistant.hartPduSweep(hartPdu -> this.testHartPdu(hartPdu));
    }

    private void testHartPdu(HartPdu expectedHartPdu) throws HartException {

        //
        // Set up the context for the state, and then do an encoding.
        //
        StateContextForEncoder inputContext = new StateContextForEncoder();
        inputContext.setActiveState(new AddressState());
        inputContext.setNumberOfExpansionBytes(0);

        inputContext.getActiveState().encodePacket(expectedHartPdu, inputContext);

        //
        // Make sure that encoding works as expected.
        //
        assertEquals(inputContext.getActiveState().getClass(), ExpansionBytesState.class);

        List<Byte> encodedBytes = inputContext.getOutputBytes();
        assertEquals(expectedHartPdu.getDeviceAddress().isPrimaryMaster(),
                (encodedBytes.get(0).byteValue() >> 7 & 0x01) == 0x01);
        assertEquals(expectedHartPdu.getDeviceAddress().isDeviceInBurstMode(),
                (encodedBytes.get(0).byteValue() >> 6 & 0x01) == 0x01);

        if (expectedHartPdu.getDeviceAddress().getAddressType() == AddressType.IS_POLLING_0_BYTE) {
            ShortFrameAddress myShortFrameAddress = (ShortFrameAddress) expectedHartPdu.getDeviceAddress();
            assertEquals(myShortFrameAddress.getPollingAddress(), (byte) (encodedBytes.get(0) & 0x3f));
        } else {
            LongFrameAddress expected = (LongFrameAddress) expectedHartPdu.getDeviceAddress();

            short expectedExpandedDeviceTypeCode = 0x0000;
            expectedExpandedDeviceTypeCode |= (encodedBytes.get(0).byteValue() & 0x3f) << 8;
            expectedExpandedDeviceTypeCode |= (encodedBytes.get(1).byteValue() & 0xff);
            assertEquals(expected.getExpandedDeviceTypeCode(), expectedExpandedDeviceTypeCode);

            for (int index = 0; index < 3; index++) {
                assertEquals(expected.getUniqueDeviceIdentifier()[index], encodedBytes.get(index + 2).byteValue());
            }
        }

        //
        // Perform a Decoding Test.
        //

        //
        // Set up the context for decoding.
        HartPdu outputHartPdu = new HartPdu();
        outputHartPdu.setDeviceAddress(expectedHartPdu.getDeviceAddress().getAddressType().createNewClassInstance());
        outputHartPdu.setPhysicalLayerType(expectedHartPdu.getPhysicalLayerType());
        outputHartPdu.setFrameType(expectedHartPdu.getFrameType());

        StateContextForDecoder outputContext = new StateContextForDecoder();
        outputContext.setActiveState(new AddressState());
        outputContext.setNumberOfExpansionBytes(0);
        outputContext.setOutputHartPdu(outputHartPdu);

        //
        // Do the decoding
        //
        int numDecoded = 0;
        for (Byte newByte : encodedBytes) {
            outputContext.getActiveState().decodeByte(newByte, outputContext);

            numDecoded++;
            if (numDecoded < encodedBytes.size()) {
                assertEquals(outputContext.getActiveState().getClass(), AddressState.class);
            }
        }
        assertEquals(encodedBytes.size(), numDecoded);

        //
        // Check the decoding results
        //
        assertEquals(outputContext.getActiveState().getClass(), ExpansionBytesState.class);

        //
        // Check that we had a successful roundtrip.
        //
        if (expectedHartPdu.getDeviceAddress().getAddressType() == AddressType.IS_POLLING_0_BYTE) {
            ShortFrameAddress expected = (ShortFrameAddress) expectedHartPdu.getDeviceAddress();
            ShortFrameAddress actual = (ShortFrameAddress) outputContext.getOutputHartPdu().getDeviceAddress();

            // Specifically check for items modified in the Address State.
            assertEquals(expected.isDeviceInBurstMode(), actual.isDeviceInBurstMode());
            assertEquals(expected.isPrimaryMaster(), actual.isPrimaryMaster());
            assertEquals(expected.getPollingAddress(), actual.getPollingAddress());

            // Catch-all
            assertEquals(expected, actual);
        } else {
            LongFrameAddress expected = (LongFrameAddress) expectedHartPdu.getDeviceAddress();
            LongFrameAddress actual = (LongFrameAddress) outputContext.getOutputHartPdu().getDeviceAddress();

            // Specifically check some of the items set in the Address State.
            assertEquals(expected.isPrimaryMaster(), actual.isPrimaryMaster());
            assertEquals(expected.isDeviceInBurstMode(), actual.isDeviceInBurstMode());
            assertEquals(expected.getAddressType(), actual.getAddressType());
            assertTrue(Arrays.equals(expected.getUniqueDeviceIdentifier(), actual.getUniqueDeviceIdentifier()));
            assertEquals(expected.getExpandedDeviceTypeCode(), actual.getExpandedDeviceTypeCode());

            // Catch-all.
            assertEquals(expected, actual);
        }
    }
}
