package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Command084ResponseTest {

    @Test
    public void testToBytes() throws EngineeringUnitsException {
        Command084Response instance = new Command084Response();

        // Check the correct number of bytes are returned by default.
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertEquals(Command084Response.TOTAL_BYTE_COUNT, outBytes.size());

        // Test that we correctly reject wrong sized input byte arrays.
        try {
            instance.fromBytes(Arrays.asList(new Byte[] { 0x01, 0x02, 0x03, 0x04 }));
            fail("This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }

    @Test
    public void testToString() {
        Command084Response instance = new Command084Response();
        assertTrue(instance.toString().matches("Command084Response \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        Command084Response instance = new Command084Response();

        assertEquals(FrameType.ACK, instance.getFrameType());
    }

    @Test
    public void testGetTotalDataByteCount() {
        Command084Response instance = new Command084Response();

        List<Byte> outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(Command084Response.TOTAL_BYTE_COUNT, outBytes.size());
        assertEquals(Command084Response.TOTAL_BYTE_COUNT, instance.getTotalDataByteCount());
    }

    @Test
    public void testGetCommandType() {
        Command084Response instance = new Command084Response();

        assertEquals(CommandRegistry.COMMAND_084, instance.getCommandType());
    }

    @Test
    public void testRoundtrip() throws EngineeringUnitsException {
        Command084Response instance = new Command084Response();
        instance.setLongTag("foo");

        Command084Response built = new Command084Response();

        List<Byte> outList = instance.toBytes();
        built.fromBytes(outList);

        assertEquals(instance.getLongTag(), built.getLongTag());
        assertEquals(instance, built);
    }

}
