package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

public class AddressTypeTest {

    @Test
    public void testToFromDelimeterByte() {
        for (boolean testBoolean : new boolean[] { true, false }) {
            byte testByte = (byte) (testBoolean ? (1 << 7) : 0x00);

            AddressType instance = AddressType.fromDelimeterByte(testByte);

            byte resultByte = instance.appendToDelimeterByte((byte) 0x00);

            assertEquals(testByte, resultByte);
        }
    }

    @Test
    public void testGetImplementingClass() {
        for (AddressType instance : AddressType.values()) {
            final Class<? extends DeviceAddress> expectedClassType;
            switch (instance) {
            case IS_POLLING_0_BYTE:
                expectedClassType = ShortFrameAddress.class;
                break;
            case IS_UNIQUE_5_BYTE:
                expectedClassType = LongFrameAddress.class;
                break;
            default:
                expectedClassType = null;
            }

            DeviceAddress deviceAddress = instance.createNewClassInstance();

            assertNotNull(deviceAddress);

            assertEquals(expectedClassType, deviceAddress.getClass());
        }
    }

}
