package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.communication.HartCommunicationException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.CommunicationErrorType;

public class CommunicationErrorTypeTest {

    @Test
    public void testGetters() throws InstantiationException, IllegalAccessException {
        for (CommunicationErrorType instance : CommunicationErrorType.values()) {
            int errorValue = instance.getErrorValue();
            assertTrue(errorValue != 0);

            String humanString = instance.getHumanReadableString();
            assertNotNull(humanString);

            Class<? extends HartCommunicationException> exceptionClassType = instance.getExceptionClass();
            assertNotNull(exceptionClassType);
            HartPduTestAssistant.checkExceptionImplementation(exceptionClassType);

            String testMessage;
            String expMessage;
            Throwable testThrowable;

            testMessage = null;
            testThrowable = null;
            expMessage = humanString;
            this.testExceptionCreation(instance, testMessage, expMessage, testThrowable);

            testMessage = "";
            testThrowable = null;
            expMessage = humanString;
            this.testExceptionCreation(instance, testMessage, expMessage, testThrowable);

            testMessage = "Test string";
            testThrowable = null;
            expMessage = testMessage;
            this.testExceptionCreation(instance, testMessage, expMessage, testThrowable);

            testMessage = null;
            testThrowable = new IllegalStateException();
            expMessage = humanString;
            this.testExceptionCreation(instance, testMessage, expMessage, testThrowable);

            testMessage = "Test String";
            testThrowable = new IllegalStateException();
            expMessage = testMessage;
            this.testExceptionCreation(instance, testMessage, expMessage, testThrowable);

        }
    }

    private void testExceptionCreation(CommunicationErrorType instance, String testMessage, String expMessage,
            Throwable testThrowable) {
        HartCommunicationException exceptionInstance = instance.createExceptionInstance(testMessage, testThrowable);
        assertNotNull(exceptionInstance);
        assertNotNull(exceptionInstance.getLocalizedMessage());
        assertEquals(expMessage, exceptionInstance.getLocalizedMessage());
        assertEquals(testThrowable, exceptionInstance.getCause());
    }

    @Test
    public void testFromToCommunicationErrorByte() {
        Map<Byte, CommunicationErrorType[]> testMatrix = new TreeMap<>();
        testMatrix.put((byte) 0x80, new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
        testMatrix.put((byte) 0xf0, new CommunicationErrorType[] {
                CommunicationErrorType.COMMUNICATION_ERROR,
                CommunicationErrorType.VERTICAL_PARITY_ERROR,
                CommunicationErrorType.OVERRUN_ERROR,
                CommunicationErrorType.FRAMING_ERROR });
        testMatrix.put((byte) 0xfa, new CommunicationErrorType[] {
                CommunicationErrorType.COMMUNICATION_ERROR,
                CommunicationErrorType.VERTICAL_PARITY_ERROR,
                CommunicationErrorType.OVERRUN_ERROR,
                CommunicationErrorType.FRAMING_ERROR,
                CommunicationErrorType.LONGITUDINAL_PARITY_ERROR,
                CommunicationErrorType.BUFFER_OVERFLOW });

        for (Byte testByte : testMatrix.keySet()) {
            CommunicationErrorType[] expectedErrors = testMatrix.get(testByte);
            CommunicationErrorType[] foundErrors = CommunicationErrorType.fromCommunicationErrorByte(testByte);

            assertEquals(expectedErrors.length, foundErrors.length);

            for (CommunicationErrorType expError : expectedErrors) {
                assertTrue(Arrays.asList(foundErrors).contains(expError));
            }

            Byte outByte = CommunicationErrorType.toCommunicationErrorByte(expectedErrors);
            assertEquals(testByte, outByte);
        }

        for (CommunicationErrorType instance : CommunicationErrorType.values()) {
            if (instance == CommunicationErrorType.COMMUNICATION_ERROR) {
                byte outByte = CommunicationErrorType
                        .toCommunicationErrorByte(new CommunicationErrorType[] { instance });
                assertEquals((byte) instance.getErrorValue(), outByte);
            } else {
                try {
                    CommunicationErrorType.toCommunicationErrorByte(new CommunicationErrorType[] { instance });
                    fail("This should have failed");
                } catch (IllegalArgumentException ex) {
                    // all is well.
                }
            }
        }
    }

    @Test
    public void testIsErrorInErrorByte() {
        Map<Byte, Boolean> testMatrix = new TreeMap<>();
        testMatrix.put((byte) 0x80, Boolean.TRUE);
        testMatrix.put((byte) 0x40, Boolean.FALSE);
        testMatrix.put((byte) 0x20, Boolean.FALSE);
        testMatrix.put((byte) 0x10, Boolean.FALSE);
        testMatrix.put((byte) 0x08, Boolean.FALSE);
        testMatrix.put((byte) 0x04, Boolean.FALSE);
        testMatrix.put((byte) 0x02, Boolean.FALSE);
        testMatrix.put((byte) 0x01, Boolean.FALSE);
        testMatrix.put((byte) 0x00, Boolean.FALSE);

        for (Byte testByte : testMatrix.keySet()) {
            Boolean expected = testMatrix.get(testByte);
            Boolean actual = CommunicationErrorType.isErrorInErrorByte(testByte);
            assertEquals(expected, actual);

            CommunicationErrorType[] foundErrors = CommunicationErrorType.fromCommunicationErrorByte(testByte
                    .byteValue());
            if (expected == Boolean.TRUE) {
                assertEquals(1, foundErrors.length);
            } else {
                assertEquals(0, foundErrors.length);
            }

        }
    }
}
