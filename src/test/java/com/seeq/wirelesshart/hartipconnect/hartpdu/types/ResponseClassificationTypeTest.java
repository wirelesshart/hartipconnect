package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.response.HartResponseException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseClassificationType;

public class ResponseClassificationTypeTest {

    @Test
    public void testGetExceptionClass() throws InstantiationException, IllegalAccessException {
        for (ResponseClassificationType instance : ResponseClassificationType.values()) {
            if (instance.name().matches("IS_ERROR.*")) {

                Class<? extends HartResponseException> exceptionClass = instance.getExceptionClass();
                assertNotNull(exceptionClass);
                assertNotNull(exceptionClass.newInstance());

                HartResponseException exceptionClassInstance = instance.createNewInstanceOfException();
                assertNotNull(exceptionClassInstance);
                assertTrue(exceptionClass.isAssignableFrom(exceptionClass));

            } else {
                assertNull(instance.getExceptionClass());
                assertNull(instance.createNewInstanceOfException());
            }
        }
    }
}
