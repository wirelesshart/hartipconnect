package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.FrameException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class FrameTypeTest {

    @Test
    public void testFromDelimeterByte() {
        for (int value = 0; value <= 0xff; value++) {
            byte testByte = (byte) value;

            switch (testByte & 0x07) {
            case 0x01:
            case 0x02:
            case 0x06:
                try {
                    FrameType type = FrameType.fromDelimeterByte(testByte);
                    assertEquals(type.getDecodeByte(), testByte & 0x07);
                } catch (FrameException e) {
                    fail("This should not fail");
                }
                break;
            default:
                try {
                    FrameType.fromDelimeterByte(testByte);
                    fail("This should have failed for " + testByte);
                } catch (FrameException e) {
                    // all is well.
                }
            }
        }
    }

    @Test
    public void testAppendToDelimeterByte() {
        for (FrameType instance : FrameType.values()) {
            byte actualByte = instance.appendToDelimeterByte((byte) 0x00);
            assertEquals(instance.getDecodeByte(), actualByte);

            try {
                FrameType actualFrameType = FrameType.fromDelimeterByte(actualByte);
                assertEquals(instance, actualFrameType);
            } catch (FrameException e) {
                fail("This should not fail");
            }
        }
    }

}
