package com.seeq.wirelesshart.hartipconnect.hartpdu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class HartUtilitiesTest {

    @Test
    public void testFloatToFromByteArray() {
        float expectedFloat = 5.0f;

        // Test conversion to byte array.
        byte[] byteArray = HartUtilities.floatToByteArray(expectedFloat);
        assertNotNull(byteArray);
        assertEquals(4, byteArray.length);

        // Test roundtrip.
        assertEquals(expectedFloat, HartUtilities.floatFromBytes(byteArray), 0.0f);

        // Test handling of null pointer.
        try {
            HartUtilities.floatFromBytes(null);
            fail("Null pointer should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }

        // Test handling of wrong sized arrays.
        try {
            HartUtilities.floatFromBytes(new byte[]{1, 2, 3});
            fail("Four bytes expected. This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }

    @Test
    public void testByteListToFromByteArray() {

        byte[] testByteArray = new byte[]{0x1, 0x2, 0x3, 0x4, 0x5, 0x6};

        // test proper handling of null.
        List<Byte> emptyList = HartUtilities.byteArrayToByteList(null);
        assertNotNull(emptyList);
        assertTrue(emptyList.isEmpty());

        assertNull(HartUtilities.byteListToByteArray(null));
        assertNull(HartUtilities.byteListToByteArray(new ArrayList<>()));

        // Test conversion to List<Byte>
        List<Byte> byteList = HartUtilities.byteArrayToByteList(testByteArray);
        assertNotNull(byteList);
        assertEquals(testByteArray.length, byteList.size());

        // Test roundtrip.
        assertTrue(Arrays.equals(testByteArray, HartUtilities.byteListToByteArray(byteList)));
    }

    @Test
    public void testShortToFromByteArray() {
        short expectedShort = 0x1111;

        // Test conversion to byte array.
        byte[] byteArray = HartUtilities.shortToByteArray(expectedShort);
        assertNotNull(byteArray);
        assertEquals(2, byteArray.length);

        // Test roundtrip.
        assertEquals(expectedShort, HartUtilities.shortFromBytes(byteArray), 0.0f);

        // Test handling of null pointer.
        try {
            HartUtilities.shortFromBytes(null);
            fail("Null pointer should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }

        // Test handling of wrong sized arrays.
        try {
            HartUtilities.shortFromBytes(new byte[]{1, 2, 3});
            fail("Two bytes expected. This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }
}
