package com.seeq.wirelesshart.hartipconnect.hartpdu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;

public class HartPduEncodeDecodeTest {
    private static final Logger log = Logger.getLogger(HartPduEncodeDecodeTest.class.getName());

    @Test
    public void test() throws HartException {
        HartPduTestAssistant.hartPduSweep(hartPdu -> this.testHartPdu(hartPdu));
    }

    private void testHartPdu(HartPdu expectedHartPdu) throws HartException {
        log.fine("------------------------------");
        log.fine("EXPECT: " + expectedHartPdu);

        try {
            byte[] encodedBytes = HartPduEncoder.encode(expectedHartPdu);

            if (expectedHartPdu.getCommandType() == CommandRegistry.COMMAND_031) {
                fail("This should have failed");
            }

            StringBuilder builder = new StringBuilder();
            builder.append("ENCODE: ");
            for (byte outByte : encodedBytes) {
                builder.append(String.format("0x%x ", outByte));
            }
            log.fine(builder.toString());

            HartPdu decodedHartPdu = HartPduEncoder.decode(encodedBytes);
            
            log.fine("REC'VD: " + decodedHartPdu);
            assertEquals(expectedHartPdu, decodedHartPdu);
        } catch (UnsupportedOperationException ex) {
            if (expectedHartPdu.getCommandType() != CommandRegistry.COMMAND_031) {
                fail("This should not have failed");
            } else {
                log.fine("##### Correctly failed command 0x31");
            }
        }
    }
}
