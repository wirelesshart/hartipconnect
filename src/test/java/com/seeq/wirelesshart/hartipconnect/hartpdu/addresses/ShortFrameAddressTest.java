package com.seeq.wirelesshart.hartipconnect.hartpdu.addresses;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

public class ShortFrameAddressTest {

    @Test
    public void testGetPollingAddress() {
        ShortFrameAddress instance = new ShortFrameAddress();
        instance.setDeviceInBurstMode(true);
        instance.setPollingAddress((byte) 0x00);
        instance.setPrimaryMaster(true);

        assertEquals(AddressType.IS_POLLING_0_BYTE, instance.getAddressType());
        assertEquals(true, instance.isDeviceInBurstMode());
        assertEquals(true, instance.isPrimaryMaster());
        assertEquals((byte) 0x00, instance.getPollingAddress());
    }

    @Test
    public void testEqualsObject() {
        ShortFrameAddress instance = new ShortFrameAddress();
        instance.setDeviceInBurstMode(true);
        instance.setPollingAddress((byte) 0x00);
        instance.setPrimaryMaster(true);

        ShortFrameAddress instance2 = new ShortFrameAddress();
        instance2.setDeviceInBurstMode(true);
        instance2.setPollingAddress((byte) 0x00);
        instance2.setPrimaryMaster(true);

        assertEquals(instance, instance);
        assertFalse(instance.equals(null));
        assertFalse(instance.equals(Integer.valueOf(0)));

        assertTrue(instance.equals(instance2));

        instance.setPollingAddress((byte) 0x00);
        instance2.setPollingAddress((byte) 0x01);
        assertFalse(instance.equals(instance2));

        instance.setPrimaryMaster(true);
        instance2.setPrimaryMaster(false);
        assertFalse(instance.equals(instance2));

        instance.setDeviceInBurstMode(true);
        instance2.setDeviceInBurstMode(false);
        assertFalse(instance.equals(instance2));

        assertFalse(instance.equals(new LongFrameAddress()));
    }

    @Test
    public void testToString() {
        ShortFrameAddress instance = new ShortFrameAddress();
        assertTrue(instance.toString().matches("ShortFrameAddress \\[.*\\]"));
    }

    @Test
    public void testHashCode() {
        ShortFrameAddress instance = new ShortFrameAddress();
        int baseLine = instance.hashCode();

        instance.setDeviceInBurstMode(true);
        int wBurstMode = instance.hashCode();
        assertTrue(wBurstMode < baseLine);

        instance.setPrimaryMaster(false);
        int woPrimaryMaster = instance.hashCode();
        assertTrue(woPrimaryMaster > wBurstMode);
    }
}
