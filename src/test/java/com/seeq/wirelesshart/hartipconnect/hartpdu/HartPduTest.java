package com.seeq.wirelesshart.hartipconnect.hartpdu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.CommandException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.CommunicationErrorType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;
import com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003.C003Dx264eResponse;

public class HartPduTest {
    @Test
    public void comprehensiveTest() throws HartException {
        HartPduTestAssistant.hartPduSweep(hartPdu -> this.testHartPdu(hartPdu));
    }

    private void testHartPdu(HartPdu instance) throws CommandException {
        FrameType frameType = instance.getFrameType();
        PhysicalLayerType physicalLayerType = instance.getPhysicalLayerType();
        DeviceStatusType[] deviceStatus = instance.getDeviceStatus();
        byte[] expansionByte = instance.getExpansionBytes();
        CommunicationErrorType[] communicationErrors = instance.getCommunicationErrors();
        DeviceAddress deviceAddress = instance.getDeviceAddress();
        CommandData commandPacket = instance.getCommandData();
        ResponseCodeType responseCodeType = instance.getResponseCodeType();
        CommandRegistry commandRegistry = instance.getCommandType();

        if (deviceAddress.getAddressType() == AddressType.IS_POLLING_0_BYTE) {
            return;
        }

        instance.validate();
        instance.hashCode();

        HartPdu newInstance = new HartPdu();
        newInstance.setFrameType(frameType);
        newInstance.setPhysicalLayerType(physicalLayerType);
        newInstance.setDeviceStatus(deviceStatus);
        newInstance.setExpansionBytes(expansionByte);
        newInstance.setCommunicationErrors(communicationErrors);
        newInstance.setDeviceAddress(deviceAddress);
        newInstance.setCommandData(commandPacket);
        newInstance.setCommandType(commandRegistry);
        newInstance.setResponseCodeType(responseCodeType);

        assertEquals(instance, instance);
        assertFalse(instance.equals(null));
        assertFalse(instance.equals(Integer.valueOf(5)));

        assertTrue(instance.equals(newInstance));

        newInstance.setFrameType(null);
        assertFalse(instance.equals(newInstance));
        newInstance.setFrameType(frameType);

        newInstance.setCommandData(commandPacket == null ? new C003Dx264eResponse() : null);
        assertFalse(instance.equals(newInstance));
        newInstance.setCommandData(commandPacket);

        instance.setCommandData(commandPacket == null ? new C003Dx264eResponse() : null);
        assertFalse(instance.equals(newInstance));
        newInstance.setCommandData(instance.getCommandData());
        assertTrue(instance.equals(newInstance));
        newInstance.setCommandData(commandPacket);
        newInstance.setCommandType(commandRegistry);
        instance.setCommandData(commandPacket);
        instance.setCommandType(commandRegistry);

        assertTrue(instance.equals(newInstance));
        CommandRegistry tmpCommandtype = CommandRegistry.fromCommandByte((byte) (instance.getCommandType().getCommandID() + 1));
        CommandData tmpCommandPacket = tmpCommandtype.createNewInstanceOfStrategy(frameType, deviceAddress);
        newInstance.setCommandData(tmpCommandPacket);
        newInstance.setCommandType(tmpCommandtype);
        assertFalse(instance.equals(newInstance));
        newInstance.setCommandType(commandRegistry);  // unlikely to happen, but let's test it out anyway.
        assertFalse(instance.equals(newInstance));
        newInstance.setCommandData(commandPacket);
        newInstance.setCommandType(commandRegistry);
        instance.setCommandData(commandPacket);
        instance.setCommandType(commandRegistry);
        assertTrue(instance.equals(newInstance));

        newInstance.setCommunicationErrors(communicationErrors == null ? new CommunicationErrorType[] {} : null);
        assertFalse(instance.equals(newInstance));
        newInstance.setCommunicationErrors(communicationErrors);

        instance.setDeviceAddress(null);
        assertFalse(instance.equals(newInstance));
        newInstance.setDeviceAddress(null);
        assertTrue(instance.equals(newInstance));
        newInstance.setDeviceAddress(deviceAddress);
        instance.setDeviceAddress(deviceAddress);

        DeviceAddress tmpAddress = (deviceAddress.getClass() == LongFrameAddress.class)
                ? new ShortFrameAddress()
                : new LongFrameAddress();
        newInstance.setDeviceAddress(tmpAddress);
        assertFalse(instance.equals(newInstance));
        newInstance.setDeviceAddress(deviceAddress);

        newInstance.setDeviceStatus(deviceStatus == null ? new DeviceStatusType[] {} : null);
        assertFalse(instance.equals(newInstance));
        newInstance.setDeviceStatus(deviceStatus);

        newInstance.setPhysicalLayerType(null);
        assertFalse(instance.equals(newInstance));
        newInstance.setPhysicalLayerType(physicalLayerType);

        newInstance.setExpansionBytes(new byte[] { 0x5f, 0x4f, 0x3f, 0x2f });
        assertFalse(instance.equals(newInstance));
        newInstance.setExpansionBytes(expansionByte);

        newInstance.setResponseCodeType(responseCodeType == null ? ResponseCodeType.RC_000 : null);
        assertFalse(instance.equals(newInstance));
        newInstance.setResponseCodeType(responseCodeType);

        instance.setCommandType(null);
        newInstance.setCommandType(tmpCommandtype);
        assertFalse(instance.equals(newInstance));
        newInstance.setCommandType(null);
        assertTrue(instance.equals(newInstance));
        newInstance.setCommandType(commandRegistry);
        instance.setCommandType(commandRegistry);

        Logger.getLogger("Successful test of HartPDU packet: " + instance.toString());
    }

    @Test
    public void testValidate() {
        HartPdu reference = new HartPdu();
        reference.setDeviceAddress(new LongFrameAddress());
        reference.setPhysicalLayerType(PhysicalLayerType.IS_ASYNCHRONOUS);
        reference.setExpansionBytes(new byte[] {});
        reference.setCommandType(CommandRegistry.COMMAND_003);
        reference.validate();

        HartPdu test = new HartPdu();
        test.setExpansionBytes(reference.getExpansionBytes());
        test.setCommandType(reference.getCommandType());
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }
        test.setDeviceAddress(reference.getDeviceAddress());

        test.setPhysicalLayerType(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }
        test.setPhysicalLayerType(reference.getPhysicalLayerType());

        test.setExpansionBytes(null);
        try {
            test.validate();
        } catch (IllegalStateException e) {
            fail("This should NOT have failed.");
        }
        test.setExpansionBytes(reference.getExpansionBytes());

        test.setCommandType(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }
        test.setCommandType(reference.getCommandType());

        test.setFrameType(FrameType.STX);
        test.setCommunicationErrors(new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
        test.setDeviceStatus(new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        test.setResponseCodeType(ResponseCodeType.RC_000);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.STX);
        test.setCommunicationErrors(new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
        test.setDeviceStatus(null);
        test.setResponseCodeType(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.STX);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        test.setResponseCodeType(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.STX);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(null);
        test.setResponseCodeType(ResponseCodeType.RC_000);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.STX);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(null);
        test.setResponseCodeType(null);
        try {
            test.validate();
        } catch (IllegalStateException e) {
            fail("This should NOT have failed.");
        }

        test.setFrameType(FrameType.ACK);
        test.setCommunicationErrors(new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
        test.setDeviceStatus(new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        try {
            test.validate();
        } catch (IllegalStateException e) {
            fail("This should NOT have failed.");
        }

        test.setFrameType(FrameType.ACK);
        test.setCommunicationErrors(new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
        test.setDeviceStatus(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.ACK);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        test.setResponseCodeType(ResponseCodeType.RC_000);
        try {
            test.validate();
        } catch (IllegalStateException e) {
            fail("This should NOT have failed.");
        }

        test.setFrameType(FrameType.ACK);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(null);
        test.setResponseCodeType(ResponseCodeType.RC_000);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

        test.setFrameType(FrameType.ACK);
        test.setCommunicationErrors(null);
        test.setDeviceStatus(new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        test.setResponseCodeType(null);
        try {
            test.validate();
            fail("This should have failed.");
        } catch (IllegalStateException e) {
            // all is well.
        }

    }

}
