package com.seeq.wirelesshart.hartipconnect.hartpdu;

import static junit.framework.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.CommunicationErrorType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseClassificationType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;

/**
 * This class provides some functions that may be useful to other unit test classes.
 */
public class HartPduTestAssistant {

    /**
     * A callback interface.
     */
    public interface HartPduCallback {
        void doTest(HartPdu testHartPdu) throws HartException;
    }

    private static short ROSEMOUNT_0X264E_WIRELESS_GATEWAY_EXPANDED_DEVICE_TYPE_CODE = 0x264e;

    /**
     * Generate a test matrix for {@link CommunicationErrorType}.
     * 
     * @param frameType
     *            The {@link FrameType}. If this is {@link FrameType#STX}, then a clear list is returned.
     * @return The list of errors.
     */
    public static Map<Byte, CommunicationErrorType[]> buildCommunicationErrorTestMatrix(FrameType frameType) {
        Map<Byte, CommunicationErrorType[]> testMatrix = new TreeMap<>();

        if (frameType != FrameType.STX) {
            testMatrix.put((byte) (0x1 << 7),
                    new CommunicationErrorType[] { CommunicationErrorType.COMMUNICATION_ERROR });
            testMatrix.put((byte) 0xf0, new CommunicationErrorType[] {
                    CommunicationErrorType.COMMUNICATION_ERROR,
                    CommunicationErrorType.VERTICAL_PARITY_ERROR,
                    CommunicationErrorType.OVERRUN_ERROR,
                    CommunicationErrorType.FRAMING_ERROR
            });
            testMatrix.put((byte) 0x8A, new CommunicationErrorType[] {
                    CommunicationErrorType.COMMUNICATION_ERROR,
                    CommunicationErrorType.LONGITUDINAL_PARITY_ERROR,
                    CommunicationErrorType.BUFFER_OVERFLOW
            });
            testMatrix.put((byte) 0x00, null);
        } else {
            testMatrix.put((byte) 0x00, null);
        }

        return testMatrix;
    }

    /**
     * Build Expansion Byte test matrix.
     * 
     * @return The expansion byte test matrix.
     */
    public static byte[][] buldExpansionByteTestMatrix() {
        byte[][] testMatrix = new byte[][] {
                null,
                new byte[] { 0x01 },
                new byte[] { 0x01, 0x02 },
                new byte[] { 0x01, 0x02, 0x03 } };
        return testMatrix;
    }

    /**
     * Build a device status test matrix.
     * 
     * @param frameType
     *            A {@link FrameType} instance.
     * @return The test matrix. If STX, then the result is null.
     */
    public static Map<Byte, DeviceStatusType[]> buildDeviceStatusTestMatrix(FrameType frameType) {
        Map<Byte, DeviceStatusType[]> testMatrix = new TreeMap<>();

        if (frameType != FrameType.STX) {
            testMatrix.put((byte) 0xf0,
                    new DeviceStatusType[] {
                            DeviceStatusType.DEVICE_MALFUNCTION,
                            DeviceStatusType.CONFIGURATION_CHANGED,
                            DeviceStatusType.COLD_START,
                            DeviceStatusType.MORE_STATUS_AVAILABLE });
            testMatrix.put((byte) 0xff,
                    new DeviceStatusType[] {
                            DeviceStatusType.DEVICE_MALFUNCTION,
                            DeviceStatusType.CONFIGURATION_CHANGED,
                            DeviceStatusType.COLD_START,
                            DeviceStatusType.MORE_STATUS_AVAILABLE,
                            DeviceStatusType.LOOP_CURRENT_FIXED,
                            DeviceStatusType.LOOP_CURRENT_SATURATED,
                            DeviceStatusType.NON_PRIMARY_VARIABLE_OOL,
                            DeviceStatusType.PRIMARY_VARIABLE_OOL });
            testMatrix.put((byte) 0x0, new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        } else {
            testMatrix.put((byte) 0x0, null);
        }

        return testMatrix;
    }

    /**
     * Build a Device Address test matrix.
     * 
     * @param isPrimaryMaster
     *            Set to true if this is the primary master, false for secondary master.
     * @param isDeviceInBurstMode
     *            Set to true if this device is in burst mode.
     * @return The test matrix.
     */
    public static DeviceAddress[] buildDeviceAddressTestMatrix(boolean isPrimaryMaster, boolean isDeviceInBurstMode) {
        LongFrameAddress longFrameAddress = new LongFrameAddress();
        longFrameAddress.setDeviceInBurstMode(isDeviceInBurstMode);
        longFrameAddress.setPrimaryMaster(isPrimaryMaster);
        longFrameAddress.setExpandedDeviceTypeCode(ROSEMOUNT_0X264E_WIRELESS_GATEWAY_EXPANDED_DEVICE_TYPE_CODE);
        longFrameAddress.setUniqueDeviceIdentifier(new byte[] { 0x01, 0x02, 0x03 });

        ShortFrameAddress shortFrameAddress = new ShortFrameAddress();
        shortFrameAddress.setDeviceInBurstMode(isDeviceInBurstMode);
        shortFrameAddress.setPrimaryMaster(isPrimaryMaster);
        shortFrameAddress.setPollingAddress((byte) 30);

        return new DeviceAddress[] { longFrameAddress, shortFrameAddress };
    }

    /**
     * Build a test matrix of {@link CommandData} instances.
     * 
     * @param frameType
     *            The {@link FrameType} instance.
     * @param deviceAddress
     *            The address of the target device.
     * 
     * @return The test matrix.
     */
    public static CommandData[] buildCommandPacketTestMatrix(FrameType frameType, DeviceAddress deviceAddress) {
        List<CommandData> outList = new ArrayList<>();
        if (deviceAddress.getClass().equals(ShortFrameAddress.class)) {
            CommandData commandPacket = CommandRegistry.COMMAND_000.createNewInstanceOfStrategy(frameType, deviceAddress);

            if (commandPacket.getClass() == GenericCommandData.class) {
                ((GenericCommandData) commandPacket).setCommandType(CommandRegistry.COMMAND_000);
                ((GenericCommandData) commandPacket).setFrameType(frameType);
            }

            outList.add(commandPacket);
        } else {
            for (CommandRegistry commandRegistry : new CommandRegistry[] {
                    CommandRegistry.COMMAND_000,
                    CommandRegistry.COMMAND_003,
                    CommandRegistry.COMMAND_074,
                    CommandRegistry.COMMAND_084,
                    CommandRegistry.COMMAND_202
            }) {

                CommandData commandPacket = commandRegistry.createNewInstanceOfStrategy(frameType, deviceAddress);

                if (commandPacket.getClass() == GenericCommandData.class) {
                    ((GenericCommandData) commandPacket).setCommandType(commandRegistry);
                    ((GenericCommandData) commandPacket).setFrameType(frameType);
                }

                outList.add(commandPacket);
            }
        }

        return outList.toArray(new CommandData[0]);
    }

    /**
     * Sweep through all possible corner case scenarios. A client can attach a callback to it so that can be called for
     * each instance.
     * 
     * @param callback
     *            The callback routine, which implements the {@link HartPduCallback} interface.
     * @throws HartException
     *             If the callback throws an exception, TBD by callback.
     */
    public static void hartPduSweep(HartPduCallback callback) throws HartException {
        for (FrameType frameType : FrameType.values()) {

            byte[][] expansionByteMatrix = HartPduTestAssistant.buldExpansionByteTestMatrix();

            Map<Byte, DeviceStatusType[]> deviceStatusTestMatrix = HartPduTestAssistant
                    .buildDeviceStatusTestMatrix(frameType);

            for (boolean isPrimaryMaster : new boolean[] { true, false }) {
                for (boolean isDeviceInBurstMode : new boolean[] { true, false }) {
                    for (DeviceAddress deviceAddress : HartPduTestAssistant.buildDeviceAddressTestMatrix(
                            isPrimaryMaster, isDeviceInBurstMode)) {
                        for (byte[] expansionByte : expansionByteMatrix) {
                            for (DeviceStatusType[] deviceStatusList : deviceStatusTestMatrix.values()) {
                                for (PhysicalLayerType physicalLayerType : PhysicalLayerType.values()) {
                                    for (CommandData commandPacket : HartPduTestAssistant.buildCommandPacketTestMatrix(
                                            frameType, deviceAddress)) {
                                        for (CommunicationErrorType[] communicationErrors : HartPduTestAssistant
                                                .buildCommunicationErrorTestMatrix(frameType).values()) {
                                            for (ResponseCodeType responseCodeType : HartPduTestAssistant
                                                    .buildResponseCodeTestMatrix(frameType, communicationErrors)) {

                                                HartPdu hartPdu = new HartPdu();
                                                hartPdu.setFrameType(frameType);
                                                hartPdu.setPhysicalLayerType(physicalLayerType);
                                                hartPdu.setDeviceStatus(deviceStatusList);
                                                hartPdu.setExpansionBytes(expansionByte);
                                                hartPdu.setDeviceAddress(deviceAddress);

                                                hartPdu.setCommunicationErrors(frameType == FrameType.STX
                                                        ? null
                                                        : communicationErrors);

                                                hartPdu.setCommandType(commandPacket.getCommandType());
                                                hartPdu.setCommandData(hartPdu.getCommunicationErrors() == null
                                                        ? commandPacket
                                                        : null);

                                                hartPdu.setResponseCodeType((frameType == FrameType.STX
                                                        || (frameType != FrameType.STX && hartPdu
                                                        .getCommunicationErrors() != null))
                                                        ? null
                                                        : responseCodeType);

                                                // Call our listener.
                                                callback.doTest(hartPdu);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Build a {@link ResponseCodeType} test matrix. This method returns at least one {@link ResponseCodeType} from each
     * of the {@link ResponseClassificationType} types.
     * 
     * @param frameType
     *            The {@FrameType}.
     * @param commErrors
     *            The communication error list, or null.
     * @return The test matrix. If {@link FrameType} instance is STX, or the communication erros list is not null/empty,
     *         then the returning test matrix is empty.
     */
    public static ResponseCodeType[] buildResponseCodeTestMatrix(FrameType frameType,
            CommunicationErrorType[] commErrors) {
        if (frameType == FrameType.STX || (commErrors != null && Arrays.asList(commErrors).isEmpty())) {
            return new ResponseCodeType[] { null };
        } else {
            return new ResponseCodeType[] {
                    ResponseCodeType.RC_000,
                    ResponseCodeType.RC_002,
                    ResponseCodeType.RC_008,
                    ResponseCodeType.RC_010,
                    ResponseCodeType.RC_030 };
        }
    }

    /**
     * Verify that the implementation of all known exceptions is complete. This checks that the zero arg constructor,
     * the single argument (message) constructor, and the double argument (message, cause) constructors are defined.
     * 
     * @param exceptionClassType
     *            The class type that we are to test, which should be a decendant class of the {@link HartException}
     *            class.
     */
    public static void checkExceptionImplementation(Class<? extends HartException> exceptionClassType) {
        String message = "Test String";
        Throwable cause = new IllegalArgumentException();

        //
        // Try the Exception(msg, cause) constructor
        //
        try {
            Constructor<? extends HartException> constructor = exceptionClassType
                    .getDeclaredConstructor(String.class, Throwable.class);
            constructor.setAccessible(true);

            Object[] argumentList = new Object[] { message, cause };

            constructor.newInstance(argumentList);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException ex) {
            fail("Missing (msg,cause) constructor for " + exceptionClassType.getName());
        }

        //
        // Try the Exception(msg) constructor
        //
        try {
            Constructor<? extends HartException> constructor = exceptionClassType
                    .getDeclaredConstructor(String.class);
            constructor.setAccessible(true);

            Object[] argumentList = new Object[] { message };

            constructor.newInstance(argumentList);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
                | NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(CommunicationErrorType.class.getName()).log(Level.SEVERE, null, ex);
            fail("Missing (msg) constructor for " + exceptionClassType.getName());
        }

        //
        // Try the Exception() constructor
        //
        try {
            exceptionClassType.newInstance();
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | SecurityException ex) {
            Logger.getLogger(CommunicationErrorType.class.getName()).log(Level.SEVERE, null, ex);
            fail("Missing zero arg constructor for " + exceptionClassType.getName());
        }
    }
}
