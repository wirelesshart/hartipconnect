package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Command074ResponseTest {

    @Test
    public void testToBytes() throws EngineeringUnitsException {
        Command074Response instance = new Command074Response();

        // Check the correct number of bytes are returned by default.
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertEquals(Command074Response.TOTAL_BYTE_COUNT, outBytes.size());

        // Test that we correctly reject wrong sized input byte arrays.
        try {
            instance.fromBytes(Arrays.asList(new Byte[] { 0x01, 0x02, 0x03, 0x04 }));
            fail("This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }

    @Test
    public void testToString() {
        Command074Response instance = new Command074Response();
        assertTrue(instance.toString().matches("Command074Response \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        Command074Response instance = new Command074Response();

        assertEquals(FrameType.ACK, instance.getFrameType());
    }

    @Test
    public void testGetTotalDataByteCount() {
        Command074Response instance = new Command074Response();

        List<Byte> outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(Command074Response.TOTAL_BYTE_COUNT, outBytes.size());
        assertEquals(Command074Response.TOTAL_BYTE_COUNT, instance.getTotalDataByteCount());
    }

    @Test
    public void testGetCommandType() {
        Command074Response instance = new Command074Response();

        assertEquals(CommandRegistry.COMMAND_074, instance.getCommandType());
    }

    @Test
    public void testRoundtrip() throws EngineeringUnitsException {
        Command074Response instance = new Command074Response();
        instance.setMaximumNumberOfChannelsPerIoCard((byte) 1);
        instance.setMaximumNumberOfDelayedResponsesSupported((byte) 2);
        instance.setMaximumNumberOfIoCards((byte) 3);
        instance.setMaximumNumberOfSubDevicesPerChannel((byte) 4);
        instance.setNumberOfDevicesDetectedInclIoSystem((short) 5);
        instance.setPrimaryMaster(true);
        instance.setRetryCountToUseWhenSendingCommandsToASubDevice((byte) 6);

        Command074Response built = new Command074Response();

        List<Byte> outList = instance.toBytes();
        built.fromBytes(outList);

        assertEquals(instance, built);
    }

}
