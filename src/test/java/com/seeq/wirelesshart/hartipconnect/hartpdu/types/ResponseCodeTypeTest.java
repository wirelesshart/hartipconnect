package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ResponseCodeType;

public class ResponseCodeTypeTest {

    public ResponseCodeTypeTest() {}

    @Test
    public void testGetCodeID() {
        for (ResponseCodeType instance : ResponseCodeType.values()) {
            int expResult = Integer.valueOf(instance.name().split("_")[1]);
            int result = instance.getCodeID();
            assertEquals(expResult, result);
        }
    }

    @Test
    public void testToFromResponseByte() throws Exception {
        for (ResponseCodeType expResult : ResponseCodeType.values()) {
            byte responseByte = expResult.toResponseCodeByte();
            ResponseCodeType result = ResponseCodeType.fromResponseByte(responseByte);
            assertEquals(expResult, result);
        }
    }

    @Test
    public void testFromResponseByte() {
        try {
            // Test what happens if a user gives us an Communication Error Byte. This has the 7th bit set.
            ResponseCodeType.fromResponseByte((byte) 0x80);
            fail("This should have failed");
        } catch (HartSystemException ex) {
            // all is well.
        }
    }

    @Test
    public void testGetHumanString() {
        for (ResponseCodeType instance : ResponseCodeType.values()) {
            assertNotNull(instance.getHumanString());
            assertNotNull(instance.getClassification());

        }
    }

}
