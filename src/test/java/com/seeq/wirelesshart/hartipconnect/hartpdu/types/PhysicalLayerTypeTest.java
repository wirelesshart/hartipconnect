package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.PhysicalLayerException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.PhysicalLayerType;

public class PhysicalLayerTypeTest {

    @Test
    public void testFromDelimeterByte() {
        for (PhysicalLayerType expected : PhysicalLayerType.values()) {
            byte delimeterByte = expected.appendToDelimeterByte((byte) 0x00);

            try {
                PhysicalLayerType result = PhysicalLayerType.fromDelimeterByte(delimeterByte);
                assertEquals(expected, result);
            } catch (PhysicalLayerException e) {
                fail("This should not have failed for " + expected.name());
            }
        }

        for (PhysicalLayerType expected : PhysicalLayerType.values()) {
            byte delimeterByte = (byte) (0x03 << 3);

            try {
                PhysicalLayerType.fromDelimeterByte(delimeterByte);
                fail("This should have failed for " + expected.name());
            } catch (PhysicalLayerException e) {
                // All is well.
            }
        }
    }

    @Test
    public void testGetValue() {
        for (PhysicalLayerType instance : PhysicalLayerType.values()) {
            int expected = instance.ordinal();
            int result = instance.getValue();
            assertEquals(expected, result);
        }
    }

}
