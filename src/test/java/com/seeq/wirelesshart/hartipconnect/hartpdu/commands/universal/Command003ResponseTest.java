package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartUtilities;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.DeviceVariable;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic.GenericNoArgCommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command003Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.EngineeringUnitsType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Command003ResponseTest {
    float pVLoopCurrent = 1.0f;
    float pVLoopCurrentALT = 2.0f;
    DeviceVariable primaryVariable = new DeviceVariable(10.0f, EngineeringUnitsType.UNITS_251);
    DeviceVariable secondaryVariable = new DeviceVariable(12.0f, EngineeringUnitsType.UNITS_001);
    DeviceVariable tertiaryVariable = new DeviceVariable(13.0f, EngineeringUnitsType.UNITS_002);
    DeviceVariable quaternaryVariable = new DeviceVariable(14.0f, EngineeringUnitsType.UNITS_003);
    DeviceVariable primaryVariableALT = new DeviceVariable(15.0f, EngineeringUnitsType.UNITS_004);
    DeviceVariable secondaryVariableALT = new DeviceVariable(16.0f, EngineeringUnitsType.UNITS_005);
    DeviceVariable tertiaryVariableALT = new DeviceVariable(17.0f, EngineeringUnitsType.UNITS_006);
    DeviceVariable quaternaryVariableALT = new DeviceVariable(18.0f, EngineeringUnitsType.UNITS_007);

    @Test
    public void testToBytes() throws EngineeringUnitsException {
        Command003Response instance = new Command003Response();

        // Check the correct number of bytes are returned by default.
        List<Byte> outBytes = instance.toBytes();
        assertEquals(0.0f, instance.getpVLoopCurrent().floatValue(), 0.0f);
        assertNotNull(instance.getPrimaryVariable());
        assertNull(instance.getSecondaryVariable());
        assertNull(instance.getTertiaryVariable());
        assertNull(instance.getQuaternaryVariable());

        assertNotNull(outBytes);
        assertEquals(9, outBytes.size());

        // Test the full encoding/decoding roundtrips using the different number of input lengths.
        this.assertSuccessfulRoundtrip(24, this.primaryVariable, this.secondaryVariable, this.tertiaryVariable,
                this.quaternaryVariable);
        this.assertSuccessfulRoundtrip(19, this.primaryVariable, this.secondaryVariable, this.tertiaryVariable, null);
        this.assertSuccessfulRoundtrip(14, this.primaryVariable, this.secondaryVariable, null, null);
        this.assertSuccessfulRoundtrip(9, this.primaryVariable, null, null, null);

        // Test that we correctly reject wrong sized input byte arrays.
        try {
            instance.fromBytes(Arrays.asList(new Byte[] { 0x01, 0x02, 0x03, 0x04 }));
            fail("This should have failed");
        } catch (IndexOutOfBoundsException ex) {
            // all is well!
        }
    }

    @Test
    public void testEqualsCommandData() throws EngineeringUnitsException {
        Command003Response instance = new Command003Response();

        // Object level equality
        assertFalse(instance.equals(null));
        assertFalse(instance.equals(Integer.valueOf(10)));
        assertTrue(instance.equals(instance));
        assertFalse(instance.equals(new GenericNoArgCommandData()));

        // Initialize our instance:
        instance.setpVLoopCurrent(this.pVLoopCurrent);
        instance.setPrimaryVariable(this.primaryVariable);
        instance.setSecondaryVariable(this.secondaryVariable);
        instance.setTertiaryVariable(this.tertiaryVariable);
        instance.setQuaternaryVariable(this.quaternaryVariable);

        // Equality against other instances of similar objects.
        Command003Response instance2 = new Command003Response();

        assertFalse(instance.equals(instance2));

        instance2.fromBytes(instance.toBytes());
        assertTrue(instance.equals(instance2));

        // Test equality of pVLoopCurrent
        instance2.setpVLoopCurrent(this.pVLoopCurrentALT);
        assertFalse(instance.equals(instance2));
        instance2.setpVLoopCurrent(null);
        assertFalse(instance.equals(instance2));
        assertFalse(instance2.equals(instance));
        instance.setpVLoopCurrent(null);
        assertTrue(instance.equals(instance2));
        instance2.setpVLoopCurrent(this.pVLoopCurrent);
        instance.setpVLoopCurrent(this.pVLoopCurrent);
        assertTrue(instance.equals(instance2));

        // Test primaryVariable
        instance2.setPrimaryVariable(this.primaryVariableALT);
        assertFalse(instance.equals(instance2));
        instance2.setPrimaryVariable(null);
        assertFalse(instance.equals(instance2));
        assertFalse(instance2.equals(instance));
        instance.setPrimaryVariable(null);
        assertTrue(instance.equals(instance2));
        instance2.setPrimaryVariable(this.primaryVariable);
        instance.setPrimaryVariable(this.primaryVariable);
        assertTrue(instance.equals(instance2));

        // Test secondaryVariable
        instance2.setSecondaryVariable(this.secondaryVariableALT);
        assertFalse(instance.equals(instance2));
        instance2.setSecondaryVariable(null);
        assertFalse(instance.equals(instance2));
        assertFalse(instance2.equals(instance));
        instance.setSecondaryVariable(null);
        assertTrue(instance.equals(instance2));
        instance2.setSecondaryVariable(this.secondaryVariable);
        instance.setSecondaryVariable(this.secondaryVariable);
        assertTrue(instance.equals(instance2));

        // Test TertiaryVariable
        instance2.setTertiaryVariable(this.tertiaryVariableALT);
        assertFalse(instance.equals(instance2));
        instance2.setTertiaryVariable(null);
        assertFalse(instance.equals(instance2));
        assertFalse(instance2.equals(instance));
        instance.setTertiaryVariable(null);
        assertTrue(instance.equals(instance2));
        instance2.setTertiaryVariable(this.tertiaryVariable);
        instance.setTertiaryVariable(this.tertiaryVariable);
        assertTrue(instance.equals(instance2));

        // Test quaternaryVariable
        instance2.setQuaternaryVariable(this.quaternaryVariableALT);
        assertFalse(instance.equals(instance2));
        instance2.setQuaternaryVariable(null);
        assertFalse(instance.equals(instance2));
        assertFalse(instance2.equals(instance));
        instance.setQuaternaryVariable(null);
        assertTrue(instance.equals(instance2));
        instance2.setQuaternaryVariable(this.quaternaryVariable);
        instance.setQuaternaryVariable(this.quaternaryVariable);
        assertTrue(instance.equals(instance2));

    }

    @Test
    public void testToString() {
        Command003Response instance = new Command003Response();
        assertTrue(instance.toString().matches("Command003Response \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        Command003Response instance = new Command003Response();

        assertEquals(FrameType.ACK, instance.getFrameType());
    }

    @Test
    public void testGetTotalDataByteCount() {
        Command003Response instance = new Command003Response();
        List<Byte> outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(9, outBytes.size());
        assertEquals(9, instance.getTotalDataByteCount());
    }

    @Test
    public void testGetCommandType() {
        Command003Response instance = new Command003Response();

        assertEquals(CommandRegistry.COMMAND_003, instance.getCommandType());
    }

    private void assertSuccessfulRoundtrip(int numBytesExpected, DeviceVariable PV, DeviceVariable SV,
            DeviceVariable TV, DeviceVariable QV) throws EngineeringUnitsException {
        Command003Response instance = new Command003Response();

        // Initialize our instance:
        instance.setpVLoopCurrent(this.pVLoopCurrent);
        instance.setPrimaryVariable(PV);
        instance.setSecondaryVariable(SV);
        instance.setTertiaryVariable(TV);
        instance.setQuaternaryVariable(QV);

        // Check that we can correctly encode our parameters
        List<Byte> encodedBytes = instance.toBytes();
        assertNotNull(encodedBytes);
        assertEquals(numBytesExpected, encodedBytes.size());

        assertEquals(HartUtilities.byteArrayToByteList(HartUtilities.floatToByteArray(this.pVLoopCurrent)),
                encodedBytes.subList(0, 4));
        assertEquals(PV.toBytes(), encodedBytes.subList(4, 9));
        if (SV != null) {
            assertEquals(SV.toBytes(), encodedBytes.subList(9, 14));
        }
        if (TV != null) {
            assertEquals(TV.toBytes(), encodedBytes.subList(14, 19));
        }
        if (QV != null) {
            assertEquals(QV.toBytes(), encodedBytes.subList(19, 24));
        }

        // Test that we correctly decode the incoming bytes into our parameters.
        Command003Response instance2 = new Command003Response();
        instance2.fromBytes(encodedBytes);
        assertEquals(PV, instance2.getPrimaryVariable());
        assertEquals(SV, instance2.getSecondaryVariable());
        assertEquals(TV, instance2.getTertiaryVariable());
        assertEquals(QV, instance2.getQuaternaryVariable());

    }

    @Test
    public void testHashCode() throws EngineeringUnitsException {
        Command003Response instance = new Command003Response();
        instance.setpVLoopCurrent(this.pVLoopCurrent);
        instance.setPrimaryVariable(this.primaryVariable);
        instance.setSecondaryVariable(this.secondaryVariable);
        instance.setTertiaryVariable(this.tertiaryVariable);
        instance.setQuaternaryVariable(this.quaternaryVariable);

        Command003Response instance2 = new Command003Response();
        instance2.fromBytes(instance.toBytes());

        assertTrue(instance.hashCode() != 0);
        assertEquals(instance.hashCode(), instance2.hashCode());
    }
}
