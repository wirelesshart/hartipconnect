package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.generic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command003Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class GenericCommandDataTest {

    @Test
    public void testToBytes() {
        GenericCommandData instance = new GenericCommandData();
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertTrue(outBytes.isEmpty());

        instance.fromBytes(Arrays.asList(new Byte[]{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a}));
        outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(10, outBytes.size());
    }

    @Test
    public void testFromBytes() {
        GenericCommandData instance = new GenericCommandData();
        instance.fromBytes(Arrays.asList(new Byte[]{0x01, 0x02, 0x03}));

        assertEquals(3, instance.toBytes().size());

        instance = new GenericCommandData();
        instance.fromBytes(new ArrayList<>());

        assertEquals(0, instance.toBytes().size());

        instance = new GenericCommandData();
        instance.fromBytes(null);

        assertEquals(0, instance.toBytes().size());
    }

    @Test
    public void testSetCommandType() {
        GenericCommandData instance = new GenericCommandData();

        assertNull(instance.getCommandType());

        instance.setCommandType(CommandRegistry.COMMAND_003);

        assertEquals(CommandRegistry.COMMAND_003, instance.getCommandType());
    }

    @Test
    public void testEqualsCommandData() {
        GenericCommandData instance = new GenericCommandData();

        assertFalse(instance.equals(null));
        assertFalse(instance.equals(Integer.valueOf(10)));
        assertTrue(instance.equals(instance));
        assertFalse(instance.equals(new Command003Response()));

        GenericCommandData instance2 = new GenericCommandData();

        assertTrue(instance.equals(instance2));

        assertEquals(instance.hashCode(), instance2.hashCode());

        instance2.fromBytes(Arrays.asList(new Byte[]{0x01, 0x02, 0x03}));
        assertFalse(instance.equals(instance2));
        instance2.fromBytes(instance.toBytes());
        assertTrue(instance.equals(instance2));

        instance2.setCommandType(CommandRegistry.COMMAND_001);
        assertFalse(instance.equals(instance2));

    }

    @Test
    public void testToString() {
        GenericCommandData instance = new GenericCommandData();
        instance.fromBytes(Arrays.asList(new Byte[]{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a}));
        assertTrue(instance.toString().matches("GenericCommandData \\[.*\\]"));

        instance = new GenericCommandData();
        instance.fromBytes(null);
        assertTrue(instance.toString().matches("GenericCommandData \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        GenericCommandData instance = new GenericCommandData();
        assertNull(instance.getFrameType());

        instance.setFrameType(FrameType.STX);
        assertEquals(FrameType.STX, instance.getFrameType());

    }

    @Test
    public void testGetTotalDataByteCount() {
        GenericCommandData instance = new GenericCommandData(CommandRegistry.COMMAND_000, FrameType.STX);
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertTrue(outBytes.isEmpty());
        assertEquals(0, instance.getTotalDataByteCount());

        instance.fromBytes(Arrays.asList(
                new Byte[]{0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a}));
        outBytes = instance.toBytes();

        assertNotNull(outBytes);

        assertEquals(10, outBytes.size());
        assertEquals(10, instance.getTotalDataByteCount());
    }
}
