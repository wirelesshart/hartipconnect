package com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;

public class Command020ResponseTest {

    @Test
    public void testToBytes() throws EngineeringUnitsException {
        Command020Response instance = new Command020Response();

        // Check the correct number of bytes are returned by default.
        List<Byte> outBytes = instance.toBytes();
        assertNotNull(outBytes);
        assertEquals(Command020Response.TOTAL_BYTE_COUNT, outBytes.size());

        // Test that we correctly reject wrong sized input byte arrays.
        try {
            instance.fromBytes(Arrays.asList(new Byte[] { 0x01, 0x02, 0x03, 0x04 }));
            fail("This should have failed");
        } catch (IllegalArgumentException ex) {
            // all is well!
        }
    }

    @Test
    public void testToString() {
        Command020Response instance = new Command020Response();
        assertTrue(instance.toString().matches("Command020Response \\[.*\\]"));
    }

    @Test
    public void testGetFrameType() {
        Command020Response instance = new Command020Response();

        assertEquals(FrameType.ACK, instance.getFrameType());
    }

    @Test
    public void testGetTotalDataByteCount() {
        Command020Response instance = new Command020Response();

        List<Byte> outBytes = instance.toBytes();

        assertNotNull(outBytes);
        assertEquals(Command020Response.TOTAL_BYTE_COUNT, outBytes.size());
        assertEquals(Command020Response.TOTAL_BYTE_COUNT, instance.getTotalDataByteCount());
    }

    @Test
    public void testGetCommandType() {
        Command020Response instance = new Command020Response();

        assertEquals(CommandRegistry.COMMAND_020, instance.getCommandType());
    }

    @Test
    public void testRoundtrip() throws EngineeringUnitsException {
        Command020Response instance = new Command020Response();
        instance.setLongTag("foo");

        Command020Response built = new Command020Response();

        List<Byte> outList = instance.toBytes();
        built.fromBytes(outList);

        assertEquals(instance.getLongTag(), built.getLongTag());
        assertEquals(instance, built);
    }

}
