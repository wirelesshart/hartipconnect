package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.EngineeringUnitsType;

public class EngineeringUnitsTypeTest {
    private static final byte UNIMPLEMENTED_UNITS = (byte) 254;

    @Test
    public void testComplete() {
        for (EngineeringUnitsType instance : EngineeringUnitsType.values()) {
            Byte testByte = instance.toEngineeringUnitsByte();
            Byte engineeringUnitsId = instance.getEngineeringUnitsId();
            String longHumanString = instance.getLongUnits();
            String shortHumanString = instance.getShortUnits();

            Byte expUnitsId = Integer.valueOf(instance.name().split("_")[1]).byteValue();

            assertNotNull(testByte);
            assertNotNull(longHumanString);
            assertNotNull(shortHumanString);

            assertTrue(testByte == engineeringUnitsId);
            assertEquals(expUnitsId, engineeringUnitsId);

            try {
                EngineeringUnitsType resultUnits = EngineeringUnitsType.fromEngineeringUnitsByte(testByte);

                assertEquals(instance, resultUnits);
            } catch (EngineeringUnitsException e) {
                fail("This should not fail");
            }
        }

        try {
            EngineeringUnitsType.fromEngineeringUnitsByte(UNIMPLEMENTED_UNITS);
            fail("This should have failed");
        } catch (EngineeringUnitsException e) {
            // all is well!
        }
    }

}
