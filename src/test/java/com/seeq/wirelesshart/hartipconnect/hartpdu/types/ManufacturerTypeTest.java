package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.system.ManufacturerException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.ManufacturerType;

public class ManufacturerTypeTest {
    private static final byte UNIMPLEMENTED_MANUFACTURER = (byte) 0xff;

    @Test
    public void testFromManufacturerByte() {
        for (ManufacturerType instance : ManufacturerType.values()) {
            Byte manufacturerID = instance.getManufacturerID();
            String manufacturerName = instance.getManufacturerName();
            Byte expManufacturerId = Integer.valueOf(instance.name().split("_")[1]).byteValue();

            assertNotNull(manufacturerID);
            assertNotNull(manufacturerName);

            assertEquals(expManufacturerId, manufacturerID);

            try {
                ManufacturerType resultManufacturer = ManufacturerType.fromID(expManufacturerId);

                assertEquals(instance, resultManufacturer);
            } catch (ManufacturerException e) {
                fail("This should not fail");
            }
        }

        try {
            ManufacturerType.fromID(UNIMPLEMENTED_MANUFACTURER);
            fail("This should have failed");
        } catch (ManufacturerException e) {
            // all is well!
        }
    }

}
