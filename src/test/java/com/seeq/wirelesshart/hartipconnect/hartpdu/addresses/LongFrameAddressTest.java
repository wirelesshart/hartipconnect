package com.seeq.wirelesshart.hartipconnect.hartpdu.addresses;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.AddressType;

public class LongFrameAddressTest {
    private static byte[] testAddress = new byte[] { 0x01, 0x02, 0x03 };

    @Test
    public void testGettersAndSetters() {
        LongFrameAddress instance = new LongFrameAddress();
        instance.setDeviceInBurstMode(true);
        instance.setExpandedDeviceTypeCode((short) 0x26ef);
        instance.setPrimaryMaster(true);

        instance.setUniqueDeviceIdentifier(testAddress);

        assertEquals(AddressType.IS_UNIQUE_5_BYTE, instance.getAddressType());
        assertEquals(true, instance.isDeviceInBurstMode());
        assertEquals((short) 0x26ef, instance.getExpandedDeviceTypeCode());
        assertEquals(true, instance.isPrimaryMaster());
        assertEquals(testAddress, instance.getUniqueDeviceIdentifier());
    }

    @Test
    public void testEqualsObject() {
        LongFrameAddress instance = new LongFrameAddress();
        instance.setDeviceInBurstMode(true);
        instance.setUniqueDeviceIdentifier(testAddress);
        instance.setPrimaryMaster(true);

        LongFrameAddress instance2 = new LongFrameAddress();
        instance2.setDeviceInBurstMode(true);
        instance2.setUniqueDeviceIdentifier(testAddress);
        instance2.setPrimaryMaster(true);

        assertEquals(instance, instance);
        assertFalse(instance.equals(null));
        assertFalse(instance.equals(Integer.valueOf(0)));

        assertTrue(instance.equals(instance2));

        instance.setUniqueDeviceIdentifier(testAddress);
        instance2.setUniqueDeviceIdentifier(new byte[] { 0x05, 0x04, 0x03 });
        assertFalse(instance.equals(instance2));
        instance2.setUniqueDeviceIdentifier(instance.getUniqueDeviceIdentifier());

        instance.setPrimaryMaster(true);
        instance2.setPrimaryMaster(false);
        assertFalse(instance.equals(instance2));
        instance2.setPrimaryMaster(instance.isPrimaryMaster());

        instance.setExpandedDeviceTypeCode((short) 0x1230);
        instance2.setExpandedDeviceTypeCode((short) 0x0321);
        assertFalse(instance.equals(instance2));
        instance2.setExpandedDeviceTypeCode(instance.getExpandedDeviceTypeCode());

        instance.setDeviceInBurstMode(true);
        instance2.setDeviceInBurstMode(false);
        assertFalse(instance.equals(instance2));
        instance2.setDeviceInBurstMode(instance.isDeviceInBurstMode());

        assertFalse(instance.equals(new ShortFrameAddress()));
    }

    @Test
    public void testToString() {
        LongFrameAddress instance = new LongFrameAddress();
        assertTrue(instance.toString().matches("LongFrameAddress \\[.*\\]"));
    }

    @Test
    public void testHashCode() {
        LongFrameAddress instance = new LongFrameAddress();
        int baseLine = instance.hashCode();

        instance.setDeviceInBurstMode(true);
        int wBurstMode = instance.hashCode();
        assertTrue(wBurstMode < baseLine);

        instance.setPrimaryMaster(false);
        int woPrimaryMaster = instance.hashCode();
        assertTrue(woPrimaryMaster > wBurstMode);
    }
}
