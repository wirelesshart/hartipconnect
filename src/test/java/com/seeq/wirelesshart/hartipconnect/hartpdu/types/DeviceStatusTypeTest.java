package com.seeq.wirelesshart.hartipconnect.hartpdu.types;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.hartpdu.types.DeviceStatusType;

public class DeviceStatusTypeTest {

    @Test
    public void testGetters() {
        for (DeviceStatusType instance : DeviceStatusType.values()) {
            byte deviceStatusCode = instance.getValue();
            if (instance != DeviceStatusType.ALL_OKAY) {
                assertTrue(deviceStatusCode != 0);
            } else {
                assertEquals(0, deviceStatusCode);
            }

            String humanString = instance.getHumanString();
            assertNotNull(humanString);
        }
    }

    @Test
    public void testToFromDeviceStatusByte() {
        Map<Byte, DeviceStatusType[]> testMatrix = new TreeMap<>();
        testMatrix.put((byte) 0x00, new DeviceStatusType[] { DeviceStatusType.ALL_OKAY });
        testMatrix.put((byte) 0xf0, new DeviceStatusType[] {
                DeviceStatusType.COLD_START,
                DeviceStatusType.CONFIGURATION_CHANGED,
                DeviceStatusType.DEVICE_MALFUNCTION,
                DeviceStatusType.MORE_STATUS_AVAILABLE });

        for (Byte testByte : testMatrix.keySet()) {
            DeviceStatusType[] expectedErrors = testMatrix.get(testByte);
            DeviceStatusType[] foundErrors = DeviceStatusType.fromDeviceStatusByte(testByte);

            assertEquals(expectedErrors.length, foundErrors.length);

            List<DeviceStatusType> foundList = Arrays.asList(foundErrors);
            for (DeviceStatusType expError : expectedErrors) {
                assertTrue(foundList.contains(expError));
            }

            Byte outByte = DeviceStatusType.toDeviceStatusByte(foundErrors);
            assertEquals(testByte, outByte);
        }
    }

}
