package com.seeq.wirelesshart.hartipconnect.hartip;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.SessionInitialization;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageIdType;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageType;
import com.seeq.wirelesshart.hartipconnect.hartip.types.StatusCodeType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;

public class HartIpEncodeDecodeTest {
    private static final Logger log = Logger.getLogger(HartIpEncodeDecodeTest.class.getName());

    @Test
    public void test() throws HartException {
        HartPduTestAssistant.hartPduSweep(hartPdu -> this.testHartIpWHartPdu(hartPdu));
    }

    private void testHartIpWHartPdu(HartPdu hartPdu) throws HartException {
        HartIp instance = new HartIp();
        instance.setMessageBody(hartPdu);
        instance.setMessageId(MessageIdType.TOKEN_PASSING_PDU);
        instance.setMessageType(MessageType.REQUEST);
        instance.setProtocolVersion((byte) 1);
        instance.setSequenceNumber((short) 100);
        instance.setStatusCode(StatusCodeType.SUCCESS);

        try {
            this.roundTripTest(instance);
            if (hartPdu.getCommandType() == CommandRegistry.COMMAND_031) {
                fail("We expected an exception be throws, as the library does not support command 31");
            }
        } catch (UnsupportedOperationException ex) {
            if (hartPdu.getCommandType() == CommandRegistry.COMMAND_031) {
                // all is well.
            } else {
                fail("Unexpected failure detected: " + ex.getLocalizedMessage());
            }
        }
    }

    @Test
    public void testHartIp() throws HartException {
        HartIp instance = new HartIp();
        instance.setProtocolVersion((byte) 1);
        instance.setSequenceNumber((short) 100);

        for (MessageType messageType : MessageType.values()) {
            for (StatusCodeType statusCodeType : StatusCodeType.values()) {
                instance.setStatusCode(statusCodeType);
                instance.setMessageType(messageType);

                instance.setMessageId(MessageIdType.KEEP_ALIVE);
                instance.setMessageBody(null);
                this.roundTripTest(instance);

                instance.setMessageId(MessageIdType.SESSION_INIT);
                instance.setMessageBody(new SessionInitialization());
                this.roundTripTest(instance);

                instance.setMessageId(MessageIdType.SESSION_CLOSE);
                instance.setMessageBody(null);
                this.roundTripTest(instance);
            }
        }
    }

    private void roundTripTest(HartIp instance) throws HartException {
        instance.validate();

        log.fine("------------------------------");
        log.fine("EXPECT: " + instance);

        byte[] encodedBytes = HartIpEncoder.encode(instance);

        StringBuilder builder = new StringBuilder();
        builder.append("ENCODE: ");
        for (byte outByte : encodedBytes) {
            builder.append(String.format("0x%x ", outByte));
        }
        log.fine(builder.toString());

        HartIp decodedHartIp = HartIpDecoder.decode(encodedBytes);

        log.fine("REC'VD: " + decodedHartIp);
        assertEquals(instance, decodedHartIp);
    }
}
