package com.seeq.wirelesshart.hartipconnect.hartip;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.HartException;
import com.seeq.wirelesshart.hartipconnect.hartip.messagebody.SessionInitialization;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageIdType;
import com.seeq.wirelesshart.hartipconnect.hartip.types.MessageType;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPdu;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.DeviceAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.LongFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.addresses.ShortFrameAddress;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandData;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandDataFactory;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.CommandRegistry;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command074Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command084Request;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.common.Command084Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command000Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.commands.universal.Command020Response;
import com.seeq.wirelesshart.hartipconnect.hartpdu.types.FrameType;
import com.seeq.wirelesshart.hartipconnect.vendors.manuf038.dev0x264e.universal.command003.C003Dx264eResponse;

/**
 * The following is the code used for the demo of the basic functionality of the {@link HartIpTransmitter}.
 * 
 * TODO: efhilton 2015-04-23: This is not exactly a unit test, but rather a sequence of commands that are to be executed
 * for the sake of demonstrating this to Mark N. Please refer to SE-27 which indicates that we need to have proper unit
 * tests.
 */
public class HartIpTransmitterTest {
    private static final short ROSEMOUNT_WIRELESS_HART_EXPANDED_DEVICE_TYPE_CODE = 0x264e;
    // private static final String IP_ADDRESS = "166.130.121.90";
    private static final String IP_ADDRESS = "192.168.1.100";
    private static final int PORT = 5094;
    private short sequenceNumber = 0;

    /**
     * Connect through an insecure connection to the gateway and do tests.
     * 
     * @throws IOException
     * @throws HartException
     * @throws NoSuchAlgorithmException
     * @throws InterruptedException
     * @throws URISyntaxException
     */
//    @Test
    public void testTransmitInsecure() throws IOException, HartException, NoSuchAlgorithmException,
            InterruptedException, URISyntaxException {
        HartGateway gateway = new HartGateway(IP_ADDRESS, PORT);
        try (HartIpTransmitter instance = new HartIpTransmitter(gateway);) {
            assertNotNull(instance);

            this.doTests(instance);
        }
    }

    /**
     * Perform common tests.
     * 
     * @param instance
     *            The {@link HartIpTransmitter} instance.
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     * @throws InterruptedException
     */
    private void doTests(HartIpTransmitter instance) throws UnknownHostException, IOException, HartException,
            InterruptedException {
        // Initialize a session.
        this.initiateSession(instance);

        // Try sending a bunch of keep-alives
        for (int index = 0; index < 5; index++) {
            this.keepSessionAlive(instance);
        }

        // Send command 0
        ShortFrameAddress shortFrameAddress = new ShortFrameAddress();
        shortFrameAddress.setPollingAddress((byte) 0);
        Command000Response commandResponse = (Command000Response) this.sendZeroArgCommand(CommandRegistry.COMMAND_000,
                instance, shortFrameAddress);

        // Define the device address from what was learned from Command 0.
        LongFrameAddress gatewayAddress = new LongFrameAddress();
        gatewayAddress.setExpandedDeviceTypeCode(commandResponse.getExpandedDeviceTypeCode());
        gatewayAddress.setUniqueDeviceIdentifier(commandResponse.getUniqueDeviceIdentificationCode());

        // Make sure this is Rosemount's WirelessHART Gateway 1420
        assertEquals(ROSEMOUNT_WIRELESS_HART_EXPANDED_DEVICE_TYPE_CODE, gatewayAddress.getExpandedDeviceTypeCode());

        // Get a specialized Rosemont Command 3.
        CommandData response;
        response = this.setSpecializedCommand3For1420(instance, gatewayAddress);

        // Send Command 20.
        response = this.sendZeroArgCommand(CommandRegistry.COMMAND_020, instance, gatewayAddress);
        System.out.println("LONG TAG: " + ((Command020Response) response).getLongTag());

        // Send Command 74
        response = this.sendZeroArgCommand(CommandRegistry.COMMAND_074, instance, gatewayAddress);
        int numDevices = ((Command074Response) response).getNumberOfDevicesDetectedInclIoSystem();
        System.out.println("NUMBER OF DEVICES DETECTED INCL. GATEWAY: " + numDevices);
        System.out.println();

        // For each of the devices found above, get some information and query its PV, SV, TV, QV.
        for (int deviceNumber = 0; deviceNumber < numDevices; deviceNumber++) {
            System.out.println("=============================== " + deviceNumber + " =============================");
            Command084Response c84Response = (Command084Response) this.sendCommand084(instance, gatewayAddress,
                    (short) deviceNumber);

            // Build the long from the address obtained from command 84.
            LongFrameAddress deviceAddress = new LongFrameAddress();
            deviceAddress.setExpandedDeviceTypeCode(c84Response.getExpandedDeviceTypeCode());
            deviceAddress.setUniqueDeviceIdentifier(c84Response.getDeviceId());

            // Read the long tag.
            response = this.sendZeroArgCommand(CommandRegistry.COMMAND_020, instance, deviceAddress);
            System.out.println("LONG TAG: " + ((Command020Response) response).getLongTag());

            // Read PV, SV, TV, QV.
            response = this.sendZeroArgCommand(CommandRegistry.COMMAND_003, instance, deviceAddress);
        }

        // close the session.
        this.shutdownSession(instance);
    }

    /**
     * Utility function to send off a Command 3 to the remote Gateway.
     * 
     * @param instance
     *            A {@link HartIpTransmitter} instance.
     * @param deviceAddress
     *            The Long Frame Address for the remote device for which we are querying. For example, use the Long
     *            Frame Address of the gateway to use the Gateway's Command 3 request/response. Use the Long Frame
     *            address of any wireless device to obtain that device's Command 3 request/response.
     * @return The response HART-IP packet.
     * @throws IllegalArgumentException
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     */
    private CommandData setSpecializedCommand3For1420(HartIpTransmitter instance, LongFrameAddress deviceAddress)
            throws IllegalArgumentException, UnknownHostException, IOException, HartException {
        // Register command 3 classes for the 1420 (pretend this has been done already by a higher code layer)
        CommandRegistry.COMMAND_003.registerResponseClass(deviceAddress.getExpandedDeviceTypeCode(),
                C003Dx264eResponse.class);

        // Build Command Packet
        CommandData command = new CommandDataFactory()
                .create(CommandRegistry.COMMAND_003, FrameType.STX, deviceAddress);

        // Build HART-PDU instance.
        HartPdu hartPdu = new HartPdu();
        hartPdu.setCommandData(command);
        hartPdu.setDeviceAddress(deviceAddress);

        // Wrap it inside a HART-IP packet and send it off.
        HartIp c003Response = this.buildHartIpAndSendHartPduRequest(instance, hartPdu);
        assertNotNull(c003Response);

        // Make sure we are dealing with a true Command 3 response
        assertEquals(MessageType.RESPONSE, c003Response.getMessageType());
        assertEquals(MessageIdType.TOKEN_PASSING_PDU, c003Response.getMessageId());
        hartPdu = (HartPdu) c003Response.getMessageBody();
        assertEquals(CommandRegistry.COMMAND_003, hartPdu.getCommandType());

        return hartPdu.getCommandData();
    }

    private CommandData sendCommand084(HartIpTransmitter instance, LongFrameAddress deviceAddress, short deviceNumber)
            throws UnknownHostException, IOException, HartException {
        // Build Command Packet
        Command084Request command = (Command084Request) new CommandDataFactory()
                .create(CommandRegistry.COMMAND_084, FrameType.STX, deviceAddress);
        command.setDeviceNumber(deviceNumber);

        // Build HART-PDU instance.
        HartPdu hartPdu = new HartPdu();
        hartPdu.setCommandData(command);
        hartPdu.setDeviceAddress(deviceAddress);

        // Wrap it inside a HART-IP packet and send it off.
        HartIp response = this.buildHartIpAndSendHartPduRequest(instance, hartPdu);
        assertNotNull(response);

        // Make sure we are dealing with a true Command 3 response
        assertEquals(MessageType.RESPONSE, response.getMessageType());
        assertEquals(MessageIdType.TOKEN_PASSING_PDU, response.getMessageId());
        hartPdu = (HartPdu) response.getMessageBody();
        assertEquals(CommandRegistry.COMMAND_084, hartPdu.getCommandType());

        return hartPdu.getCommandData();
    }

    private CommandData sendZeroArgCommand(CommandRegistry commandRegistry, HartIpTransmitter instance,
            DeviceAddress deviceAddress)
            throws IllegalArgumentException, UnknownHostException, IOException, HartException {
        // Build Command Packet
        CommandData command = new CommandDataFactory().create(commandRegistry, FrameType.STX, deviceAddress);

        // Build HART-PDU instance.
        HartPdu hartPdu = new HartPdu();
        hartPdu.setCommandData(command);
        hartPdu.setDeviceAddress(deviceAddress);

        // Wrap it inside a HART-IP packet and send it off.
        HartIp response = this.buildHartIpAndSendHartPduRequest(instance, hartPdu);
        assertNotNull(response);

        // Make sure we are dealing with a complete command response.
        assertEquals(MessageType.RESPONSE, response.getMessageType());
        assertEquals(MessageIdType.TOKEN_PASSING_PDU, response.getMessageId());
        hartPdu = (HartPdu) response.getMessageBody();
        assertEquals(commandRegistry, hartPdu.getCommandType());

        return hartPdu.getCommandData();
    }

    /**
     * Utility function to send off a "Keep Session Alive" to the remote Gateway.
     * 
     * @param instance
     *            A {@link HartIpTransmitter} instance.
     * @return The response HART-IP packet.
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     */
    private HartIp keepSessionAlive(HartIpTransmitter instance) throws UnknownHostException, IOException, HartException {
        System.out.println();

        // Keep session alive.
        HartIp request = new HartIp();
        request.setMessageId(MessageIdType.KEEP_ALIVE);
        request.setSequenceNumber(this.getSequenceNumber());
        System.out.println(request.getMessageId().name() + " REQUEST: " + request.toString());

        // Make the call, process the response.
        HartIp response = instance.transmit(request);
        assertNotNull(response);
        System.out.println(response.getMessageId().name() + " RESPONS: " + response.toString());

        return response;
    }

    /**
     * Utility function to send off a "Shutdown Session" command to the remote Gateway. Any commands sent to the Gateway
     * after this will result in an error.
     * 
     * @param instance
     *            A {@link HartIpTransmitter} instance.
     * @return The response HART-IP packet.
     * @throws IllegalArgumentException
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     */
    private HartIp shutdownSession(HartIpTransmitter instance) throws UnknownHostException, IOException, HartException {
        System.out.println();

        // Shutdown a session.
        HartIp request = new HartIp();
        request.setMessageId(MessageIdType.SESSION_CLOSE);
        request.setSequenceNumber(this.getSequenceNumber());
        System.out.println(request.getMessageId().name() + " REQUEST: " + request.toString());

        // Make the call, process the response.
        HartIp response = instance.transmit(request);
        assertNotNull(response);
        System.out.println(response.getMessageId().name() + " RESPONS: " + response.toString());

        return response;
    }

    /**
     * Send a "Session Initialize" command to gateway. Failure to send this command at the beginning of an interaction
     * with the Gateway will cause an error on the Gateway.
     * 
     * @param instance
     *            The {@link HartIpTransmitter} instance.o
     * @return The HartIp response.
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     */
    private HartIp initiateSession(HartIpTransmitter instance) throws UnknownHostException, IOException, HartException {
        System.out.println();

        // Initiate a session.
        HartIp request = new HartIp();
        request.setMessageId(MessageIdType.SESSION_INIT);
        request.setMessageBody(new SessionInitialization());
        request.setSequenceNumber(this.getSequenceNumber());
        System.out.println(request.getMessageId().name() + " REQUEST: " + request.toString());

        // Make the call, process the response.
        HartIp response = instance.transmit(request);
        assertNotNull(response);
        System.out.println(response.getMessageId().name() + " RESPONS: " + response.toString());

        return response;
    }

    /**
     * Convenience function to use when transmitting HartPdu packets to the Gateway, embed them inside a HART-IP packet,
     * and transmit it to the remote gateway.
     * 
     * @param instance
     *            A {@link HartIpTransmitter} instance.
     * @param hartPdu
     *            A {@link HartPdu} instance to encapsulate within a HART-IP packet.
     * @return The {@link HartIp} packet response.
     * @throws UnknownHostException
     * @throws IOException
     * @throws HartException
     */
    private HartIp buildHartIpAndSendHartPduRequest(HartIpTransmitter instance, HartPdu hartPdu)
            throws UnknownHostException, IOException, HartException {
        // Build the HART-IP packet.
        HartIp request = new HartIp();
        request.setMessageId(MessageIdType.TOKEN_PASSING_PDU);
        request.setMessageBody(hartPdu);
        request.setSequenceNumber(this.getSequenceNumber());
        System.out.println();
        System.out.println(request.getMessageId().name() + " REQUEST: " + request.toString());

        // Make the call, process the response.
        HartIp response = instance.transmit(request);
        assertNotNull(response);
        System.out.println(response.getMessageId().name() + " RESPONS: " + response.toString());

        return response;
    }

    /**
     * Calculate the appropriate sequence number for the transmission. The sequence number is used to identify a unique
     * request from its unique corresponding response.
     * 
     * @return The next sequence number to use in the transmission.
     */
    private short getSequenceNumber() {
        this.sequenceNumber++;

        // TODO: efhilton 2015-04-23: Mark N, Eric R: The protocol says that the sequence number is a short. Yet, it
        // causes the gateway to crash when the sequence number is greater than 255. What gives?
        if (this.sequenceNumber == 255) {
            this.sequenceNumber = 1;
        }
        return this.sequenceNumber;
    }

}
