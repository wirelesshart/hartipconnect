package com.seeq.wirelesshart.hartipconnect.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.seeq.wirelesshart.hartipconnect.exceptions.communication.BufferOverflowException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.FramingErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.HartCommunicationException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.LongitudinalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.OverrunErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.communication.VerticalParityErrorException;
import com.seeq.wirelesshart.hartipconnect.exceptions.response.ErrorMultiResponseException;
import com.seeq.wirelesshart.hartipconnect.exceptions.response.ErrorSingleResponseException;
import com.seeq.wirelesshart.hartipconnect.exceptions.response.HartResponseException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.AddressException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.CommandException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.EngineeringUnitsException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.FrameException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.HartSystemException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.ManufacturerException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.PhysicalLayerException;
import com.seeq.wirelesshart.hartipconnect.exceptions.system.ResponseException;
import com.seeq.wirelesshart.hartipconnect.hartpdu.HartPduTestAssistant;

public class HartExceptionTest {

    @Test
    public void test() {
        for (Class<? extends HartException> classType : this.getAllKnownExceptions()) {
            HartPduTestAssistant.checkExceptionImplementation(classType);
        }
    }

    private List<Class<? extends HartException>> getAllKnownExceptions() {
        List<Class<? extends HartException>> array = new ArrayList<>();

        // parent exception
        array.add(HartException.class);

        // system exceptions
        array.add(HartSystemException.class);
        array.add(AddressException.class);
        array.add(CommandException.class);
        array.add(FrameException.class);
        array.add(PhysicalLayerException.class);
        array.add(ResponseException.class);
        array.add(EngineeringUnitsException.class);
        array.add(ManufacturerException.class);

        // response exceptions
        array.add(ErrorMultiResponseException.class);
        array.add(ErrorSingleResponseException.class);
        array.add(HartResponseException.class);

        // communication exceptions
        array.add(BufferOverflowException.class);
        array.add(FramingErrorException.class);
        array.add(HartCommunicationException.class);
        array.add(LongitudinalParityErrorException.class);
        array.add(OverrunErrorException.class);
        array.add(VerticalParityErrorException.class);

        return array;
    }

}
