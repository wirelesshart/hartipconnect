# HART-IP Connect
This is the HartIPConnect library, used by client applications to connect to a remote WirelessHART HART-IP 
compatible Gateway device.

## Overview
This library has been created from the ground up to connect to any of 
the HART-IP compatible devices. The library:

1. It defines the HART PDU, which implements the full HART protocol.
2. It piggy-backs the HART PDU on top of the HART-IP protocol.
3. It can be extended to piggy-back the HART PDU on any other protocol, 
such as HART RTU, and any other HART transport layer.
4. It defines several commands and vendors, such that the protocol can 
easily be extended to support other commands and manufacturers.  If
the given command is not defined, then the library will return the default 
values in lieu of the command.

## Author
I am the main author.  Please contact me, Edgar Hilton, at edgar.hilton@gmail.com if you have any questions, comments, 
or concerns.